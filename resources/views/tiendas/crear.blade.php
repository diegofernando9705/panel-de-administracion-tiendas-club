@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                              Crear una nueva Tienda
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            {!! Form::open(['route' => 'tiendas.store', 'files' => true]) !!}

			                    @method('POST')
			                    <meta name="csrf-token" content="{{ csrf_token() }}">


			                    <div class="form-group">
			                        <label><b>Nombre identificador de la tienda:</b></label>
			                        <em>(Este ser&aacute; un nombre &uacute;nico registrado en el sistema, aparecerà en la URL)</em><br>
			                        <input type="text" class="form-control" id="nombreTiendaUnico" name="nombreTiendaUnico">
			                        <div class="nombreTiendaUnicoDiv" id="nombreTiendaUnicoDiv"></div>
			                    </div>
			                    
			                    <div class="form-group">
			                        <label><b>Imagen de su negocio:</b></label><br>
			                        <input type="file" class="form-control" name="imgTienda" id="imgTienda">
			                    </div>
			                    <div class="form-group">
			                        <label><b>Fondo de la tienda:</b></label><br>
			                        <input type="file" class="form-control" name="background_tienda" id="background_tienda">
			                    </div>
			                    <div class="form-group">
			                        <label><b>Nombre:</b></label><br>
			                        <input type="text" class="form-control" name="nombreTienda" id="nombreTienda" value="{{ old('nombreTienda') }}">
			                    </div>
			                    <div class="form-group">
			                        <label><b>Descripcion:</b></label><br>
			                        <textarea class="form-control" name="descripcionTienda" id="descripcionTienda">{{ old('descripcionTienda') }}</textarea>
			                    </div>
			                    <div class="form-group">
			                        <label><b>Direccion:</b></label><br>
			                        <textarea class="form-control" name="direccionTienda" id="direccionTienda">{{ old('direccionTienda') }}</textarea>
			                    </div>
			                    <div class="form-group">
			                        <label><b>Horarios:</b></label><br><br>
			                        <textarea class="form-control" name="horarioTienda" id="horarioTienda">{{ old('horarioTienda') }}</textarea>

			                        <!--<div class="row">
			                        	<div class="lunes col-4 col-sm-4 col-lg-4 col-xl-4">
			                        		<input type="checkbox" name="dia_lunes" value="1" id="1" style="margin-top: 30px;"> Lunes 
			                        		  <div class="row">
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_inicio_lunes" placeholder="First name" id="hora_inicio_1">
											      <small>Hora inicio</small>
											    </div>
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_fin_lunes" placeholder="Last name" id="hora_fin_1">
											      <small>Hora fin</small>
											    </div>
											  </div>
			                        	</div>
			                        	<div class="lunes col-4 col-sm-4 col-lg-4 col-xl-4">
			                        		<input type="checkbox" name="dia_martes" value="2"id="2" style="margin-top: 30px;"> Martes 
			                        		  <div class="row">
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_inicio_martes" placeholder="First name" id="hora_inicio_2">
											      <small>Hora inicio</small>
											    </div>
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_fin_martes" placeholder="Last name" id="hora_fin_2">
											      <small>Hora fin</small>
											    </div>
											  </div>
			                        	</div>
			                        	<div class="lunes col-4 col-sm-4 col-lg-4 col-xl-4">
			                        		<input type="checkbox" name="dia_miercoles" value="3" id="3" style="margin-top: 30px;"> Miercoles
			                        		  <div class="row">
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_inicio_miercoles" placeholder="First name" id="hora_inicio_3">
											      <small>Hora inicio</small>
											    </div>
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_fin_miercoles" placeholder="Last name" id="hora_fin_3">
											      <small>Hora fin</small>
											    </div>
											  </div>
			                        	</div>
			                        	<div class="lunes col-4 col-sm-4 col-lg-4 col-xl-4">
			                        		<input type="checkbox" name="dia_jueves" value="4"id="4" style="margin-top: 30px;"> Jueves
			                        		  <div class="row">
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_inicio_jueves" placeholder="First name" id="hora_inicio_4">
											      <small>Hora inicio</small>
											    </div>
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_fin_jueves" placeholder="Last name" id="hora_fin_4">
											      <small>Hora fin</small>
											    </div>
											  </div>
			                        	</div>
			                        	<div class="lunes col-4 col-sm-4 col-lg-4 col-xl-4">
			                        		<input type="checkbox" name="dia_viernes" value="5" id="5" style="margin-top: 30px;"> Viernes
			                        		  <div class="row">
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_inicio_viernes" placeholder="First name" id="hora_inicio_5">
											      <small>Hora inicio</small>
											    </div>
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_fin_viernes" placeholder="Last name" id="hora_fin_5">
											      <small>Hora fin</small>
											    </div>
											  </div>
			                        	</div>

			                        	<div class="lunes col-4 col-sm-4 col-lg-4 col-xl-4" style="margin-top: 30px;">
			                        		<input type="checkbox" name="dia_sabado" value="6" id="6"> Sabado
			                        		  <div class="row">
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_inicio_sabado" placeholder="First name" id="hora_inicio_6">
											      <small>Hora inicio</small>
											    </div>
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_fin_sabado" placeholder="Last name" id="hora_fin_6">
											      <small>Hora fin</small>
											    </div>
											  </div>
			                        	</div>
			                        	<div class="lunes col-4 col-sm-4 col-lg-4 col-xl-4" style="margin-top: 30px;">
			                        		<input type="checkbox" name="dia_domingo" value="7" id="7"> Domingo
			                        		  <div class="row">
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_inicio_domingo" placeholder="First name" id="hora_inicio_7">
											      <small>Hora inicio</small>
											    </div>
											    <div class="col-6 col-md-6">
											      <input type="time" class="form-control" name="hora_de_fin_domingo" placeholder="Last name" id="hora_fin_7">
											      <small>Hora fin</small>
											    </div>
											  </div>
			                        	</div>
			                        </div>-->
			                        
			                    </div>
			                    <div class="form-group">
			                        <label><b>Valor domicilio: </b></label><br>
			                        <input type="number" class="form-control" name="domicilio">
			                    </div>
			                    <div class="form-group">
			                        <label><b>Departamento:</b></label><br>
			                        <select class="form-control mi-selector" id="departamento" name="departamentoTienda">
			                            <option value="0" selected="">Seleccionar departamento</option>
			                            @foreach($departamentos as $departamento)
			                            <option value="{{ $departamento->id }}">{{ $departamento->nombre_departamento }}</option>
			                            @endforeach
			                        </select>
			                    </div>
			                    <div class="form-group">
			                        <label><b>Ciudad:</b></label><br>
			                        <select class="form-control mi-selector" id="ciudad" name="ciudadTienda">
			                            <option value="" selected="">Seleccione una ciudad</option>
			                        </select>
			                    </div>
			                    <div class="form-group">
			                        <label>Categoria Tienda:</label></label><br>
			                        <select class="form-control mi-selector" id="categoria_tienda" name="categoria_tienda">
			                            <option value="0" selected="">Categoria de la tienda</option>
			                            @foreach($categorias_tienda as $categoria_tienda)
			                            <option value="{{ $categoria_tienda->id }}">{{ $categoria_tienda->nombre_categoria_tienda }}</option>
			                            @endforeach
			                        </select>
			                    </div>
			                    
			                    <div class="form-group">
			                        <label><b>Persona:</b></label><br>
			                        <select class="form-control mi-selector" name="personas">
			                            <option value="0" selected="">Seleccionar departamento</option>
			                            @foreach($personas as $persona)
			                            <option value="{{ $persona->id }}">{{ $persona->primer_nombre }}{{ $persona->primer_apellido }}</option>
			                            @endforeach
			                        </select>
			                    </div>

			                    <div class="form-group">
			                        <label><b>Zona principal:</b></label><br>
			                        <label><code><b>(*)</b>Nombre de la zona:</code></label>
			                        <input type="text" class="form-control" name="nombre_zona" value="{{ old('nombre_zona') }}">
			                        <label><code>Descripción de la zona:</code></label>
			                        <input type="text" class="form-control" name="descripcion_zona" value="{{ old('descripcion_zona') }}">
			                        <label><code><b>(*)</b>Número de WhatsApp de la zona:</code></label>
			                        <input type="text" class="form-control" name="celular_zona" value="{{ old('celular_zona') }}">
			                    </div>


			                    <div class="form-group">
			                        <label><b>Telefono fijo:</b></label></label><br>
			                        <input type="number" class="form-control" name="telFijo" id="telFijo" value="{{ old('telFijo') }}">
			                    </div>

			                    <div class="form-group">
			                        <label><b>Link de Facebook:</b></label>
			                        <input type="text" class="form-control" name="linkFb" id="linkFb" value="{{ old('linkFb') }}">
			                    </div>

			                    <div class="form-group">
			                        <label><b>Link de Instagram:</b></label><br>
			                        <input type="text" class="form-control" name="linkIns" id="linkIns" value="{{ old('linkIns') }}">
			                    </div>

			                    <div class="form-group">
			                        <label><b>Link de Twitter:</b></label><br>
			                        <input type="text" class="form-control" name="linkTwitter" id="linkTwitter" value="{{ old('linkTwitter') }}">
			                    </div>
			                    
			                    <div class="form-group">
			                        <label><b>Llave p&uacute;blica de WOMPI:</b></label><br>
			                        <input type="text" class="form-control" name="llave_publica" id="linkTwitter" value="{{ old('llave_publica') }}">
			                    </div>

			                    <center>
			                        <a href="{{ url('tienda') }}" style="color:white; ">
			                            <button type="button" class="btn btn-danger">
			                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="25px" height="25px" class=""><g><g>
			                                <g>
			                                <path d="M256,0C114.508,0,0,114.497,0,256c0,141.493,114.497,256,256,256c141.492,0,256-114.497,256-256    C512,114.507,397.503,0,256,0z M256,472c-119.384,0-216-96.607-216-216c0-119.385,96.607-216,216-216    c119.384,0,216,96.607,216,216C472,375.385,375.393,472,256,472z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
			                                </g>
			                                </g><g>
			                                <g>
			                                <path d="M343.586,315.302L284.284,256l59.302-59.302c7.81-7.81,7.811-20.473,0.001-28.284c-7.812-7.811-20.475-7.81-28.284,0    L256,227.716l-59.303-59.302c-7.809-7.811-20.474-7.811-28.284,0c-7.81,7.811-7.81,20.474,0.001,28.284L227.716,256    l-59.302,59.302c-7.811,7.811-7.812,20.474-0.001,28.284c7.813,7.812,20.476,7.809,28.284,0L256,284.284l59.303,59.302    c7.808,7.81,20.473,7.811,28.284,0C351.398,335.775,351.397,323.112,343.586,315.302z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
			                                </g>
			                                </g></g> </svg>
			                                Cancelar registro
			                            </button>
			                        </a>

			                        <button type="submit" class="btn btn-info" id="registroTienda">Registrar tienda</button>
			                    </center>
			                    {!! Form::close() !!}
                        </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>


@endsection
