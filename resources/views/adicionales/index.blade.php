<!-- Listado de las tiendas, no influye si es Administrador o Cliente -->

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @include('custom.message')

            <div class="card">
                <div class="card-header">Listado de las categorias con productos</div>
                <div class="card-body">
                    <strong>Buscar tienda:</strong>
                    <input type="text" class="form-control col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" />

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Imagen Categorias</th>
                                <th>Nombre Categoria</th>
                                <th>Descripcion Categoria</th>
                                <th>Estado</th>
                                <th colspan="3"><center>Acciones</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categorias as $categoria)
                            <?php if (is_array($array_categoria) && in_array($categoria->id, $array_categoria)) { ?>
                                <tr>
                                    <td><img src="{{ asset('/storage/'.$categoria->imagen_categoria) }}" width="150px"/></td>
                                    <td>{{ $categoria->nombre_categoria }}</td>
                                    <td>{{ $categoria->descripcion_categoria }}</td>
                                    <td>
                                        @if($categoria->estado_categoria == 1)
                                            Activo
                                        @else
                                            Inactivo
                                        @endif
                                    </td>
                                    @if(Gate::authorize('haveaccess','categoryProductos.edit'))
                                    <td>
                                        <a href="{{ route('categoriasproductos.edit', $categoria->id)}}">
                                            <button type="button" class="btn btn-info btn-sm">
                                                <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                </svg>
                                            </button>
                                        </a>
                                    </td>
                                    @endif

                                    @if(Gate::authorize('haveaccess','categoryProductos.show'))
                                    <td>
                                        <a href="{{ route('categoriasproductos.show', $categoria->id)}}">
                                            <button type="button" class="btn btn-success btn-sm">
                                                <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                                </svg>
                                            </button>
                                        </a>
                                    </td>
                                    @endif

                                    @if(Gate::authorize('haveaccess','categoryProductos.destroy'))
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm">
                                            <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-archive-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
                                            </svg>
                                        </button>
                                    </td>
                                    @endif
                                </tr>
                            <?php } ?>


                            <?php if (is_array($codigo_temporal) && in_array($categoria->id, $codigo_temporal)) { ?>
                                <tr>
                                    <td><img src="{{ asset('/storage/'.$categoria->imagen_categoria) }}" width="150px"/></td>
                                    <td>{{ $categoria->nombre_categoria }}</td>
                                    <td>{{ $categoria->descripcion_categoria }}</td>
                                    <td>
                                        @if($categoria->estado_categoria == 1)
                                            Activo
                                        @elseif ($categoria->estado_categoria == 3)
                                            Sin asignacion de productos
                                        @else
                                            Inactivo
                                        @endif
                                    </td>
                                    @if(Gate::authorize('haveaccess','categoryProductos.edit'))
                                    <td>
                                        <a href="{{ route('categoriasproductos.edit', $categoria->id)}}">
                                            <button type="button" class="btn btn-info btn-sm">
                                                <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                </svg>
                                            </button>
                                        </a>
                                    </td>
                                    @endif

                                    @if(Gate::authorize('haveaccess','categoryProductos.show'))
                                    <td>
                                        <a href="{{ route('categoriasproductos.show', $categoria->id)}}">
                                            <button type="button" class="btn btn-success btn-sm">
                                                <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                                </svg>
                                            </button>
                                        </a>
                                    </td>
                                    @endif

                                    @if(Gate::authorize('haveaccess','categoryProductos.destroy'))
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm">
                                            <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-archive-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
                                            </svg>
                                        </button>
                                    </td>
                                    @endif
                                </tr>
                            <?php } ?>

                            @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
            <br /><br />
        
        </div>
    </div>
</div>
@endsection