<div class="tituloModal">
    <div class="container">
        <h3>
            <strong>Descripcion del pedido</strong>
        </h3>
    </div>
</div> 
<div class="container" class="contenerdorFormulario">
	@foreach($pedidos_domicilios as $pedido_domicilio)
		<div class="form-group">
			<label><b>Codigo del pedido:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->codigo_pedido }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Nombre del cliente:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->nombre }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Telefono del cliente:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->telefono }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Direccion del domicilio:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->direccion }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Barrio del cliente:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->barrio }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Indicaciones adicionales:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->indicacion_adicional }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Comentarios del cliente:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->comentarios }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Productos comprados:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->productos }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Adicionales de los productos:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->adicionales }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Costo del domicilio:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->domicilio }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Valor del adicional</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->valor_adicional }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Valor de los productos:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->valor_productos }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Valor total del pedido:</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_domicilio->total_pedido }}" disabled="">
		</div>
	@endforeach
</div>