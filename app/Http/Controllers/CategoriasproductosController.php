<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Gate;
use DateTime;


class CategoriasproductosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        Gate::authorize('haveaccess','categoryProductos.show');


        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {
                $categorias = DB::table('categorias_productos')->paginate(10);
                
                return view('categorias_productos.administrator.index', compact('categorias'));
            } else {

                $array_categoria = [];
                $codigo_temporal = [];

                $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();

                foreach ($tiendas as $tienda) {
                    $codigo_tienda = $tienda->id_tienda;
                }

                
                $productos_tienda = DB::table('detalle_tienda_productos')
                        ->select('productos.*', 'detalle_tienda_productos.*')
                        ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->where('id_tienda', $codigo_tienda)
                        ->get();

                $categorias = DB::table('categorias_productos')->get();


                foreach ($productos_tienda as $producto) {
                    $array_categoria[] = $producto->id_categoria;
                }


                $categorias_temporales = DB::table('categorias_temporales')->where('id_tienda', $codigo_tienda)->get();
                
                foreach ($categorias_temporales as $categoria_temporal) {
                    $codigo_temporal[] = $categoria_temporal->id_categoria;
                }


                /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

            $detalles_del_plan = DB::table('detalle_planes_tiendas')
              ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
              ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
              ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
              ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
              ->get();

            $dia_actual = new DateTime(date('Y-m-d'));
            $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

            $funcion = $dia_actual->diff($fecha_proximo_pago);

            if($fecha_proximo_pago < $dia_actual){
              $dias_faltantes = 0;
            }else{
              $dias_faltantes = $funcion->days;
            }

          /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

          if($dias_faltantes <= 0){
            $planes = DB::table('tabla_planes')->get();
            return view('planes.tienda.pago-plan', compact('planes'));
          }else{
            return view('categorias_productos.index', compact('array_categoria', 'categorias', 'codigo_temporal'));
          }

          
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        Gate::authorize('haveaccess','categoryProductos.create');


        $usuario = DB::table('role_user')
          ->select('users.*', 'roles.*', 'role_user.*')
          ->join('roles', 'role_user.role_id', '=', 'roles.id')
          ->join('users', 'role_user.user_id', '=', 'users.id')
          ->where('users.id', Auth::user()->id)->get();


        foreach ($usuario as $user) {
          if ($user->slug == "admin") {

            $dias_faltantes = 1; 

            $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();

          }else{
            $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();
              /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

              $detalles_del_plan = DB::table('detalle_planes_tiendas')
                ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
                ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
                ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
                ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
                ->get();

              $dia_actual = new DateTime(date('Y-m-d'));
              $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

              $funcion = $dia_actual->diff($fecha_proximo_pago);

              if($fecha_proximo_pago < $dia_actual){
                $dias_faltantes = 0;
              }else{
                $dias_faltantes = $funcion->days;
              }

            /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */
          }
        }

          if($dias_faltantes <= 0){
            $planes = DB::table('tabla_planes')->get();
            return view('planes.tienda.pago-plan', compact('planes'));
          }else{

            /* VALIDAMOS CUANDO SEA PLAN FREE PARA QUE NO AGREGUE MAS DE 10 PRODUCTOS */

                $total_productos = DB::table('detalle_tienda_productos')
                        ->select('productos.*', 'detalle_tienda_productos.*')
                        ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->where('id_tienda', $tiendas[0]->id_tienda)
                        ->count();

                $captura_rol = DB::table('role_user')->where('user_id', Auth::user()->id)->get();

                if($captura_rol[0]->role_id == '755'){
                  if($total_productos <= 10){
                     return view('categorias_productos.create');  
                  }else{
                     return view('productos.mensaje_actualizar_plan');
                  }
                }else{
                   return view('categorias_productos.create');
                }

                

                /* FIN VALIDACIONES CUANDO SEA PLAN FREE PARA QUE NO AGREGUE MAS DE 10 PRODUCTOS */
          }

          
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        Gate::authorize('haveaccess','categoryProductos.create');

        $array_categoria = [];

        $request->validate([
            'imagen_categoria' => 'required|mimes:jpg,jpeg,bmp,png',
            'nombre_categoria' => 'required',
        ]);

        //obtenemos el campo file definido en el formulario
        $file = $request->file('imagen_categoria');
        //obtenemos el nombre del archivo
        $imagen_categoria = $file->getClientOriginalName();
        //obtenemos el nombre del archivo
        $imagen_categoria = $file->getClientOriginalName('imagen_categoria');
        \Storage::disk('public')->put($imagen_categoria, \File::get($file));

        

        $tiendas = DB::table('detalle_usuario_tiendas')
                        ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                        ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                        ->join('users', 'personas.user_register', '=', 'users.id')
                        ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($tiendas as $tienda) {
            $codigo_tienda = $tienda->id_tienda;
        }

        DB::table('categorias_productos')->insert([
            'imagen_categoria' => $imagen_categoria,
            'nombre_categoria' => $request->nombre_categoria,
            'descripcion_categoria' => $request->descripcion_categoria,
            'estado_categoria' => '3'
        ]);

        $categorias = DB::table('categorias_productos')->orderby('id', 'DESC')->take(1)->get();
        foreach ($categorias as $categoria) {
            $array_categoria[] = $categoria->id;
        }

        $codigo_cate = array_sum($array_categoria);

        DB::table('categorias_temporales')->insert([
            'id_categoria' => $codigo_cate,
            'id_tienda' => $codigo_tienda
        ]);

        DB::table('categorias_productos_tiendas')->insert([
            'id_categoria' => $codigo_cate,
            'id_tienda' => $codigo_tienda
        ]);
        
        return redirect()->route('categoriasproductos.index')
                        ->with('status_success', 'Actualizacion exitosa!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
        Gate::authorize('haveaccess','categoryProductos.show');

        $categorias = DB::table('categorias_productos')->where('id', $id)->get();
        return view('categorias_productos.ver', compact('categorias'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
        Gate::authorize('haveaccess','categoryProductos.edit');

        $contenido_categorias = [];
        
        
        $categorias_consulta = DB::table('categorias_productos')->where('id', $id)->get();
        
        foreach ($categorias_consulta as $categoria){
            $contenido_categorias[] = $categoria->id;
        }
        
        //return $contenido_categorias;
        
        if(empty($contenido_categorias)){
            $categorias = DB::table('categorias_temporales')->where('id', $id)->get();
        }else{
            $categorias = DB::table('categorias_productos')->where('id', $id)->get();
        }
        
        return view('categorias_productos.edicion', compact('categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        Gate::authorize('haveaccess','categoryProductos.edit');

        $request->validate([
            'foto_categoria_producto' => 'mimes:jpg,jpeg,bmp,png',
            'nombre_categoria' => 'required',
            'estado_categoria' => 'required',
        ]);

        if(empty($request->foto_categoria)){
            $imagen_categoria = $request->imagen_bd;
        }else{
            //obtenemos el nombre del archivo
             //obtenemos el campo file definido en el formulario
            $file = $request->file('foto_categoria');
            $imagen_categoria = $file->getClientOriginalName('foto_categoria');
            \Storage::disk('public')->put($imagen_categoria, \File::get($file));
        }

        DB::table('categorias_productos')->where('id', $id)->update([
            'imagen_categoria' => $imagen_categoria,
            'nombre_categoria' => $request->nombre_categoria,
            'descripcion_categoria' => $request->descripcion_categoria,
            'estado_categoria' => $request->estado_categoria,
        ]);

        return redirect()->route('categoriasproductos.index')
            ->with('status_success','Actualizacion exitosa!'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
         Gate::authorize('haveaccess','categoryProductos.destroy');

         DB::table('categorias_productos')->where('id', $id)->update([
            'estado_categoria' => '0',
        ]);
        

        return redirect()->route('categoriasproductos.index')
            ->with('status_success','Actualizacion exitosa!'); 

    }


    public function keyup($variable){
        
        Gate::authorize('haveaccess','productos.show');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {

                if(empty($variable)){
                    $categorias = DB::table('categorias_productos')->get();
                }else{
                    $categorias = DB::table('categorias_productos')->where("nombre_categoria", 'like', $variable . "%")->get();
                }
                
                return view('categorias_productos.administrator.keyup', compact('categorias'));
            }else{

                $array_categoria = [];
                $codigo_temporal = [];

                $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();

                foreach ($tiendas as $tienda) {
                    $codigo_tienda = $tienda->id_tienda;
                }

                
                $productos_tienda = DB::table('detalle_tienda_productos')
                        ->select('productos.*', 'detalle_tienda_productos.*')
                        ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->where('id_tienda', $codigo_tienda)
                        ->get();

                $categorias = DB::table('categorias_productos')->where("nombre_categoria", 'like', $variable . "%")->get();


                foreach ($productos_tienda as $producto) {
                    $array_categoria[] = $producto->id_categoria;
                }


                $categorias_temporales = DB::table('categorias_temporales')->where('id_tienda', $codigo_tienda)->get();
                
                foreach ($categorias_temporales as $categoria_temporal) {
                    $codigo_temporal[] = $categoria_temporal->id_categoria;
                }

                return view('categorias_productos.keyup', compact('array_categoria', 'categorias', 'codigo_temporal'));
            }
        }
    }

}
