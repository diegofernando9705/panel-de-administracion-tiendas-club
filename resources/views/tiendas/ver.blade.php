<div class="tituloModal">
    <div class="container">
        <h3>
            <strong>Detalles de la tienda</strong>
        </h3>
    </div>
</div> 
<div class="container" class="contenerdorFormulario">
                            @foreach($tiendas as $tienda)

                                {!! Form::open(['route' => array('tiendas.update', $tienda->url_tienda), 'files' => true]) !!}

                                @csrf
                                @method('PUT')
                                


                                <div class="form-group">
                                    <label>Nombre identificador de la tienda</label>
                                    <em>(Este ser&aacute; un nombre &uacute;nico registrado en el sistema, aparecerà en la URL)</em><br>
                                    <input type="text" class="form-control" id="nombreTiendaUnico" value="{{ $tienda->url_tienda }}" disabled="">
                                    <input type="hidden" class="form-control" id="nombreTiendaUnico" value="{{ $tienda->url_tienda }}" name="nameUnicoTienda">
                                </div>
                                <div class="row">
                                    <div class="form-group col-3 col-md-3 col-lg-3 col-xl-3">
                                        <img src="{{ asset('/storage/'.$tienda->image_tienda) }}" width="150px"/>
                                    </div>
                                    <input type="hidden" name="imagen_bd" value="{{ $tienda->image_tienda }}">
                                    
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Nombre</strong></label><br>
                                    <input type="text" class="form-control" name="nombreTienda" id="nombreTienda" value="{{ $tienda->nombre_tienda }}" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Descripcion</strong></label><br>
                                    <textarea class="form-control" name="descripcionTienda" id="descripcionTienda" disabled="disabled">{{ $tienda->descripcion_tienda }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Direccion:</strong></label><br>
                                    <textarea class="form-control" name="direccionTienda" id="direccionTienda" disabled="disabled">{{ $tienda->direccion_tienda }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Horarios</strong></label><br>
                                    <textarea class="form-control" name="horarioTienda" id="horarioTienda" disabled="disabled">{{ $tienda->horario_tienda }}</textarea>
                                    <small id="emailHelp" class="form-text text-muted">Por favor, separe los dias con una coma. Ejemplo: Lunes de 10:00 a 22:00, Martes de 05:00 a 22:00</small>

                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Domicilio: </strong></label><br>
                                    <input type="number" class="form-control" name="domicilio" value="{{ $tienda->domicilio }}" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Departamento</strong></label><br>
                                    <select class="form-control mi-selector" id="departamento" name="departamentoTienda" disabled="disabled">
                                        <option value="0" selected="">Seleccionar departamento</option>
                                        @foreach($departamentos as $departamento)
                                        <option value="{{ $departamento->id }}" selected="">{{ $departamento->nombre_departamento }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Ciudad</strong></label><br>
                                    <select class="form-control mi-selector" id="departamento" name="ciudadTienda" disabled="disabled">
                                        <option value="0" selected="">Seleccionar departamento</option>
                                        @foreach($ciudades as $ciudad)
                                            @if($tienda->id_ciudad == $ciudad->id)
                                                <option value="{{ $ciudad->id }}" selected="">{{ $ciudad->nombre_ciudad }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Categoria Tienda</strong></label><br>
                                    <select class="form-control mi-selector" id="categoria_tienda" name="categoria_tienda" disabled="disabled">
                                        <option value="0" selected="">Categoria de la tienda</option>
                                        @foreach($categorias_tienda as $categoria_tienda)
                                            @if($categoria_tienda->id == $tienda->id_cate_tienda)
                                                <option value="{{ $categoria_tienda->id }}" selected="">{{ $categoria_tienda->nombre_categoria_tienda }}</option>
                                            @else
                                                <option value="{{ $categoria_tienda->id }}">{{ $categoria_tienda->nombre_categoria_tienda }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label><strong><code>(*)</code> Telefono Movil</strong></label><br>
                                    <input type="number" class="form-control" name="telMovil" id="telMovil" value="{{ $tienda->telefono_movil }}" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Telefono fijo</label><br>
                                    <input type="number" class="form-control" name="telFijo" id="telFijo" value="{{ $tienda->telefono_fijo }}" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Link de Facebook</label>
                                    <input type="text" class="form-control" name="linkFb" id="linkFb" value="{{ $tienda->facebook_link }}" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Link de Instagram</label><br>
                                    <input type="text" class="form-control" name="linkIns" id="linkIns" value="{{ $tienda->instagram_link }}" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Link de Twitter</label><br>
                                    <input type="text" class="form-control" name="linkTwitter" id="linkTwitter" value="{{ $tienda->twitter_link }}" disabled="disabled">
                                </div>
                                 <div class="form-group">
                                    <label><strong><code>(*)</code>Estado de la tienda:</strong></label><br>
                                <select class="form-control mi-selector" id="categoria_tienda" name="categoria_tienda">
                                            @if($tienda->estado_tienda == 1)
                                                <option value="1" selected="">Activo</option>
                                                <option value="0">Inactivo</option>
                                            @else
                                                <option value="0">Activo</option>
                                                <option value="0" selected="">Inactivo</option>
                                            @endif
                                        </select> 
                                    </div>

                                @endforeach

                                {!! Form::close() !!}
                            </div>