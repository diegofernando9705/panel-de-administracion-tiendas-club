<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZonasTiendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zonas_tiendas', function (Blueprint $table) {
            $table->id('id_zona');
            $table->char('nombre_zona', 150);
            $table->longText('descripcion_zona')->nullable();
            $table->char('celular_zona', 13);
            $table->integer('estado_zona');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zonas_tiendas');
    }
}
