<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasTiendasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('categorias_tiendas', function (Blueprint $table) {
            $table->id();
            $table->longText('imagen_categoria_tienda');
            $table->string('nombre_categoria_tienda');
            $table->longText('descripcion_categoria_tienda');
            $table->integer('estado_categoria_tienda');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('categorias_tiendas');
    }

}
