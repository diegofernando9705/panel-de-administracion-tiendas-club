<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encryption extends Model
{
    protected $fillable = ['code_tienda', 'tk_tienda', 'tipo_token', 'estado_token'];
}
