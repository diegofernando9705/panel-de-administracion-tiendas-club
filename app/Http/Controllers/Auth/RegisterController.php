<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB; 

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {   
        $codigo_tienda = rand(1, 9999);

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $usuarios = DB::table('users')->orderby('id','DESC')->take(1)->get();
        foreach($usuarios as $user){
            $array_usuario = $user->id;
        }
        

        
        $usuario = $array_usuario+1;
        $user_delete = $array_usuario;



         //die(print_r($usuario));
        DB::table('personas')->insert([
            'foto_persona'       => 'Tiendas club',
            'primer_nombre'      => 'Tiendas club',
            'segundo_nombre'     => '',
            'primer_apellido'    => 'Tiendas club',
            'segundo_apellido'   => '',
            'telefono'           => '12345678',
            'correo_electronico' => $data['email'],
            'id_ciudad'          => '0',
            'direccion'          => 'Tiendas club',
            'barrio'             => 'Tiendas club',
            'user_register'      => $usuario,
            'estado_persona'     => '3',
        ]);

        DB::table('users')->where('id', $user_delete)->delete();

        $personas = DB::table('personas')->orderby('id','DESC')->take(1)->get();
        foreach($personas as $persona){
            $persona_code = $persona->id;
        }
        
        DB::table('detalle_usuario_tiendas')->insert([
            'id_persona' => $persona_code,
            'id_tienda' => $codigo_tienda
        ]);
        
        DB::table('tiendas')->insert([
            'id_tienda' => $codigo_tienda,
            'image_tienda' => 'Tiendas Club',
            'nombre_tienda' => 'Tiendas Club',
            'url_tienda' => $data['nombreTiendaUnico'],
            'descripcion_tienda' => 'Tiendas club',
            'direccion_tienda' => 'Tiendas club',
            'horario_tienda' => 'Tiendas club',
            'domicilio' => '1',
            'id_cate_tienda' => '1',
            'id_ciudad' => '1',
            'telefono_movil' => '3135523085',
            'telefono_fijo' => '1',
            'instagram_link' => '1',
            'facebook_link' => '1',
            'twitter_link' => '1',
            'estado_tienda' => '1',
        ]);

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        
    }
}
