<form id="formulario-registro-tienda">

    <div class="login__header text-center mb-lg-5 mb-4">
        <h3 class="login__title mb-2">
            <img src="https://tiendas.club/web_principal/images/TIENDAS_CLUB.png" width="150px">
        </h3>
        <br>
        <p>Registro de la tienda - <b>COMPLETAMENTE GRATIS</b>.</p>
    </div>
    
    <div id="informacion-respuesta"></div>
    <div class="informacion">
        <div class="row">
            <div class="codigo_tienda col-6 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                <label><b>C&oacute;digo de la tienda:</b></label><br>
                <input type="text" class="form-control col" name="codigo_tienda" value="{{ $codigo_tienda }}" readonly="">
            </div>
            <div class="codigo_tienda col-6 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                <label><b>Usuario:</b></label><br>
                <input type="hidden" class="form-control col" name="id_user" value="{{ $id_user }}">
                <input type="text" class="form-control col" value="{{ $usuario[0]->name }}" readonly="">
            </div>
        </div>
    </div>
    

    <div class="form-group" style="margin-top: 30px;">
        <label><b>Nombre identificador de la tienda:</b></label>
        <em>(Este ser&aacute; un nombre &uacute;nico registrado en el sistema, aparecerà en la URL)</em><br>
        <input type="text" class="form-control" id="nombreTiendaUnico" name="nombreTiendaUnico">
        <div class="nombreTiendaUnicoDiv" id="nombreTiendaUnicoDiv"></div>
    </div>

    <div class="form-group">
        <label><b>Nombre:</b></label><br>
        <input type="text" class="form-control" name="nombreTienda" value="{{ old('nombreTienda') }}">
    </div>

    <div class="form-group">
        <label><b>Descripcion:</b></label><br>
        <textarea class="form-control" name="descripcionTienda" id="descripcionTienda">{{ old('descripcionTienda') }}</textarea>
    </div>

    <div class="form-group">
        <label><b>Direccion:</b></label><br>
        <textarea class="form-control" name="direccionTienda" id="direccionTienda">{{ old('direccionTienda') }}</textarea>
    </div>

    <div class="form-group">
        <label><b>Horarios:</b></label><br>
        <textarea class="form-control" name="horarioTienda" id="horarioTienda">{{ old('horarioTienda') }}</textarea>
        <small id="emailHelp" class="form-text text-muted">Por favor, separe los dias con una coma. Ejemplo: Lunes de 10:00 a 22:00, Martes de 05:00 a 22:00</small>
    </div>

    <div class="form-group">
        <label><b>Valor del domicilio:</b> </label><br>
        <input type="number" class="form-control" name="domicilio">
    </div>

    <div class="form-group">
        <label><b>Departamento</b></label><br>
        <select class="form-control mi-selector" id="departamento" name="departamentoTienda">
            <option value="0" selected="">Seleccionar departamento</option>
            @foreach($departamentos as $departamento)
                <option value="{{ $departamento->id }}">{{ $departamento->nombre_departamento }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label><b>Ciudad</b></label><br>
        <select class="form-control mi-selector" id="ciudad" name="ciudadTienda">
            <option value="" selected="">Seleccione una ciudad</option>
        </select>
    </div>

    <div class="form-group">
        <label><b>Categoria Tienda</b></label><br>
        <select class="form-control mi-selector" id="categoria_tienda" name="categoria_tienda">
            <option value="0" selected="">Categoria de la tienda</option>
            @foreach($categorias_tienda as $categoria_tienda)
                <option value="{{ $categoria_tienda->id }}">{{ $categoria_tienda->nombre_categoria_tienda }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label><b>Zona principal:</b></label><br>
        <label><code><b>(*)</b>Nombre de la zona:</code></label>
        <input type="text" class="form-control" name="nombre_zona" value="{{ old('nombre_zona') }}">
        <label><code>Descripción de la zona:</code></label>
        <input type="text" class="form-control" name="descripcion_zona" value="{{ old('descripcion_zona') }}">
        <label><code><b>(*)</b>Número de WhatsApp de la zona:</code></label>
        <input type="text" class="form-control" name="celular_zona" value="{{ old('celular_zona') }}">
    </div>

    <div class="form-group">
        <label><b>Telefono fijo</b></label><br>
        <input type="number" class="form-control" name="telFijo" id="telFijo" value="{{ old('telFijo') }}">
    </div>

    <div class="form-group">
        <label><b>Link de Facebook</b></label>
        <input type="text" class="form-control" name="linkFb" id="linkFb" value="{{ old('linkFb') }}">
    </div>

    <div class="form-group">
        <label><b>Link de Instagram</b></label><br>
        <input type="text" class="form-control" name="linkIns" id="linkIns" value="{{ old('linkIns') }}">
    </div>

    <div class="form-group">
        <label><b>Link de Twitter</b></label><br>
        <input type="text" class="form-control" name="linkTwitter" id="linkTwitter" value="{{ old('linkTwitter') }}">
    </div>

    <center>
        <a href="{{ url('tienda') }}" style="color:white; ">
            <button type="button" class="btn btn-danger">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="25px" height="25px" class=""><g><g>
                    <g>
                        <path d="M256,0C114.508,0,0,114.497,0,256c0,141.493,114.497,256,256,256c141.492,0,256-114.497,256-256    C512,114.507,397.503,0,256,0z M256,472c-119.384,0-216-96.607-216-216c0-119.385,96.607-216,216-216    c119.384,0,216,96.607,216,216C472,375.385,375.393,472,256,472z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                    </g>
                </g><g>
                    <g>
                        <path d="M343.586,315.302L284.284,256l59.302-59.302c7.81-7.81,7.811-20.473,0.001-28.284c-7.812-7.811-20.475-7.81-28.284,0    L256,227.716l-59.303-59.302c-7.809-7.811-20.474-7.811-28.284,0c-7.81,7.811-7.81,20.474,0.001,28.284L227.716,256    l-59.302,59.302c-7.811,7.811-7.812,20.474-0.001,28.284c7.813,7.812,20.476,7.809,28.284,0L256,284.284l59.303,59.302    c7.808,7.81,20.473,7.811,28.284,0C351.398,335.775,351.397,323.112,343.586,315.302z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                    </g>
                </g></g> </svg>
                Cancelar registro
            </button>
        </a>
        <button type="button" class="btn btn-info" id="btn-registro-tienda">Registrar tienda</button>
    </center>

</form>

<script type="text/javascript">
    
    $("#nombreTiendaUnico").keyup(function(){              
        var ta      =   $("#nombreTiendaUnico");
        letras      =   ta.val().replace(/ /g, "-");
        ta.val(letras)
    });
    
</script>
