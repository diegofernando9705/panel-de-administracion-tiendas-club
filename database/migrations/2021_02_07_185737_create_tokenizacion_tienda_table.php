<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTokenizacionTiendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokenizacion_tienda', function (Blueprint $table) {
            $table->increments('id_tokenizacion');
            $table->integer('code_tienda');
            $table->longText('tk_tienda');
            $table->char('tipo_token', 50);
            $table->integer('estado_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('encryptions');
    }
}
