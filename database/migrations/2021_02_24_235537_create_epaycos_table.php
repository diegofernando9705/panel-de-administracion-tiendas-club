<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEpaycosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epaycos', function (Blueprint $table) {
            $table->id();
            $table->longText('client_id');
            $table->longText('clave_id');
            $table->longText('public_key');
            $table->longText('private_key');
            $table->string('epay_tienda');
            $table->string('estado_epayco');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epaycos');
    }
}
