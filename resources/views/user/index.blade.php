@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                              Módulo Tienda
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">

                            <div class="messaging">
                                <b>Búsqueda de la tienda:</b>

                                <input type="text" name="buscadorTienda" id="buscadorTienda" class="form-control" placeholder="Busca tu producto..." />
                            </div>
                            <br/>
                            <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Rol(es)</th>
                                <th colspan="3"></th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($users as $user)

                            <tr>
                                <th scope="row">{{ $user->id}}</th>
                                <td>{{ $user->name}}</td>
                                <td>{{ $user->email}}</td>
                                <td>
                                    @isset( $user->roles[0]->name )
                                    {{ $user->roles[0]->name}}
                                    @endisset

                                </td>
                                <td> 
                                    @can('view',[$user, ['user.show','userown.show'] ])
                                    <a class="btn btn-info" href="{{ route('user.show',$user->id)}}">Ver</a> 
                                    @endcan
                                </td>  
                                <td> 
                                    @can('view', [$user, ['user.edit','userown.edit'] ])
                                    <a class="btn btn-success" href="{{ route('user.edit',$user->id)}}">Editar</a> 
                                    @endcan
                                </td>  
                                <td> 
                                    @can('haveaccess','user.destroy')
                                    <form action="{{ route('user.destroy',$user->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger">Eliminar</button>
                                    </form>
                                    @endcan


                                </td>  
                            </tr>      
                            @endforeach
                        </tbody>
                    </table>
                     {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modales-tiendas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-body">
        
    </div>
  </div>
</div>

@endsection
