@extends('layouts.app')

@section('content')


<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                              Edicion del usuario
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">

                            <form action="{{ route('user.update', $user->id)}}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="container">
                            <div class="form-group">                            
                                <input type="text" class="form-control" 
                                       id="name" 
                                       placeholder="Name"
                                       name="name"
                                       value="{{ old('name', $user->name)}}"
                                       disabled>
                            </div>
                            <div class="form-group">                            
                                <input type="text" 
                                       class="form-control" 
                                       id="email" 
                                       placeholder="email"
                                       name="email"
                                       value="{{ old('email' , $user->email)}}"
                                       disabled>
                            </div>

                            <div class="form-group">                            
                                <select disabled class="form-control"  name="roles" id="roles">
                                    @foreach($roles as $role)
                                    <option value="{{ $role->id }}"
                                            @isset($user->roles[0]->name)
                                            @if($role->name ==  $user->roles[0]->name)
                                            selected
                                            @endif
                                            @endisset


                                            >{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <hr>

                            <a class="btn btn-success" href="{{route('user.edit',$user->id)}}">Editar</a>
                            <a class="btn btn-danger" href="{{route('user.index')}}">Regresar</a>

                        </div>

                    </form>
                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modales-tiendas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-body">
        
    </div>
  </div>
</div>



@endsection
