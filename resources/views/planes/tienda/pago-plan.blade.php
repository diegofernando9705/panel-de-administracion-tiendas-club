<!DOCTYPE html>
<html>
<head>
	<title>Pago del Plan</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body style="background-color: #121d51a6;">

	<main class="container card div-planes" style="margin-top: 10vh; box-shadow: 9px 10px 32px 0px rgba(0,0,0,0.75);
-webkit-box-shadow: 9px 10px 32px 0px rgba(0,0,0,0.75);
-moz-box-shadow: 9px 10px 32px 0px rgba(0,0,0,0.75);">
	  <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
	    <h1 class="display-4"><img src="http://tiendas.club/web_principal/images/TIENDAS_CLUB.png" width="200px"></h1>
	    <p class="lead"><strong>A continuaci&oacute;n se proceder&aacute; al pago del plan deseado por usted.</strong></p>
	  </div>

	  <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">

	  	@foreach($planes as $plan)
		  	<div class="col-6 col-md-6 col-sm-6 col-lg-6 col-xl-6">
		  		<div class="card mb-4 shadow-sm">
		  			<div class="card-header">
		  				<h4 class="my-0 fw-normal"><strong>{{ $plan->nombre_plan }}</strong></h4>
		  			</div>
		  			<div class="card-body">
		  				<h1 class="card-title pricing-card-title">$ {{ number_format($plan->valor_plan) }} <small class="text-muted">/ mes</small></h1>
		  				<ul class="list-unstyled mt-3 mb-4">
		  					{{ $plan->descripcion_plan }}
		  				</ul>
		  				@if($plan->id_plan == 2)
		  					<button type="button" class="w-100 btn btn-lg btn-outline-primary" disabled="">No disponible</button>
		  				@else
			  				<a href="{{ route('cambio_plan') }}">
			  					<button type="button" class="w-100 btn btn-lg btn-outline-primary" >Adquirir plan</button>
			  				</a>
		  				@endif
		  			</div>
			    </div>
		    </div>
	  	@endforeach
	    
	  </div>

	</main>
</body>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</html>