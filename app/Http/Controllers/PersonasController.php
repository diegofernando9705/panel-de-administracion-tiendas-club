<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Gate;

class PersonasController extends Controller {

    public function index() {
        
        $personas = DB::table('personas')->orderBy('id', 'DESC')->paginate(10);

        return view('personas.index', compact('personas'));
    }

    public function create(){
        $usuarios = DB::table('users')->get();
        $departamentos = DB::table('departamentos')->get();
        return view('personas.crear', compact('departamentos', 'usuarios'));
    }


    public function store(Request $request){

        $request->validate([
            'foto_persona' => 'required|mimes:jpg,jpeg,bmp,png',
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'celular' => 'required',
            'email' => 'required|email',
            'direccion' => 'required',
            'usuario' => 'required',
            'estado' => 'required',
        ]);

        //obtenemos el campo file definido en el formulario
        $file = $request->file('foto_persona');
        //obtenemos el nombre del archivo
        $imagen_persona = $file->getClientOriginalName();
        //obtenemos el nombre del archivo
        $imagen_persona = $file->getClientOriginalName('foto_persona');
        \Storage::disk('public')->put($imagen_persona, \File::get($file));

        DB::table('personas')->insert([
            'foto_persona' => $imagen_persona,
            'primer_nombre' => $request->primer_nombre,
            'segundo_nombre' => $request->segundo_nombre,
            'primer_apellido' => $request->primer_apellido,
            'segundo_apellido' => $request->segundo_apellido,
            'telefono' => $request->celular,
            'correo_electronico' => $request->email,
            'id_ciudad' => $request->ciudad,
            'direccion' => $request->direccion,
            'user_register' => $request->usuario,
            'barrio' => $request->barrio,
            'estado_persona' => $request->estado
        ]);


        return redirect()->route('personas.index')
            ->with('status_success','Registro exitoso!');
    }

    public function edit($id){
        $usuarios = DB::table('users')->get();
        $personas = DB::table('personas')->where('id', $id)->get();

        $departamentos = DB::table('departamentos')->get();
        return view('personas.edicion', compact('departamentos', 'usuarios', 'personas'));
    }

    public function update(Request $request, $id){

        $request->validate([
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'celular' => 'required',
            'email' => 'required|email',
            'direccion' => 'required',
            'usuario' => 'required',
            'estado' => 'required',
        ]);

        if(empty($request->foto_persona)){
            $imagen_persona = $request->imagen_bd;
        }else{
            //obtenemos el nombre del archivo
             //obtenemos el campo file definido en el formulario
            $file = $request->file('foto_persona');
            $imagen_persona = $file->getClientOriginalName('foto_persona');
            \Storage::disk('public')->put($imagen_persona, \File::get($file));
        }

        DB::table('personas')->where('id', $id)->update([
            'foto_persona' => $imagen_persona,
            'primer_nombre' => $request->primer_nombre,
            'segundo_nombre' => $request->segundo_nombre,
            'primer_apellido' => $request->primer_apellido,
            'segundo_apellido' => $request->segundo_apellido,
            'telefono' => $request->celular,
            'correo_electronico' => $request->email,
            'id_ciudad' => $request->ciudad,
            'direccion' => $request->direccion,
            'user_register' => $request->usuario,
            'barrio' => $request->barrio,
            'estado_persona' => $request->estado
        ]);

        return redirect()->route('personas.index')
            ->with('status_success','Actualizado correctamente!');

    }

    public function show($id){
        $usuarios = DB::table('users')->get();
        $personas = DB::table('personas')->where('id', $id)->get();
        $departamentos = DB::table('departamentos')->get();

        $departamentos = DB::table('departamentos')->get();
        return view('personas.ver', compact('departamentos', 'usuarios', 'personas', 'departamentos'));
    }    

    public function perfil() {
        
        $descripcion_perfil = DB::table('personas')->where('user_register', Auth::user()->id)->get();
        $ciudades = DB::table('ciudades')->get();
        

        return view('user.perfil', compact('descripcion_perfil', 'ciudades'));
    }

    public function perfilUpdate(Request $request) {
        
        $request->validate([
            'foto_persona' => 'required|mimes:jpg,jpeg,bmp,png',
            'nombre' => 'required',
            'apellido' => 'required',
            'celular' => 'required',
            'correo' => 'required|email',
            'ciudad' => 'required',
            'direccion' => 'required',
            'barrio' => 'required',
        ]);

        //obtenemos el campo file definido en el formulario
        $file = $request->file('foto_persona');

        //obtenemos el nombre del archivo
        $nombre = $file->getClientOriginalName('foto_persona');
        \Storage::disk('public')->put($nombre, \File::get($file));

        $descripcion_perfil = DB::table('personas')->where('user_register', Auth::user()->id)->get();
        
        DB::table('personas')->where('user_register', Auth::user()->id)->update([
            'foto_persona' => $nombre,
            'primer_nombre' => $request->nombre,
            'segundo_nombre' => $request->segundo_nombre,
            'primer_apellido' => $request->apellido,
            'segundo_apellido' => $request->segundo_apellido,
            'telefono' => $request->celular,
            'correo_electronico' => $request->correo,
            'id_ciudad' => $request->ciudad,
            'direccion' => $request->direccion,
            'barrio' => $request->barrio,
        ]);
        
        return redirect('perfil')->with('status', 'Su perfil se ha actualizado!');
    }

    public function destroy($id){
        
        DB::table('personas')->where('id', $id)->update([
            'estado_persona' => '0'
        ]);

        return redirect()->route('personas.index')
            ->with('status_success','Se elimino correctamente');
    }
}
