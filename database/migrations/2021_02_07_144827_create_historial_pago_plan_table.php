<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialPagoPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_pago_plan', function (Blueprint $table) {
            $table->increments('id_historial_plan');
            $table->char('fecha_pago', 100);
            $table->integer('plan_pago');
            $table->integer('valor_pago');
            $table->char('estado_pago');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_pago_plan');
    }
}
