<!-- Listado de las tiendas, no influye si es Administrador o Cliente -->

@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">

                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="messaging alert alert-danger">
                                Es una funci&oacute;n Premium, por favor, actualiza tu plan.                      
                            </div>
                            <center>
                            	<a href="{{ url('cambio_plan') }}">
                            		<button type="button" class="btn btn-success">Ir a actualizar Plan</button>
                            	</a>
                            </center>
                        </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modales-tiendas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-body">
        
    </div>
  </div>
</div>


@endsection
