
	<form id="formulario-personas">
			@csrf
			<div class="login__header text-center mb-lg-5 mb-4">
				<h3 class="login__title mb-2">
					<img src="https://tiendas.club/web_principal/images/TIENDAS_CLUB.png" width="150px">
				</h3>
				<br>
				<p>Formulario para el Registro de personas - <b>COMPLETAMENTE GRATIS</b>.</p>
				
			</div>
			<div id="success_message"></div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<label style="padding-bottom: 5px;"><b>(*) Primer nombre:</b></label>
						<input type="text" class="form-control login_text_field_bg input-style" placeholder="" name="primer_nombre" value="{{ old('primer_nombre') }}">
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 5px;">
						<label style="padding-bottom: 5px;"><b> Segundo nombre:</b></label>
						<input type="text" class="form-control login_text_field_bg input-style" placeholder="" name="segundo_nombre" value="{{ old('segundo_nombre') }}">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<label style="padding-bottom: 5px;"><b>(*) Primer apellido:</b></label>
						<input type="text" class="form-control login_text_field_bg input-style" placeholder="" name="primer_apellido" value="{{ old('primer_apellido') }}">
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 5px;">
						<label style="padding-bottom: 5px;"><b> Segundo apellido:</b></label>
						<input type="text" class="form-control login_text_field_bg input-style" placeholder="" name="segundo_apellido" value="{{ old('segundo_apellido') }}">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<label style="padding-bottom: 5px;"><b>(*) Celular:</b></label>
						<input type="number" class="form-control login_text_field_bg input-style" placeholder="" name="celular" value="{{ old('celular') }}">
					</div>

					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-top: 5px;">
						<label style="padding-bottom: 5px; "><b>(*) Correo electronico:</b></label>
						<input type="email" class="form-control login_text_field_bg input-style" placeholder="" name="email" value="{{ $usuario[0]->email }}" readonly="">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<label style="padding-bottom: 5px;"><b>(*) Direccion:</b></label>
						<input type="text" class="form-control login_text_field_bg input-style" placeholder="" name="direccion" value="{{ old('direccion') }}">
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" >
						<label style="padding-bottom: 5px; margin-top: 5px;"><b>(*) Barrio:</b></label>
						<input type="text" class="form-control login_text_field_bg input-style" placeholder="" name="barrio" value="{{ old('barrio') }}">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<label style="padding-bottom: 5px;"><b>(*) Departamento:</b></label>
						<select class="form-control mi-selector" id="departamento" name="departamento">
								<option value="0" selected="">Seleccionar departamento</option>
							@foreach($departamentos as $departamento)
								<option value="{{ $departamento->id }}">{{ $departamento->nombre_departamento }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<label style="padding-bottom: 5px; margin-top: 5px;"><b>(*) Ciudad:</b></label>
						<select class="form-control mi-selector" id="ciudad" name="ciudad" required="">
							<option value="" selected="">Seleccione una ciudad</option>
						</select>
					</div>
				</div>
			</div>


			<center>
				<button type="button" class="btn btn-info" id="btn-registro-persona">Continuar</button>
			</center>
	</form>
