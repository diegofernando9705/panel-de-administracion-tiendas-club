@extends('layouts.app')

@section('content')


<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                                Crear un nuevo producto
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                        {!! Form::open(['route' => 'productos.store', 'files' => true]) !!}

                    @csrf
                    @method('POST')


                    <div class="form-group">
                        <label><strong><code>(*) </code>Imagen producto: </strong></label><br>
                            <input type="file" class="form-control" name="imagen_producto" id="telMovil" />
                    </div>

                    <div class="form-group">
                        <label><strong><code>(*) </code>Nombre producto: </strong></label>
                        <input type="text" class="form-control" name="nombre_producto" value="{{ old('nombre_categoria') }}">
                    </div>

                    <div class="form-group">
                        <label>Descripción del producto:</label>
                        <input type="text" class="form-control" name="descripcion_producto" value="{{ old('descripcion_categoria') }}">
                    </div>
                    
                    <div class="form-group">
                        <label><strong><code>(*) </code>Valor del producto: </strong></label><br>
                            <input type="number" class="form-control" name="valor_producto" id="telMovil" />
                    </div>
                    
                    <div class="form-group">
                        <label><strong><code>(*) </code>Categoria del Producto: </strong></label><br>
                            <select class="form-control mi-selector" name="categoria_producto">
                                <option value="" selected="">Selecciona una categoria del producto</option>
                                @foreach($categorias as $categoria)
                                    <?php if (is_array($array_categoria) && in_array($categoria->id, $array_categoria)) { ?>
                                        <option value="{{ $categoria->id }}">{{ $categoria->nombre_categoria }}</option>
                                    <?php } ?>


                                    <?php if (is_array($codigo_temporal) && in_array($categoria->id, $codigo_temporal)) { ?>
                                        <option value="{{ $categoria->id }}">{{ $categoria->nombre_categoria }}</option>
                                    <?php } ?>

                                @endforeach
                            </select>
                    </div>

                    <div class="form-group">
                        <label><strong><code></code>Adicionales de los Producto: </strong></label><br>
                            <select class="form-control mi-selector" multiple="" name="adicionales_productos[]">
                                    @foreach($adicionales as $adicion)
                                        <?php if (is_array($detalle_adicionales_tienda) && in_array($adicion->id_unico, $detalle_adicionales_tienda)) { ?>
                                            <option value="{{ $adicion->id_unico }}">{{ $adicion->nombre_adicional }}</option>
                                        <?php } ?>
                                    @endforeach
                            </select>
                    </div>

                    <div class="form-group">
                        <label><strong><code>(*) </code>Estado: </strong></label><br>
                            <select class="form-control mi-selector" name="estado_producto">
                                <option value="1">Activa</option>
                                <option value="0">Inactiva</option>
                            </select>
                    </div>

                    <center>
                        <a href="{{ url('tienda') }}" style="color:white; ">
                            <button type="button" class="btn btn-danger">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="25px" height="25px" class=""><g><g>
                                            <g>
                                                <path d="M256,0C114.508,0,0,114.497,0,256c0,141.493,114.497,256,256,256c141.492,0,256-114.497,256-256    C512,114.507,397.503,0,256,0z M256,472c-119.384,0-216-96.607-216-216c0-119.385,96.607-216,216-216    c119.384,0,216,96.607,216,216C472,375.385,375.393,472,256,472z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                        </g><g>
                                            <g>
                                                <path d="M343.586,315.302L284.284,256l59.302-59.302c7.81-7.81,7.811-20.473,0.001-28.284c-7.812-7.811-20.475-7.81-28.284,0    L256,227.716l-59.303-59.302c-7.809-7.811-20.474-7.811-28.284,0c-7.81,7.811-7.81,20.474,0.001,28.284L227.716,256    l-59.302,59.302c-7.811,7.811-7.812,20.474-0.001,28.284c7.813,7.812,20.476,7.809,28.284,0L256,284.284l59.303,59.302    c7.808,7.81,20.473,7.811,28.284,0C351.398,335.775,351.397,323.112,343.586,315.302z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                        </g></g> </svg>
                                Cancelar registro
                            </button>
                        </a>

                        <button type="submit" class="btn btn-info" id="registroTienda">Registrar producto</button>
                    </center>
                    {!! Form::close() !!}
                </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>

@endsection

