<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Panel de administración - Tiendas Club</title>
        <!--<link rel="stylesheet" href="{{ asset('bootstrap4/css/bootstrap.css') }}"> --
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        -->
        <!-- Template CSS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
        <link href="{{  asset('style-template/css/style.css') }}" rel="stylesheet" />
        <link href="{{  asset('style-template/css/style-personalizados.css') }}" rel="stylesheet" />
        <link href="{{ asset('style-template/css/font-awesome.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('style-template/css/tarjetas_creditos.css') }}" type="text/css" media="all" />

        <!-- google fonts -->
        <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet" />
    </head>

    <body>
        
    <section>

         <!-- sidebar menu start -->
        <div class="sidebar-menu sticky-sidebar-menu">
            <!-- logo start -->
            <div class="logo" style="padding-top: 10px; padding-bottom: 70px;">
                <h1>
                    <a href="index.html">
                        <img alt="logo-icon" src="https://tiendas.club/web_principal/images/TIENDAS_CLUB.png" style="padding-left: 10px; padding-right: 10px; width: 100px;"/>
                    </a>
                </h1>
            </div>
            <div class="logo-icon text-center">
                <a href="index.html" title="logo">
                    <img alt="logo-icon" src="https://tiendas.club/web_principal/images/TIENDAS_CLUB.png" />
                </a>
            </div>

            <div class="sidebar-menu-inner">
                <!-- sidebar nav start -->
                <ul class="nav nav-pills nav-stacked custom-nav">
                    <li class="{{ Request::path() == 'home' ? ' nav-active' : '' }} {{ Request::path() == '/' ? ' nav-active' : '' }}">
                        <a href="{{ url('/home') }}">
                            <i class="fa fa-tachometer">
                            </i>
                            <span>
                                Principal
                            </span>
                        </a>
                    </li>
                    <li class="menu-list {{ Request::path() == 'tiendas' ? ' nav-active' : '' }} {{ Request::path() == 'tiendas/create' ? ' nav-active' : '' }} {{ Request::path() == 'tiendas/listado' ? ' nav-active' : '' }}">
                        
                        <a href="#">
                            <i class="fa fa-cogs">
                            </i>
                            <span>
                                Tiendas
                                <i class="lnr lnr-chevron-right">
                                </i>
                            </span>
                        </a>
                        @can('haveaccess','tiendas.create')
                        <ul class="sub-menu-list">
                            <li>
                                <a href="{{ route('tiendas.create')}}" class="nav-link">
                                    Crear
                                </a>
                            </li>
                        </ul>                        
                        @endcan

                        @can('haveaccess','tiendas.index')
                        <ul class="sub-menu-list">
                            <li>
                                <a href="{{ url('tiendas/listado')}}" class="nav-link">
                                    Listar
                                </a>
                            </li>
                        </ul>
                         @endcan
                    </li>

                    <li class="menu-list {{ Request::path() == 'zonas' ? ' nav-active' : '' }} {{ Request::path() == 'zonas/create' ? ' nav-active' : '' }} {{ Request::path() == 'zonas/listado' ? ' nav-active' : '' }}">
                        
                        <a href="#">
                            <i class="fa fa-cogs">
                            </i>
                            <span>
                                Zonas de tiendas
                                <i class="lnr lnr-chevron-right">
                                </i>
                            </span>
                        </a>

                        @can('haveaccess','zonas.create')
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="{{ route('zonas.create')}}" class="nav-link">
                                        Crear
                                    </a>
                                </li>
                            </ul>                        
                        @endcan
                            
                        @can('haveaccess','zonas.show')
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="{{ route('zonas.index')}}" class="nav-link">
                                        Listar
                                    </a>
                                </li>
                            </ul>
                        @endcan
                    </li>

                    <!--
                    <li class="menu-list">
                        <a href="#">
                            <i class="fa fa-cogs">
                            </i>
                            <span>
                                Categoria tiendas
                                <i class="lnr lnr-chevron-right">
                                </i>
                            </span>
                        </a>
                        <ul class="sub-menu-list">
                            
                        </ul>
                    </li>-->
                    <li class="menu-list {{ Request::path() == 'productos' ? ' nav-active' : '' }} {{ Request::path() == 'productos/create' ? ' nav-active' : '' }}">
                        
                        <a href="#">
                            <i class="fa fa-th">
                            </i>
                            <span>
                                Productos
                                <i class="lnr lnr-chevron-right">
                                </i>
                            </span>
                        </a>
                        <ul class="sub-menu-list">
                            @can('haveaccess','productos.create')
                            <li>
                                <a href="{{ route('productos.create') }}">
                                    Crear
                                </a>
                            </li>
                            @endcan
                            @can('haveaccess','productos.show')
                            <li>
                                <a href="{{ route('productos.index') }}">
                                    Listar
                                </a>
                            </li>
                            @endcan
                        </ul>
                        
                    </li>

                    <li class="menu-list {{ Request::path() == 'categoriasproductos' ? ' nav-active' : '' }} {{ Request::path() == 'categoriasproductos/create' ? ' nav-active' : '' }}">
                        <a href="#">
                            <i class="fa fa-th">
                            </i>
                            <span>
                                Categorias productos
                                <i class="lnr lnr-chevron-right">
                                </i>
                            </span>
                        </a>
                        <ul class="sub-menu-list">
                            @can('haveaccess','categoryProductos.create')
                            <li>
                                <a href="{{ route('categoriasproductos.create') }}">
                                    Crear
                                </a>
                            </li>
                            @endcan
                            @can('haveaccess','categoryProductos.show')
                            <li>
                                <a href="{{ route('categoriasproductos.index') }}">
                                    Listar
                                </a>
                            </li>
                            @endcan
                        </ul>
                    </li>

                    <li class="menu-list {{ Request::path() == 'adicionales' ? ' nav-active' : '' }} {{ Request::path() == 'adicionales/listar' ? ' nav-active' : '' }} {{ Request::path() == 'adicionales/crear' ? ' nav-active' : '' }}">
                        <a href="#">
                            <i class="fa fa-th">
                            </i>
                            <span>
                                Adicionales
                                <i class="lnr lnr-chevron-right">
                                </i>
                            </span>
                        </a>
                        <ul class="sub-menu-list">
                            @can('haveaccess','adicionales.create')
                            <li>
                                <a href="{{ route('adicionales.crear') }}">
                                    Crear
                                </a>
                            </li>
                            @endcan
                            @can('haveaccess','adicionales.show')
                            <li>
                                <a href="{{ route('adicionales.index') }}">
                                    Listar
                                </a>
                            </li>
                            @endcan
                        </ul>
                    </li>


                    <li class="menu-list {{ Request::path() == 'sugerencias' ? ' nav-active' : '' }}">
                        <a href="#">
                            <i class="fa fa-th">
                            </i>
                            <span>
                                Formularios
                                <i class="lnr lnr-chevron-right">
                                </i>
                            </span>
                        </a>
                        <ul class="sub-menu-list">
                            @can('haveaccess','adicionales.show')
                            <li>
                                <a href="{{ route('sugerencias.index') }}">
                                    Buz&oacute;n de sugerencias
                                </a>
                            </li>
                            @endcan
                            @can('haveaccess','contacto.index')
                            <li>
                                <a href="{{ route('contacto.index') }}">
                                    Formulario de contacto
                                </a>
                            </li>
                            @endcan
                        </ul>
                    </li>

                    <hr>
                    <!--
                    <li class="menu-list">
                        <a href="#">
                            <i class="fa fa-th">
                            </i>
                            <span>
                                Planes Mensuales
                                <i class="lnr lnr-chevron-right">
                                </i>
                            </span>
                        </a>
                        
                    </li>-->
                    <li class="active">
                        <a href="#">
                            <i class="fa fa-tachometer">
                            </i>
                            <span>
                                Configuracion Avanzada
                            </span>
                        </a>
                        

                        <li class="menu-list {{ Request::path() == 'cupones' ? ' nav-active' : '' }} {{ Request::path() == 'cupones/create' ? ' nav-active' : '' }}">
                            
                            <a href="#">
                                <i class="fa fa-cogs">
                                </i>
                                <span>
                                    Cupones
                                    <i class="lnr lnr-chevron-right">
                                    </i>
                                </span>
                            </a>
                            <ul class="sub-menu-list">
                                @can('haveaccess','cupones.create')
                                <li>
                                    <a href="{{route('cupones.create')}}" class="nav-link">
                                        Crear
                                    </a>
                                </li>
                                @endcan
                            </ul>
                            <ul class="sub-menu-list">
                                @can('haveaccess','cupones.show')
                                <li>
                                    <a href="{{route('cupones.index')}}" class="nav-link">
                                        Listar
                                    </a>
                                </li>
                                @endcan
                            </ul>
                        </li>


                        <li class="menu-list {{ Request::path() == 'planes' ? ' nav-active' : '' }} {{ Request::path() == 'planes/create' ? ' nav-active' : '' }}  {{ Request::path() == 'asignaciones' ? ' nav-active' : '' }}">
                            @can('haveaccess','planes.index')
                            <a href="#">
                                <i class="fa fa-cogs">
                                </i>
                                <span>
                                    Planes
                                    <i class="lnr lnr-chevron-right">
                                    </i>
                                </span>
                            </a>
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="{{route('planes.create')}}" class="nav-link">
                                        Crear
                                    </a>
                                </li>
                            </ul>
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="{{route('planes.index')}}" class="nav-link">
                                        Listar
                                    </a>
                                </li>
                            </ul>
                             <ul class="sub-menu-list">
                                <li>
                                    <a href="{{route('planes.asignaciones')}}" class="nav-link">
                                        Listado de asignaciones
                                    </a>
                                </li>
                            </ul>
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="{{route('planes.asignar')}}" class="nav-link">
                                        Asignar plan
                                    </a>
                                </li>
                            </ul>
                            @endcan
                        </li>


                        <li class="menu-list">

                            <a href="#">
                                <i class="fa fa-cogs">
                                </i>
                                <span>
                                    Historial de Pedidos
                                    <i class="lnr lnr-chevron-right">
                                    </i>
                                </span>
                            </a>
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="{{route('tiendas.historial')}}" class="nav-link">
                                        Listar
                                    </a>
                                </li>
                            </ul>

                        </li>

                        <li class="menu-list {{ Request::path() == 'personas' ? ' nav-active' : '' }} {{ Request::path() == 'personas/create' ? ' nav-active' : '' }}">
                            @can('haveaccess','role.index')
                            <a href="#">
                                <i class="fa fa-cogs">
                                </i>
                                <span>
                                    Modulo Personas
                                    <i class="lnr lnr-chevron-right">
                                    </i>
                                </span>
                            </a>
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="{{route('personas.create')}}" class="nav-link">
                                        Crear
                                    </a>
                                </li>
                            </ul>
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="{{route('personas.index')}}" class="nav-link">
                                        Listar
                                    </a>
                                </li>
                            </ul>
                            @endcan
                        </li>

                        <li class="menu-list {{ Request::path() == 'user' ? ' nav-active' : '' }} {{ Request::path() == 'user/show' ? ' nav-active' : '' }} {{ Request::path() == 'user/edit' ? ' nav-active' : '' }}">
                            @can('haveaccess','user.index')
                            <a href="#">
                                <i class="fa fa-cogs">
                                </i>
                                <span>
                                    Modulo de Usuarios
                                    <i class="lnr lnr-chevron-right">
                                    </i>
                                </span>
                            </a>
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="{{route('user.index')}}" class="nav-link">
                                        Listar
                                    </a>
                                </li>
                            </ul>
                            @endcan
                        </li>

                        <li class="menu-list {{ Request::path() == 'role' ? ' nav-active' : '' }}">
                            @can('haveaccess','role.index')
                            <a href="#">
                                <i class="fa fa-cogs">
                                </i>
                                <span>
                                    Modulo Roles
                                    <i class="lnr lnr-chevron-right">
                                    </i>
                                </span>
                            </a>
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="{{route('role.index')}}" class="nav-link">
                                        Listar
                                    </a>
                                </li>
                            </ul>
                            @endcan
                        </li>


                        <li class="menu-list {{ Request::path() == 'epayco' ? ' nav-active' : '' }} {{ Request::path() == 'epayco/create' ? ' nav-active' : '' }}">
                            
                            <a href="#">
                                <i class="fa fa-cogs">
                                </i>
                                <span>
                                    Epayco
                                    <i class="lnr lnr-chevron-right">
                                    </i>
                                </span>
                            </a>
                            <ul class="sub-menu-list">
                                @can('haveaccess','epayco.index')
                                <li>
                                    <a href="{{route('epayco.create')}}" class="nav-link">
                                        Crear
                                    </a>
                                </li>
                                @endcan
                            </ul>
                            <ul class="sub-menu-list">
                                @can('haveaccess','epayco.create')
                                <li>
                                    <a href="{{route('epayco.index')}}" class="nav-link">
                                        Listar
                                    </a>
                                </li>
                                @endcan
                            </ul>
                        </li>


                        <li class="">
                            <a href="{{ route('planes.cambio_plan') }}" class="update-plan">
                                <span>
                                    Actualizar plan actual
                                </span>
                            </a>
                        </li>



                        <!--
                        <li class="menu-list">
                            <a href="#">
                                <i class="fa fa-cogs">
                                </i>
                                <span>
                                    Permisos usuarios
                                    <i class="lnr lnr-chevron-right">
                                    </i>
                                </span>
                            </a>
                            <ul class="sub-menu-list">
                                <li>
                                    <a href="cards.html">
                                        Asignar
                                    </a>
                                </li>
                                <li>
                                    <a href="carousels.html">
                                        Listar
                                    </a>
                                </li>
                            </ul>
                        </li>-->
                    </li>
                </ul>
                <!-- //sidebar nav end -->
                <!-- toggle button start -->
                <a class="toggle-btn">
                    <i class="fa fa-angle-double-left menu-collapsed__left">
                        <span>
                            Cerrar
                        </span>
                    </i>
                    <i class="fa fa-angle-double-right menu-collapsed__right">
                    </i>
                </a>
                <!-- //toggle button end -->
            </div>
        </div>

        <div class="header sticky-header">
            <!-- notification menu start -->
            <div class="menu-right">
                <div class="navbar user-panel-top">
                    <div class="search-box">
                        <form action="#search-results.html" method="get">
                            <input class="search-input" id="search" placeholder="Busqueda rapida..." type="search" />
                                <button class="search-submit" value="">
                                    <span class="fa fa-search">
                                    </span>
                                </button>
                        </form>
                    </div>
                    <div class="user-dropdown-details d-flex">
                        <div class="profile_details_left">
                            <ul class="nofitications-dropdown">
                                <div id="nofitications-dropdown"></div>
                            </ul>
                        </div>
                        <div class="profile_details">
                            <ul>
                                <li class="dropdown profile_details_drop">
                                    <a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownMenu3">
                                        <div class="profile_img">
                                            <img alt="logo-icon" class="rounded-circle" src="https://tiendas.club/web_principal/images/TIENDAS_CLUB.png" />
                                            <div class="user-active">
                                                <span>
                                                </span>
                                            </div>
                                        </div>
                                    </a>
                                    <ul aria-labelledby="dropdownMenu3" class="dropdown-menu drp-mnu">
                                        <li class="user-info">
                                            <h5 class="user-name">
                                                {{ auth()->user()->name }}
                                            </h5>
                                            <span class="status ml-2">
                                                {{ auth()->user()->name }}
                                            </span>
                                        </li>
                                        <li>
                                            <a href="{{ route('perfil.show', auth()->user()->id) }}">
                                                <i class="lnr lnr-user">
                                                </i>
                                                Mi perfil
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="lnr lnr-users">
                                                </i>
                                                Seguridores
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="lnr lnr-cog">
                                                </i>
                                                Configuracion
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="lnr lnr-heart">
                                                </i>
                                                100 Likes
                                            </a>
                                        </li>
                                        <li class="logout">
                                            <a href="{{ url('/logout') }}">
                                                <i class="fa fa-power-off">
                                                </i>
                                                Cerrar sesión
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div> <!-- FIN user-dropdown-details d-flex -->
                </div>
            </div>
            <!--notification menu end -->
        </div>

        <main class="">
            @yield('content')
        </main>

    </section>
    
    @yield('scripts')

    <button class="bg-primary" id="movetop" onclick="topFunction()" title="Go to top">
        <span class="fa fa-angle-up"></span>
    </button>

    <script>

          window.onscroll = function () {
            scrollFunction()
          };

          function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
              document.getElementById("movetop").style.display = "block";
            } else {
              document.getElementById("movetop").style.display = "none";
            }
          }


          function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
          }
    </script>

    
    <script src="{{ asset('style-template/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('style-template/js/jquery-1.10.2.min.js') }}"></script>
    <script src="{{ asset('style-template/js/Chart.min.js') }}"></script>
    <script src="{{ asset('style-template/js/utils.js') }}"></script>
    <script src="{{ asset('style-template/js/bar.js') }}"></script>
    <script src="{{ asset('style-template/js/linechart.js') }}"></script>
    <script src="{{ asset('style-template/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset('style-template/js/scripts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset('style-template/js/actions.js') }}"></script>
    <script src="{{ asset('style-template/js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://demo.tutorialzine.com/2016/11/simple-credit-card-validation-form/assets/js/jquery.payform.min.js" charset="utf-8"></script>
    <script src="{{ asset('bootstrap4/js/acciones.js') }}"></script>

    @yield('scripts')

    <script>
        var closebtns = document.getElementsByClassName("close-grid");
        var i;

        for (i = 0; i < closebtns.length; i++) {
            closebtns[i].addEventListener("click", function () {
              this.parentElement.style.display = 'none';
            });
          }
    </script>

    <script>
        $(function () {
            $('.sidebar-menu-collapsed').click(function () {
                $('body').toggleClass('noscroll');
            })
        });
    </script>

    <script src="{{ asset('style-template/js/modernizr.js') }}"></script>

    <script>
        $(window).load(function () {
            $(".se-pre-con").fadeOut("slow");;
        });
    </script>

    <script type="text/javascript">
        
        $(document).ready(function(){
            
            setInterval( function(){

                $.ajax({
                    type: 'get',
                    url: '/notification/get/',
                    success: function (data) {
                        document.getElementById("nofitications-dropdown").innerHTML = data;
                    }
                });

            }, 3000);

        });

    </script>

    </body>
</html>
