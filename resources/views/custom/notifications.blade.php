<li class="dropdown">
                                    <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-bell-o">
                                        </i>
                                        <span class="badge blue" id="cantidad-notificaciones">
                                            {{ $notification }}
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <div class="contenedor-notificaciones" id="cuerpo-notificaciones">
                                        <li>
                                            <div class="notification_header" id="header-notificaciones">
                                                <h3>
                                                    Tienes {{ $notification }} notificaciones
                                                </h3>
                                            </div>
                                        </li>
                                        <?php foreach($descripcion_notificaciones as $notificaciones) { ?> 
                                        	@if($notificaciones->seccion_notificaciones == 'formulario-sugerencia')
                                        		<li>
		                                            <a class="grid" href="{{ url('/notification/show/'.$notificaciones->id_notificaciones) }}">
		                                                <div class="user_img">
		                                                    <img alt="" src="https://www.vippng.com/png/detail/353-3539905_estrella-png-estrellita-png.png" width="50px" />
		                                                </div>
		                                                <div class="notification_desc" id="cuerpo-notificaciones">
		                                                    <p>
		                                                        Se ha registrado una sugerencia
		                                                    </p>
		                                                    <span>
		                                                        {{ $notificaciones->creacion }}
		                                                    </span>
		                                                </div>
		                                            </a>
		                                        </li>
		                                    @elseif($notificaciones->seccion_notificaciones == 'formulario-contacto')
                                        		<li>
		                                            <a class="grid" href="{{ url('/notification/show/'.$notificaciones->id_notificaciones) }}">
		                                                <div class="user_img">
		                                                    <img alt="" src="https://img.icons8.com/ios/452/form.png" width="50px" />
		                                                </div>
		                                                <div class="notification_desc" id="cuerpo-notificaciones">
		                                                    <p>
		                                                        Se ha registrado un nuevo formulario de contacto
		                                                    </p>
		                                                    <span>
		                                                        {{ $notificaciones->creacion }}
		                                                    </span>
		                                                </div>
		                                            </a>
		                                        </li>
		                                    @else
                                        	@endif
                                        <?php }?>
                                        </div> 
                                        <li>
                                            <div class="notification_bottom">
                                                <a class="bg-primary" href="#all">
                                                    Ver m&aacute;s notificaciones
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-comment-o">
                                        </i>
                                        <span class="badge blue">
                                            0
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="notification_header">
                                                <h3>
                                                    Sin mensajes
                                                </h3>
                                            </div>
                                        </li>
                                        <li>
                                        	<!--
                                            <a class="grid" href="#">
                                                <div class="user_img">
                                                    <img alt="" src="assets/images/avatar1.jpg"/>
                                                </div>
                                                <div class="notification_desc">
                                                    <p>
                                                        Johnson purchased template
                                                    </p>
                                                    <span>
                                                        Just Now
                                                    </span>
                                                </div>
                                            </a>-->
                                        </li>
                                        <li class="odd">
                                        	<!--
                                            <a class="grid" href="#">
                                                <div class="user_img">
                                                    <img alt="" src="assets/images/avatar2.jpg"/>
                                                </div>
                                                <div class="notification_desc">
                                                    <p>
                                                        New customer registered
                                                    </p>
                                                    <span>
                                                        1 hour ago
                                                    </span>
                                                </div>
                                            </a>-->
                                        </li>
                                       
                                        <li>
                                            <div class="notification_bottom">
                                                <a class="bg-primary" href="#all">
                                                    Ver mas mensajes
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>