<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Gate;
use DateTime;
use App\Epayco;
use Illuminate\Support\Facades\Crypt;

class PlanesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        Gate::authorize('haveaccess','planes.index');

        $planes = DB::table('tabla_planes')->get();

        return view('planes.index', compact('planes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        Gate::authorize('haveaccess','planes.create');

        return view('planes.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Gate::authorize('haveaccess','planes.create');

        $request->validate([
            'nombre_plan' => 'required',
            'valor_plan' => 'required',
            'estado_plan' => 'required'
        ]);

        DB::table('tabla_planes')->insert([
            'nombre_plan' => $request->nombre_plan,
            'descripcion_plan' => $request->descripcion_plan,
            'valor_plan' => $request->valor_plan,
            'estado_plan' => $request->estado_plan
        ]);

        return redirect()->route('planes.index')
            ->with('status_success','Creación exitosa!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Gate::authorize('haveaccess','planes.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        Gate::authorize('haveaccess','planes.edit');

        $plan = DB::table('tabla_planes')->where('id_plan', $id)->get();

        return view('planes.edicion', compact('plan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        Gate::authorize('haveaccess','planes.edit');


        $request->validate([
            'nombre_plan' => 'required',
            'valor_plan' => 'required',
            'estado_plan' => 'required'
        ]);

        DB::table('tabla_planes')->where('id_plan', $id)->update([
            'nombre_plan' => $request->nombre_plan,
            'descripcion_plan' => $request->descripcion_plan,
            'valor_plan' => $request->valor_plan,
            'estado_plan' => $request->estado_plan
        ]);

        return redirect()->route('planes.index')
            ->with('status_success','Actualización exitosa!'); 
    }
    
    public function destroy($id)
    {   

        Gate::authorize('haveaccess','planes.destroy');


        DB::table('tabla_planes')->where('id_plan', $id)->update([
            'estado_plan' => '0'
        ]);

        return redirect()->route('planes.index')
            ->with('status_success','Borrado exitoso!'); 
    }

    public function asignar(){
        $planes = DB::table('tabla_planes')->get();
        $tiendas = DB::table('tiendas')->get();


        return view('planes.asignar_tienda', compact('planes', 'tiendas'));
    }

    public function asignar_store(Request $request){
        
        Gate::authorize('haveaccess','planes.create');


        $request->validate([
            'plan' => 'required',
            'tienda' => 'required',
            'fecha_pago' => 'required'
        ]);

        $date = $request->fecha_pago;

        $fecha_15 = strtotime($date."+ 1 month");

        $resultado = DB::table('detalle_planes_tiendas')->where([['id_detalle_plan', $request->plan], ['id_detalle_plan_tienda', $request->tienda], ['estado_detalle_plan', '1']])->count();


        if($resultado > 0){
            
            return redirect()->route('planes.index')
            ->with('status_success','Ya hay un plan vigente con esta tienda!');

        }else{

            DB::table('detalle_planes_tiendas')->insert([
                'id_detalle_plan' => $request->plan,
                'id_detalle_plan_tienda' => $request->tienda,
                'fecha_pago' => $date,
                'fecha_proximo_pago' => date("Y-m-d", $fecha_15),
                'estado_detalle_plan' => '1'
            ]);

            return redirect()->route('planes.index')
            ->with('status_success','Asignacion exitosa!');

        } 
    }


    public function asignaciones(){

        Gate::authorize('haveaccess','planes.create');


        $asignaciones = DB::table('detalle_planes_tiendas')
                        ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
                        ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
                        ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
                        ->get();

        return view('planes.listado', compact('asignaciones'));
    }

    public function cambio_plan(){
       if (isset(Auth::user()->id)){
        date_default_timezone_set('America/Bogota');

        $informacion = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)
                                ->get();

        
        $detalles_del_plan = DB::table('detalle_planes_tiendas')
                            ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
                            ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
                            ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
                            ->where([['id_detalle_plan_tienda', $informacion[0]->id_tienda], ['estado_detalle_plan', '1']])
                            ->get();

        $planes = DB::table('tabla_planes')->get();

        $date1 = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);
        $date2 = new DateTime(date('d-m-Y'));
        $diff = $date1->diff($date2);
        // will output 2 days
        $dias =  $diff->days;

        return view('planes.cambio_plan', compact('detalles_del_plan', 'dias', 'planes'));
       }else{
        return "Error de permisos";
       }
        
    }

    public function metodoPagoActualizacion(Request $request){

        $request->validate([
            'plan' => 'required',
            'metodo' => 'required'
        ]);

        if($request->metodo == 'epayco'){
            $respuesta = DB::table('cliente_epayco')->where('id_user_tiendasclub', Auth::user()->id)->count();

            if($respuesta == 0){
                $info_epayco = Epayco::where('epay_tienda', 'aplicativo')->get();

                /* SE AUTENTICA PARA API */

                    $data = array(
                            'public_key' => Crypt::decrypt($info_epayco[0]->public_key), 
                            'private_key' => Crypt::decrypt($info_epayco[0]->private_key)
                        );                                                          
            
                    $data_string = json_encode($data);                                                                                   
                    $ch = curl_init('https://api.secure.payco.co/v1/auth/login');               
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                     
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                                'Content-Type: application/json'                                                                     
                    ));

                    $result = curl_exec($ch);
                    curl_close($ch);

                    
                    $data = json_decode($result, true);
 
                    if ($data['status'] == 1){
                                               
                        $persona =  DB::table('personas')->where('user_register', Auth::user()->id)->get();

                        return view('planes.formulario_actualizacion', compact('persona'));

                    }else{
                        echo "<div class='alert alert-danger'>".$data['message']."</div> <br> <center><button type='button' class='btn btn-success refrescar'>Regresar</button></center>";
                    };

                    return "";   
                    
                /* FIN AUTENTICACION API */

                print_r($info_epayco);
                return "";
            }else{
                return "ees";    
            }
            
        }else{

        	$persona =  DB::table('personas')->where('user_register', Auth::user()->id)->get();
        	 $url = "https://production.wompi.co/v1/merchants/".config('app.wompi_public');


            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            //for debug only!
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $resp = curl_exec($curl);
            curl_close($curl);

            $data = json_decode($resp, true);
      
            $accepted_payment_methods = $data['data']['accepted_payment_methods'];

        	// Veifico si registro un token para realizar compras
        	$respuesta = DB::table('cliente_epayco')->where('id_user_tiendasclub', Auth::user()->id)->count();
        
        	if($respuesta == 0){
        		$planes = DB::table('tabla_planes')->where('id_plan', $request->plan)->get();
        		return view('planes.formulario_actualizacion_wompi', compact('accepted_payment_methods', 'persona', 'planes'));
        	}else{
        	}
        }
    }

    public function metodoUpdatePay(Request $request, $valor){
        if($valor == 'tarjetas'){
            return view('planes.conf.formulario_tarjetas');
        }else{
        	return view('planes.conf.formulario_pse');
        }
    }

    public function metodoUpdatePayNequi(Request $request, $valor){
        if($valor == 'tarjetas-nequi'){
            return view('planes.conf.nequi.formulario_tarjetas');
        }else if($valor == 'nequi'){
        	return view('planes.conf.nequi.formulario_nequi');
        }else{

        }
    }

	public function pagoActualizacionPlan(Request $request){
         
        if($request->metodo == 'tarjetas-nequi'){

        	$request->validate([
	        	'nombre' => 'required',
				'email_wompi' => 'required',
				'telefono' => 'required',
				'nombre_tarjeta' => 'required',
				'numero_tarjeta' => 'required',
				'cvv_tarjeta' => 'required',
				'mes_tarjeta' => 'required',
				'ano_tarjeta' => 'required',
				'cuotas' => 'required',
                'code_plan' => 'required',
	        ]);

        	$cards = array( 
        		"visa" => "(4\d{12}(?:\d{3})?)", 
        		"amex" => "(3[47]\d{13})", 
        		"jcb" => "(35[2-8][89]\d\d\d{10})", 
        		"maestro" => "((?:5020|5038|6304|6579|6761)\d{12}(?:\d\d)?)", 
        		"solo" => "((?:6334|6767)\d{12}(?:\d\d)?\d?)", 
        		"mastercard" => "(5[1-5]\d{14})", 
        		"switch" => "(?:(?:(?:4903|4905|4911|4936|6333|6759)\d{12})|(?:(?:564182|633110)\d{10})(\d\d)?\d?)", 
        	); 

        	$names = array("Visa", "American Express", "JCB", "Maestro", "Solo", "Mastercard", "Switch"); 
        	$matches = array(); 
        	$pattern = "#^(?:".implode("|", $cards).")$#"; 
        	$result = preg_match($pattern, str_replace(" ", "", $request->numero_tarjeta), $matches); 

        	if(false && $result > 0){ 
        		$result = (validatecard($request->numero_tarjeta))?1:0; 
        	}


        	if($result == 0){
        		return $result;
        	}else{


            /* CAPTURAR TOKEN TERMINOS Y CONDICIONES */
                $url = "https://production.wompi.co/v1/merchants/".config('app.wompi_public');
                


                $nombre_cliente = $request->nombre;
                $email_wompi = $request->email_wompi;
                $telefono = $request->telefono;
                $valor_pago = $request->valor_pago;
                

                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

                $resp = curl_exec($curl);
                curl_close($curl);

                $data = json_decode($resp, true);


                /* SE GUARDA VARIABLE DE TOKEN*/
                $acceptance_token = $data['data']['presigned_acceptance']['acceptance_token'];

            /* FIN CAPTURAR TOKEN TERMINOS Y CONDICIONES */


            /* SE CAPTURA EL TOKEN DE ACEPTACION DE TARJETAS DE CREDITO PARA REALIZAR LA TRANSACCION*/
                
                $numero_tarjeta = str_replace(' ', '', $request->numero_tarjeta);

                $data = array(
                            'number' => $numero_tarjeta, 
                            'exp_month' => $request->mes_tarjeta,
                            'exp_year' => $request->ano_tarjeta, 
                            'cvc' => $request->cvv_tarjeta, 
                            'card_holder' => $request->nombre_tarjeta
                        );                                                          
            
                $data_string = json_encode($data);                                                                                   
                $ch = curl_init('https://production.wompi.co/v1/tokens/cards');               
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                     
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                            'Content-Type: application/json',
                            'Authorization: Bearer '.config('app.wompi_public'),
                            'Content-Length: ' . strlen($data_string))                                                                       
                );

                $result = curl_exec($ch);
                curl_close($ch);

                
                $data = json_decode($result, true);
                
                $token = $data['data']['id'];
            /* FIN CAPTURA DE TOKEN DE ACEPTACION DE TARJETAS DE CREDITO PARA REALIZAR LA TRANSACCION*/


            /* SE GENERA EL ID PDE LA TRANSACCION*/
                $data_pay = array(
                            'type' => 'CARD', 
                            'token' => $token, 
                            'customer_email' => $email_wompi,
                            'acceptance_token' => $acceptance_token
                        );                                                                    
                $data_string_pay = json_encode($data_pay);                                                                                   
                $ch_pay = curl_init('https://production.wompi.co/v1/payment_sources');               
                curl_setopt($ch_pay, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch_pay, CURLOPT_POSTFIELDS, $data_string_pay);                                                                  
                curl_setopt($ch_pay, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_pay , CURLOPT_HTTPHEADER, array(                                                                          
                            'Content-Type: application/json',
                            'Authorization: Bearer '.config('app.wompi_private'),
                            'Content-Length: ' . strlen($data_string_pay))                                                                       
                ); 

                $result_pay = curl_exec($ch_pay);
                curl_close($ch_pay);

                $data_pay = json_decode($result_pay, true);

                $payment_source_id = $data_pay['data']['id'];
            /* FIN GENERAR ID PARA TRANSACCION*/


            /* GENERAR TRANSACCION PARA PAGO */
                $reference = "PAGO_ACTUALIZACION_PLAN-".$email_wompi."-".time();
                $valor = intval($request->valor_pago)."00";

                $data_transaccion = array(
                            'amount_in_cents' => intval($valor), 
                            'currency' => 'COP', 
                            'customer_email' => $email_wompi,
                            'payment_method' => [
                                    'installments' => $request->cuotas
                            ],
                            'reference' => $reference, // Referencia única de pago
                            'payment_source_id' => $payment_source_id
                        );                                                                    
                $data_string_transaccion = json_encode($data_transaccion);                                                                                   
                $ch_transaccion = curl_init('https://production.wompi.co/v1/transactions');               
                curl_setopt($ch_transaccion, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch_transaccion, CURLOPT_POSTFIELDS, $data_string_transaccion);                                                  
                curl_setopt($ch_transaccion, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_transaccion, CURLOPT_HTTPHEADER, array(                  
                            'Content-Type: application/json',
                            'Authorization: Bearer '.config('app.wompi_private'),
                            'Content-Length: ' . strlen($data_string_transaccion))                                                    
                ); 

                $result_transaction = curl_exec($ch_transaccion);
                curl_close($ch_transaccion);

                $data_transaccion = json_decode($result_transaction, true);

                return $data_transaccion;

            /* FIN GENERAR TRANSACCION PARA PAGO */


        	}

        }else if($request->metodo == 'nequi'){

            /* CAPTURAR TOKEN TERMINOS Y CONDICIONES */
                $url = "https://production.wompi.co/v1/merchants/".config('app.wompi_public');

                $codigo_tienda = $request->codigo_tienda;
                $nombre_cliente = $request->nombre_wompi;
                $telefono_cliente = $request->telefono_wompi;
                $hora_cliente = $request->hora_llegada;
                $comentario_cliente = $request->comentarios;


                //return $url;

                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                //for debug only!
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

                $resp = curl_exec($curl);
                curl_close($curl);

                $data = json_decode($resp, true);
                //return $data;

                $acceptance_token = $data['data']['presigned_acceptance']['acceptance_token'];

            /* FIN CAPTURAR TOKEN TERMINOS Y CONDICIONES */
  
            /* SE CAPTURA EL TOKEN DE ACEPTACION DE NEQUI PARA REALIZAR LA TRANSACCION*/
                $body = array(
                            'phone_number' => $request->numero_celular
                        );                                                                    
                $data_string = json_encode($body);                                                                                   
                $ch = curl_init('https://production.wompi.co/v1/tokens/nequi');               
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                            'Content-Type: application/json',
                            'Authorization: Bearer '.config('app.wompi_public'),
                            'Content-Length: ' . strlen($data_string))                                                                       
                );

                $result = curl_exec($ch);
                curl_close($ch);

                
                $data = json_decode($result, true);

                $token = $data['data']['id'];
                $status = $data['data']['status'];

                return $token;
            /* FIN CAPTURA DE TOKEN DE ACEPTACION DE NEQUI PARA REALIZAR LA TRANSACCION*/
        }else{
        }
    }


    public function verification_nequi($id){
        
         $url = "https://production.wompi.co/v1/tokens/nequi/".$id;

         $curl = curl_init($url);
         curl_setopt($curl, CURLOPT_URL, $url);
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

         $resp = curl_exec($curl);
         curl_close($curl);

         $data = json_decode($resp, true);
        return $data;
    }

    /* se genera ID transaccion para pago por Nequi*/
    public function pagar_nequi(Request $request, $token){

        /* SE GENERA EL ID PDE LA TRANSACCION*/

        $data_pay = array(
            'type' => 'NEQUI',
            'token' => $token
        );

        $data_string_pay = json_encode($data_pay);                                                                                   
        $ch_pay = curl_init('https://production.wompi.co/v1/payment_sources');               
        curl_setopt($ch_pay, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch_pay, CURLOPT_POSTFIELDS, $data_string_pay);
        curl_setopt($ch_pay, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_pay , CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',
            'Authorization: Bearer '.config('app.wompi_private'),
            'Content-Length: ' . strlen($data_string_pay))
        );

        $result_pay = curl_exec($ch_pay);
        curl_close($ch_pay);

        $data_pay = json_decode($result_pay, true);
        $payment_source_id = $data_pay['data']['id'];

        /* FIN GENERAR ID PARA TRANSACCION*/
    

        /* GENERAR TRANSACCION PARA PAGO */

        $reference = "actualizacionPlanTiendasClub-".$request->email_wompi."-".time();

        $data_transaccion = array(
            'amount_in_cents' => intval($request->valor_pago."00"), 
            'currency' => 'COP', 
            'customer_email' => $request->email_wompi,
            'reference' => $reference, // Referencia única de pago
            'payment_source_id' => $payment_source_id
        );      

        $data_string_transaccion = json_encode($data_transaccion);


        $ch_transaccion = curl_init('https://production.wompi.co/v1/transactions');               
        curl_setopt($ch_transaccion, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch_transaccion, CURLOPT_POSTFIELDS, $data_string_transaccion);                                                  
        curl_setopt($ch_transaccion, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_transaccion, CURLOPT_HTTPHEADER, array(                  
            'Content-Type: application/json',
            'Authorization: Bearer '.config('app.wompi_private'),
            'Content-Length: ' . strlen($data_string_transaccion))                                                    
        ); 

        $result_transaction = curl_exec($ch_transaccion);
        curl_close($ch_transaccion);

        $data_transaccion = json_decode($result_transaction, true);

        return $data_transaccion;

        /* FIN GENERAR TRANSACCION PARA PAGO */
    }

    /* Se verifica la transacción si es APROVED o NO*/
    public function verification_transaction($id){

        $ch_transaccion = curl_init('https://production.wompi.co/v1/transactions/'.$id);               
        curl_setopt($ch_transaccion, CURLOPT_CUSTOMREQUEST, "GET");                                                 
        curl_setopt($ch_transaccion, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_transaccion, CURLOPT_HTTPHEADER, array(                  
            'Content-Type: application/json'
        )); 

        $result_transaction = curl_exec($ch_transaccion);
        curl_close($ch_transaccion);

        $data_transaccion = json_decode($result_transaction, true);

        return $data_transaccion;

    }


    /* RESULTADO APROBADO, SE ACTUALIZAN LOS DIAS DEL PLAN */
    
    public function registro_bd(Request $request, $id){

         $informacion = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)
                                ->get();

        $codigo_tienda = $informacion[0]->id_tienda;
        $codigo_plan = $request->code_plan;

        DB::table('detalle_planes_tiendas')->where('id_detalle_plan_tienda', $codigo_tienda)->delete();

        $date = date('Y-m-d');

        $fecha_15 = strtotime($date."+ 1 month");

       DB::table('detalle_planes_tiendas')->insert([
                'id_detalle_plan' => $codigo_plan,
                'id_detalle_plan_tienda' => $codigo_tienda,
                'fecha_pago' => $date,
                'fecha_proximo_pago' => date("Y-m-d", $fecha_15),
                'estado_detalle_plan' => '1'
            ]);

        DB::table('role_user')->where('user_id', Auth::user()->id)->update([
            'role_id' => '777'
        ]);


       return redirect()->route('planes.cambio_plan')
            ->with('status_success','Actualización exitosa!');

    }

}
