<div class="tituloModal">
    <div class="container">
        <h3>
            <strong>Edición del producto</strong>
        </h3>
    </div>
</div> 
<div class="container" class="contenerdorFormulario">

                	@foreach($productos as $producto)
                                {!! Form::open(['route' => array('productos.update', $producto->id), 'files' => true]) !!}

                    @csrf
                    @method('PUT')

                    <div class="row">
                                    <div class="form-group col-3 col-md-3 col-lg-3 col-xl-3">
                                        <img src="{{ asset('/storage/'.$producto->imagen_producto) }}" width="150px"/>
                                    </div>
                                    <input type="hidden" name="imagen_bd" value="{{ $producto->imagen_producto }}">
                                    <div class="form-group col-9 col-md-9 col-lg-9 col-xl-9">

                                        <label><strong><code>(*)</code> Foto del producto:  </strong></label>
                                        <input type="file" class="form-control" 
                                               id="name" 
                                               placeholder="Nombre"
                                               name="foto_producto"
                                               value="{{ $producto->imagen_producto }}"
                                               >
                                    </div>
                                </div>

                    <div class="form-group">
                        <label><strong><code>(*) </code>Nombre producto: </strong></label>
                        <input type="text" class="form-control" name="nombre_producto" value="{{ $producto->name }}">
                    </div>

                    <div class="form-group">
                        <label>Descripción del producto:</label>
                        <input type="text" class="form-control" name="descripcion_producto" value="{{ $producto->description }}">
                    </div>
                    
                    <div class="form-group">
                        <label><strong><code>(*) </code>Valor del producto: </strong></label><br>
                            <input type="number" class="form-control" name="valor_producto" value="{{ $producto->valor }}" />
                    </div>

                    <div class="form-group">
                        <label><strong><code>(*) </code>Categoria del Producto: </strong></label><br>
                            <select class="form-control mi-selector" name="categoria_producto">
                            	<option value="" selected="">Selecciona una categoria del producto</option>
	                                @foreach($categorias as $categoria)
	                                		@if($categoria->id == $producto->id_categoria)
	                            				<option value="{{ $categoria->id }}" selected="">{{ $categoria->nombre_categoria }}</option>
	                            			@else
	                            				<option value="{{ $categoria->id }}">{{ $categoria->nombre_categoria }}</option>
	                            			@endif
	                                @endforeach
                            </select>
                    </div>

                    <div class="form-group">
                        <label><strong><code>(*) </code>Estado: </strong></label><br>
                            <select class="form-control mi-selector" name="estado_producto">
                                <option value="1">Activa</option>
                                <option value="0">Inactiva</option>
                            </select>
                    </div>

                    <center>
                        <!--
                        <a href="{{ url('productos') }}" style="color:white; ">
                            <button type="button" class="btn btn-danger">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="25px" height="25px" class=""><g><g>
                                            <g>
                                                <path d="M256,0C114.508,0,0,114.497,0,256c0,141.493,114.497,256,256,256c141.492,0,256-114.497,256-256    C512,114.507,397.503,0,256,0z M256,472c-119.384,0-216-96.607-216-216c0-119.385,96.607-216,216-216    c119.384,0,216,96.607,216,216C472,375.385,375.393,472,256,472z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                        </g><g>
                                            <g>
                                                <path d="M343.586,315.302L284.284,256l59.302-59.302c7.81-7.81,7.811-20.473,0.001-28.284c-7.812-7.811-20.475-7.81-28.284,0    L256,227.716l-59.303-59.302c-7.809-7.811-20.474-7.811-28.284,0c-7.81,7.811-7.81,20.474,0.001,28.284L227.716,256    l-59.302,59.302c-7.811,7.811-7.812,20.474-0.001,28.284c7.813,7.812,20.476,7.809,28.284,0L256,284.284l59.303,59.302    c7.808,7.81,20.473,7.811,28.284,0C351.398,335.775,351.397,323.112,343.586,315.302z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                        </g></g> </svg>
                                Cancelar actualizacion
                            </button>
                        </a>-->

                        <button type="submit" class="btn btn-info" id="registroTienda">Actualizar producto</button><br><br>
                    </center>
                               
                                {!! Form::close() !!}
                                 @endforeach

                            </div>