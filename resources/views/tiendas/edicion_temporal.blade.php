@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                             	<b>Editar información de tu tienda</b>
                            </div>
                        </div>
                        
                    </div>
                    @include('custom.message')
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            @foreach($tiendas as $tienda)

                                {!! Form::open(['route' => array('tiendas.update', $tienda->url_tienda), 'files' => true]) !!}

                                @csrf
                                @method('PUT')
                                

                                <input type="hidden" name="id_persona" value="{{ $id_persona }}">
                                <div class="form-group">
                                    <label>Nombre identificador de la tienda</label>
                                    <em>(Este ser&aacute; un nombre &uacute;nico registrado en el sistema, aparecerà en la URL)</em><br>
                                    <input type="text" class="form-control" id="nombreTiendaUnico" value="{{ $tienda->url_tienda }}" disabled="">
                                    <input type="hidden" class="form-control" id="nombreTiendaUnico" value="{{ $tienda->url_tienda }}" name="nameUnicoTienda">
                                </div>
                                <div class="row">
                                    <div class="form-group col-3 col-md-3 col-lg-3 col-xl-3">
                                        <img src="{{ asset('/storage/'.$tienda->image_tienda) }}" width="150px"/>
                                    </div>
                                    <input type="hidden" name="imagen_bd" value="{{ $tienda->image_tienda }}">
                                    <div class="form-group col-9 col-md-9 col-lg-9 col-xl-9">

                                        <label><strong><code>(*)</code> Foto de la tienda:  </strong></label>
                                        <input type="file" class="form-control" 
                                               id="name" 
                                               placeholder="Nombre"
                                               name="foto_tienda"
                                               value="{{ $tienda->image_tienda }}"
                                               >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><b>Fondo de la tienda:</b></label><br>
                                    <input type="file" class="form-control" name="background_tienda" id="background_tienda">
                                </div>
                                
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Nombre</strong></label><br>
                                    <input type="text" class="form-control" name="nombreTienda" id="nombreTienda" value="{{ $tienda->nombre_tienda }}">
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Descripcion</strong></label><br>
                                    <textarea class="form-control" name="descripcionTienda" id="descripcionTienda">{{ $tienda->descripcion_tienda }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Direccion:</strong></label><br>
                                    <textarea class="form-control" name="direccionTienda" id="direccionTienda">{{ $tienda->direccion_tienda }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Horarios</strong></label><br>
                                    <textarea class="form-control" name="horarioTienda" id="horarioTienda">{{ $tienda->horario_tienda }}</textarea>
                                    <small id="emailHelp" class="form-text text-muted">Por favor, separe los dias con una coma. Ejemplo: Lunes de 10:00 a 22:00, Martes de 05:00 a 22:00</small>

                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Valor del Domicilio: </strong></label><br>
                                    <input type="number" class="form-control" name="domicilio" value="{{ $tienda->domicilio }}">
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Departamento</strong></label><br>
                                    <select class="form-control mi-selector" id="departamento" name="departamentoTienda">
                                        <option value="0" selected="">Seleccionar departamento</option>
                                        @foreach($departamentos as $departamento)
                                        <option value="{{ $departamento->id }}" selected="">{{ $departamento->nombre_departamento }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Ciudad</strong></label><br>
                                    <select class="form-control mi-selector" id="ciudad" name="ciudadTienda">
                                        <option value="0" selected="">Seleccionar departamento</option>
                                        @foreach($ciudades as $ciudad)
                                            @if($tienda->id_ciudad == $ciudad->id)
                                                <option value="{{ $ciudad->id }}" selected="">{{ $ciudad->nombre_ciudad }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code> Categoria Tienda</strong></label><br>
                                    <select class="form-control mi-selector" id="categoria_tienda" name="categoria_tienda">
                                        <option value="0" selected="">Categoria de la tienda</option>
                                        @foreach($categorias_tienda as $categoria_tienda)
                                            @if($categoria_tienda->id == $tienda->id_cate_tienda)
                                                <option value="{{ $categoria_tienda->id }}" selected="">{{ $categoria_tienda->nombre_categoria_tienda }}</option>
                                            @else
                                                <option value="{{ $categoria_tienda->id }}">{{ $categoria_tienda->nombre_categoria_tienda }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label><strong><code>(*)</code> WhatsApp</strong></label><br>
                                    <input type="number" class="form-control" name="telMovil" id="telMovil" value="{{ $tienda->telefono_movil }}">
                                </div>

                                <div class="form-group">
                                    <label>Telefono fijo</label><br>
                                    <input type="number" class="form-control" name="telFijo" id="telFijo" value="{{ $tienda->telefono_fijo }}">
                                </div>

                                <div class="form-group">
                                    <label>Link de Facebook</label>
                                    <input type="text" class="form-control" name="linkFb" id="linkFb" value="{{ $tienda->facebook_link }}">
                                </div>

                                <div class="form-group">
                                    <label>Link de Instagram</label><br>
                                    <input type="text" class="form-control" name="linkIns" id="linkIns" value="{{ $tienda->instagram_link }}">
                                </div>

                                <div class="form-group">
                                    <label>Link de Twitter</label><br>
                                    <input type="text" class="form-control" name="linkTwitter" id="linkTwitter" value="{{ $tienda->twitter_link }}">
                                </div>
                                 <div class="form-group">
                                    <label><strong><code>(*)</code>Estado de la tienda:</strong></label><br>
                                    <?php if($rol == 'admin'){ ?> 
                                        
                                        <select class="form-control mi-selector" id="categoria_tienda" name="estado_tienda">
                                            @if($tienda->estado_tienda == 1)
                                                <option value="1" selected="">Activo</option>
                                                <option value="0">Inactivo</option>
                                            @else
                                                <option value="1">Activo</option>
                                                <option value="0" selected="">Inactivo</option>
                                            @endif
                                        </select>

                                    <?php }else{ ?>
                                        <input  type="hidden" class="form-control" name="estado_tienda" value="{{ $tienda->estado_tienda }}" />

                                        @if($tienda->estado_tienda == 1)
                                            <input  type="text" class="form-control" value="Activo" disabled=""/>
                                        @else
                                            <input  type="text" class="form-control" value="Inactivo" disabled=""/>
                                        @endif

                                    <?php } ?>
                                         
                                    </div>

                                <center>
                                    <div class="contenedorBotones">
                                        <a href="{{ url('tiendas/listado') }}" style="color:white; ">
                                            <button type="button" class="btn btn-danger">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="25px" height="25px" class=""><g><g>
                                                <g>
                                                <path d="M256,0C114.508,0,0,114.497,0,256c0,141.493,114.497,256,256,256c141.492,0,256-114.497,256-256    C512,114.507,397.503,0,256,0z M256,472c-119.384,0-216-96.607-216-216c0-119.385,96.607-216,216-216    c119.384,0,216,96.607,216,216C472,375.385,375.393,472,256,472z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                                </g>
                                                </g><g>
                                                <g>
                                                <path d="M343.586,315.302L284.284,256l59.302-59.302c7.81-7.81,7.811-20.473,0.001-28.284c-7.812-7.811-20.475-7.81-28.284,0    L256,227.716l-59.303-59.302c-7.809-7.811-20.474-7.811-28.284,0c-7.81,7.811-7.81,20.474,0.001,28.284L227.716,256    l-59.302,59.302c-7.811,7.811-7.812,20.474-0.001,28.284c7.813,7.812,20.476,7.809,28.284,0L256,284.284l59.303,59.302    c7.808,7.81,20.473,7.811,28.284,0C351.398,335.775,351.397,323.112,343.586,315.302z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                                </g>
                                                </g></g> </svg>
                                                Cancelar registro
                                            </button>
                                        </a>

                                        <button type="submit" class="btn btn-info">Actualizar tienda</button>
                                    </div>
                                </center>


                                {!! Form::close() !!}
                                @endforeach
                        </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>


@endsection
