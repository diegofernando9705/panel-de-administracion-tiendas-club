<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallePlanesTiendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_planes_tiendas', function (Blueprint $table) {
            $table->increments('id_detalle_planes_tiendas');
            $table->integer('id_detalle_plan');
            $table->integer('id_detalle_plan_tienda');
            $table->char('fecha_pago', 100);
            $table->char('fecha_proximo_pago', 100);
            $table->char('estado_detalle_plan', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_planes_tiendas');
    }
}
