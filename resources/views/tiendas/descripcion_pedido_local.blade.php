<div class="tituloModal">
    <div class="container">
        <h3>
            <strong>Descripcion del pedido</strong>
        </h3>
    </div>
</div> 
<div class="container" class="contenerdorFormulario">
	@foreach($pedidos_locales as $pedido_local)
		<div class="form-group">
			<label><b>Codigo del pedido</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->codigo_pedido }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Nombre del cliente</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->nombre }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Telefono del cliente</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->telefono }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Hora de llegada</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->hora_llegada }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Comentarios del cliente</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->comentarios }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Productos comprados</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->productos }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Adicionales de los productos</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->adicionales }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Costo del domicilio</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->domicilio }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Valor del adicional</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->valor_adicional }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Valor de los productos</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->valor_productos }}" disabled="">
		</div>
		<div class="form-group">
			<label><b>Valor total del pedido</b></label>
			<input type="text" class="form-control" name="" value="{{ $pedido_local->total_pedido }}" disabled="">
		</div>
	@endforeach
</div>