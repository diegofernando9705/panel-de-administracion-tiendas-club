<!-- Listado de las tiendas, no influye si es Administrador o Cliente -->

@extends('layouts.app')

@section('content')
<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                              Módulo Historial de Pedidos
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">

                            <div class="messaging">
                                <!--<b>Búsqueda de la tienda:</b>

                                <input type="text" name="buscadorTienda" id="buscadorTienda" class="form-control" placeholder="Busca tu producto..." />-->
                            </div>
                            <br/>
                            <br />
                            <h3><b>Pedidos locales</b></h3>
                            <table class="table table-bordered table-hover" id="table_tienda">
                                <thead>
                                    <tr>
                                        <th>Codigo del pedido</th>
                                        <th>Nombre del cliente</th>
                                        <th>Hora de llegada</th>
                                        <th>Celular</th>
                                        <th>Estado</th>
                                        <th colspan="3">
                                        	<center>Acciones</center>
                                        </th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($pedidos_locales as $pedido_local)
                                    <tr>
                                        <td>{{ $pedido_local->codigo_pedido }}</td>
                                        <td>{{ $pedido_local->nombre }}</td>
                                        <td>{{ $pedido_local->hora_llegada }}</td>
                                        <td>{{ $pedido_local->telefono }}</td>

                                        @if($pedido_local->estado_pedido == 'Recibido')
                                        	<td class="alert alert-dark">Recibido</td>
                                        @elseif($pedido_local->estado_pedido == 'Proceso')
                                        	<td class="alert alert-warning">En Proceso</td> 
                                        @elseif($pedido_local->estado_pedido == 'Camino')
                                        	<td class="alert alert-primary">En camino</td>
                                        @elseif($pedido_local->estado_pedido == 'Entregado')
                                        	<td class="alert alert-success">Entregado</td>
                                        @else                       
                                        @endif
                                        <td>
                                        <td>
                                        	<center>
		                                        @if($pedido_local->estado_pedido == 'Recibido')
		                                        <a data-href="{{ route('tiendas.verPedidoLocal', $pedido_local->codigo_pedido ) }}" class="identificador-pedido">
	                                                <button type="button" class="btn btn-info btn-sm">
	                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
	                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
	                                                    </svg>
	                                                    Ver pedido
	                                                </button>
                                            	</a>
		                                        <form method="POST" action="{{ route('tiendas.estado') }}" method="POST">
                                                @csrf
                                                @method('POST')
                                                	<input type="hidden" name="codigo_pedido" value="{{  $pedido_local->codigo_pedido }}">
                                                	<input type="hidden" name="estado" value="Proceso">
		                                        	<button type="submit" class="btn btn-warning">Cambiar a En Proceso</button>
		                                        </form>

		                                        @elseif($pedido_local->estado_pedido == 'Proceso')
		                                        <a data-href="{{ route('tiendas.verPedidoLocal', $pedido_local->codigo_pedido)}}" class="identificador-pedido">
	                                                <button type="button" class="btn btn-info btn-sm">
	                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
	                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
	                                                    </svg>
	                                                    Ver pedido
	                                                </button>
                                            	</a>

		                                        <form method="POST" action="{{ route('tiendas.estado') }}" method="POST">
                                                @csrf
                                                @method('POST')
                                                	<input type="hidden" name="codigo_pedido" value="{{  $pedido_local->codigo_pedido }}">
                                                	<input type="hidden" name="estado" value="Camino">
		                                        	<button type="submit" class="btn btn-primary">Cambiar a En Camino</button> 
		                                        </form>

		                                        @elseif($pedido_local->estado_pedido == 'Camino')
		                                        <a data-href="{{ route('tiendas.verPedidoLocal', $pedido_local->codigo_pedido ) }}" class="identificador-pedido">
	                                                <button type="button" class="btn btn-info btn-sm">
	                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
	                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
	                                                    </svg>
	                                                    Ver pedido
	                                                </button>
                                            	</a>
		                                        <form method="POST" action="{{ route('tiendas.estado') }}" method="POST">
                                                	@csrf
                                                	@method('POST')
                                                	<input type="hidden" name="codigo_pedido" value="{{  $pedido_local->codigo_pedido }}">
                                                	<input type="hidden" name="estado" value="Entregado">
		                                        	<button type="submit" class="btn btn-success">Cambiar a Entregado</button>
		                                        </form> 
		                                        @elseif($pedido_local->estado_pedido == 'Entregado')
		                                        <a data-href="{{ route('tiendas.verPedidoLocal', $pedido_local->codigo_pedido ) }}" class="identificador-pedido">
	                                                <button type="button" class="btn btn-info btn-sm">
	                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
	                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
	                                                    </svg>
	                                                    Ver pedido
	                                                </button>
                                            	</a>
		                                        	<button type="button" class="btn btn-success">Entregado</button> 
                                                    @elseif($pedido_local->estado_pedido == 'Cerrado')
                                                    <button type="button" class="btn btn-danger">Pedido cerrado</button>
		                                        @else                       
		                                        @endif
                                    		</center>
                                        </td>
                                        <td>
                                            <form method="POST" action="{{ route('tiendas.estado') }}" method="POST">
                                                    @csrf
                                                    @method('POST')
                                                    <input type="hidden" name="codigo_pedido" value="{{  $pedido_local->codigo_pedido }}">
                                                    <input type="hidden" name="estado" value="Cerrado">
                                                    <button type="submit" class="btn btn-danger">Cerrar pedido</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $pedidos_locales->links() }}
                            <br><br>
                            <h3><b>Pedidos a Domicilio</b></h3>
                            <table class="table table-bordered table-hover" id="table_tienda">
                                <thead>
                                    <tr>
                                        <th>Codigo del pedido</th>
                                        <th>Nombre del cliente</th>
                                        <th>Direccion</th>
                                        <th>Barrio</th>
                                        <th>Comentarios</th>
                                        <th>Estado</th>
                                        <th colspan="3"><center>Acciones</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($pedidos_domicilios as $pedido_domicilio)
                                    <tr>
                                        <td>{{ $pedido_domicilio->codigo_pedido }}</td>
                                        <td>{{ $pedido_domicilio->nombre }}</td>
                                        <td>{{ $pedido_domicilio->direccion }}</td>
                                        <td>{{ $pedido_domicilio->barrio }}</td>
                                        <td>{{ $pedido_domicilio->comentarios }}</td>
                                        @if($pedido_domicilio->estado_pedido == 'Recibido')
                                        		<td class="alert alert-dark">Recibido</td>
                                        @elseif($pedido_domicilio->estado_pedido == 'Proceso')
                                        	<td class="alert alert-warning">En Proceso</td> 
                                        @elseif($pedido_domicilio->estado_pedido == 'Camino')
                                        	<td class="alert alert-primary">En camino</td>
                                        @elseif($pedido_domicilio->estado_pedido == 'Entregado')
                                        	<td class="alert alert-success">Entregado</td>
                                        @else                       
                                        @endif
                                        <td>
                                        	<center>
		                                        @if($pedido_domicilio->estado_pedido == 'Recibido')
		                                        <a data-href="{{ route('tiendas.verPedidoDomicilio', $pedido_domicilio->codigo_pedido ) }}" class="identificador-pedido">
	                                                <button type="button" class="btn btn-info btn-sm">
	                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
	                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
	                                                    </svg>
                                                        Ver pedido
	                                                </button>
                                            	</a>
		                                        <form method="POST" action="{{ route('tiendas.estado') }}" method="POST">
                                                @csrf
                                                @method('POST')
                                                	<input type="hidden" name="codigo_pedido" value="{{  $pedido_domicilio->codigo_pedido }}">
                                                	<input type="hidden" name="estado" value="Proceso">
		                                        	<button type="submit" class="btn btn-warning">Cambiar a En Proceso</button>
		                                        </form>

		                                        @elseif($pedido_domicilio->estado_pedido == 'Proceso')
		                                        <a data-href="{{ route('tiendas.verPedidoDomicilio', $pedido_domicilio->codigo_pedido ) }}" class="identificador-pedido">
	                                                <button type="button" class="btn btn-info btn-sm">
	                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
	                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
	                                                    </svg>
	                                                    Ver pedido
	                                                </button>
                                            	</a>
		                                        <form method="POST" action="{{ route('tiendas.estado') }}" method="POST">
                                                @csrf
                                                @method('POST')
                                                	<input type="hidden" name="codigo_pedido" value="{{  $pedido_domicilio->codigo_pedido }}">
                                                	<input type="hidden" name="estado" value="Camino">
		                                        	<button type="submit" class="btn btn-primary">Cambiar a En Camino</button> 
		                                        </form>

		                                        @elseif($pedido_domicilio->estado_pedido == 'Camino')
		                                        <a data-href="{{ route('tiendas.verPedidoDomicilio', $pedido_domicilio->codigo_pedido ) }}" class="identificador-pedido">
	                                                <button type="button" class="btn btn-info btn-sm">
	                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
	                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
	                                                    </svg>
	                                                    Ver pedido
	                                                </button>
                                            	</a>
		                                        <form method="POST" action="{{ route('tiendas.estado') }}" method="POST">
                                                	@csrf
                                                	@method('POST')
                                                	<input type="hidden" name="codigo_pedido" value="{{  $pedido_domicilio->codigo_pedido }}">
                                                	<input type="hidden" name="estado" value="Entregado">
		                                        	<button type="submit" class="btn btn-success">Cambiar a Entregado</button>
		                                        </form>

		                                        @elseif($pedido_domicilio->estado_pedido == 'Entregado')
		                                        <a data-href="{{ route('tiendas.verPedidoDomicilio', $pedido_domicilio->codigo_pedido ) }}" class="identificador-pedido">
	                                                <button type="button" class="btn btn-info btn-sm">
	                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
	                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
	                                                    </svg>
	                                                    Ver pedido
	                                                </button>
                                            	</a>
		                                        	<button type="button" class="btn btn-success">Entregado</button> 
                                                @elseif($pedido_domicilio->estado_pedido == 'Cerrado')
                                                <button type="submit" class="btn btn-danger">Pedido cerrado</button>
		                                        @else                       
		                                        @endif
                                    		</center>
                                        </td>
                                        <td>
                                            <form method="POST" action="{{ route('tiendas.estado') }}" method="POST">
                                                    @csrf
                                                    @method('POST')
                                                    <input type="hidden" name="codigo_pedido" value="{{  $pedido_domicilio->codigo_pedido }}">
                                                    <input type="hidden" name="estado" value="Cerrado">
                                                    <button type="submit" class="btn btn-danger">Cerrar pedido</button>
                                            </form>
                                        </td>

                                                
                                    </tr>
                                    @endforeach
  
                                </tbody>
                            </table>
                            <br><br>
                            {{ $pedidos_domicilios->links() }}
                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modales-tiendas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-body">
        
    </div>
  </div>
</div>

@endsection
