@extends('layouts.app')

@section('content')


<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                                Crear un nuevo cup&oacute;n
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                        {!! Form::open(['route' => 'cupones.store', 'files' => false]) !!}

                    @csrf
                    @method('POST')

                    <div class="form-group">
                    	 <label><strong><code>(*) </code>C&oacute;digo del cup&oacute;n: </strong></label>
	                    <div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="basic-addon1">club_</span>
						  </div>
						  <input type="text" class="form-control" placeholder="código del cupón" name="cod_cupon" value="{{ old('cod_cupon') }}">
						</div>
					</div>

                    <div class="form-group">
                        <label><strong><code>(*) </code>Nombre cup&oacute;n: </strong></label>
                        <input type="text" class="form-control" name="nombre_cupon" value="{{ old('nombre_cupon') }}">
                    </div>

                    <div class="form-group">
                        <label><b>Descripción del cup&oacute;n:</b></label>
                        <textarea class="form-control" name="descripcion_cupon" maxlength="100">{{ old('descripcion_cupon') }}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <label><strong><code>(*) </code>Valor del descuento: </strong></label><br>
                            <input type="number" class="form-control" name="valor_descuento" value="{{ old('valor_descuento') }}" />
                            <!--
                            <small id="emailHelp" class="form-text text-muted">Llenar si se ha seleccionado tipo: Dinero.</small>
                            -->
                    </div>

                    <div class="form-group">
                        <label><strong><code>(*) </code>Tiempo de caducidad: </strong></label><br>
                        <select class="form-control mi-selector" name="tipo_cupon" id="tipo_cupon">
                            <option value="" disabled="" selected="">Selecciona una opción</option>
                            <option value="cantidad">Cantidad de cupones</option>
                            <option value="fecha">Fecha de caducidad</option>
                            <option value="personalizado">Personalizado</option>
                        </select>
                    </div>

                    
                    <div class="form-group" id="fecha">
					    <label><strong>Fecha del cupón: </strong></label><br>
					    <div class="row">
						    <div class="fecha_inicio col-6 col-md-6 col-lg-6 col-sm-6 col-xl-6">
						    	<input type="date" class="form-control" placeholder="Fecha inicio" name="fecha_inicio" value="{{ date('Y-m-d') }}">
						    	<small id="emailHelp" class="form-text text-muted">Fecha inicial para la activación.</small>
							</div>

						    <div class="fecha_fin col-6 col-md-6 col-lg-6 col-sm-6 col-xl-6">
						    	<input type="date" class="form-control" placeholder="Fecha fin" name="fecha_fin">
						    	<small id="emailHelp" class="form-text text-muted">Fecha final para la desactivación.</small>
							</div>
						</div>
					</div>

                    <div class="form-group" id="cantidad">
                        <label><strong>Cupones disponibles: </strong></label><br>
                        <input type="number" class="form-control" placeholder="Cantidad de cupones" name="cantidad" value="{{ old('cantidad') }}">
                        <small id="emailHelp" class="form-text text-muted">Cantidad disponible al p&uacute;blico.</small>
                    </div>

                    <div class="form-group" id="personalizado">
                        <label><strong><code></code>Personalizado: </strong></label><br>
                        <div class="alert alert-danger">Con esta opci&oacute; usted podra desactivar el cup&oacute;n cuando desee.</div>
                    </div>

                    @if($usuario[0]->slug == 'admin')
					<div class="form-group">
                        <label><strong>Tienda: </strong></label><br>
                            <select class="form-control mi-selector" name="tienda">
                                	<option value="1">Campaña Tiendas Club</option>
                                @foreach($tiendas as $tienda)
                                	<option value="{{ $tienda->id_tienda }}">{{ $tienda->nombre_tienda }}</option>
                                @endforeach
                            </select>
                    </div>
                    @endif

                    <div class="form-group">
                        <label><strong><code>(*) </code>Estado: </strong></label><br>
                            <select class="form-control mi-selector" name="estado_cupon">
                                <option value="1">Activo</option>
                                <option value="2">Próximo</option>
                                <option value="0">Inactiva</option>
                            </select>
                    </div>

                    <center>
                        <a href="{{ route('cupones.index') }}" style="color:white; ">
                            <button type="button" class="btn btn-danger">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="25px" height="25px" class=""><g><g>
                                            <g>
                                                <path d="M256,0C114.508,0,0,114.497,0,256c0,141.493,114.497,256,256,256c141.492,0,256-114.497,256-256    C512,114.507,397.503,0,256,0z M256,472c-119.384,0-216-96.607-216-216c0-119.385,96.607-216,216-216    c119.384,0,216,96.607,216,216C472,375.385,375.393,472,256,472z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                        </g><g>
                                            <g>
                                                <path d="M343.586,315.302L284.284,256l59.302-59.302c7.81-7.81,7.811-20.473,0.001-28.284c-7.812-7.811-20.475-7.81-28.284,0    L256,227.716l-59.303-59.302c-7.809-7.811-20.474-7.811-28.284,0c-7.81,7.811-7.81,20.474,0.001,28.284L227.716,256    l-59.302,59.302c-7.811,7.811-7.812,20.474-0.001,28.284c7.813,7.812,20.476,7.809,28.284,0L256,284.284l59.303,59.302    c7.808,7.81,20.473,7.811,28.284,0C351.398,335.775,351.397,323.112,343.586,315.302z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                        </g></g> </svg>
                                Cancelar registro
                            </button>
                        </a>

                        <button type="submit" class="btn btn-info" id="registroTienda">Crear cupón</button>
                    </center>
                    {!! Form::close() !!}
                </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>

@endsection

