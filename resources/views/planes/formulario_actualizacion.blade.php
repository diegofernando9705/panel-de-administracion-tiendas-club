<div class="row">
	<div class="datos_personales col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
		<div class="form-group">
			<label><strong>Nombre: </strong></label>
			<input class="form-control" type="text" name="name" value="{{ $persona[0]->primer_nombre }} {{ $persona[0]->primer_apellido }}" readonly="">
		</div>
		<div class="form-group">
			<label><strong>Email: </strong></label>
			<input class="form-control" type="text" name="email" value="{{ $persona[0]->correo_electronico }}" readonly="">
		</div>
		<div class="form-group">
			<label><strong>Celular: </strong></label>
			<input class="form-control" type="text" name="celular" value="{{ $persona[0]->telefono }}" readonly="">
		</div>
		<div class="form-group">
			<label><strong><code>(*) </code>Medio de pago: </strong></label><br>
			<br>
			<div class="row">
				<div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
					<center>
						<input type="checkbox" name="metodo" class="only-one" value="pse">
					</center>
					<img src="{{ asset('img/pse_logo.png') }}" width="100%">
				</div>
				<div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
					<center>
						<input type="checkbox" name="metodo" class="only-one" value="tarjetas">
					</center>
					<img src="{{ asset('img/tarjetas.png') }}" width="100%">
				</div>
			</div>
		</div>
	</div>
	<div class="datos_pago col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6" id="formulario_pago">
		<div class="alert alert-success">
			<strong>Usuario: </strong> Por favor selecciona un medio de pago, para continuar con la actualizaci&oacute;n de Plan
		</div>
	</div>
</div>

<script type="text/javascript">
	 let Checked = null;
    //The class name can vary
    for (let CheckBox of document.getElementsByClassName('only-one')){
        CheckBox.onclick = function(){
        if(Checked!=null){
          Checked.checked = false;
          Checked = CheckBox;
        }
        Checked = CheckBox;
      }
    }


    $(document).on("click", ".only-one", function(){
    	
    	var formData = $("#update_plan").serialize();

    	$valor = $(this).val();

    	$.ajax({
    		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    		type: "POST",
    		url: "/metodoUpdatePay/"+$valor,
    		data: formData,
    		success: function (data) {
    			$("#formulario_pago").html(data);
    		}
    	});
    });


</script>