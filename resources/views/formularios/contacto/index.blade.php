<!-- Listado de las tiendas, no influye si es Administrador o Cliente -->

@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                               Formulario de contacto
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="messaging">
                                <b>Búsqueda de contacto:</b>

                                <input type="text" name="buscadorSugerencias" id="buscadorSugerencias" class="form-control" placeholder="Busca tu sugerencia..." />                            
                            </div>
                            <br>
                            <table class="table table-bordered table-hover" id="tabla-sugerencias">
                                <thead>
                                    <tr>
                                        <th>Nombre Persona</th>
                                        <th>Empresa Persona</th>
                                        <th>Correo Persona</th>
                                        <th>Telefono Persona</th>
                                        <th>Mensaje</th>
                                        <!--
                                        <th><center>Estado</center></th>
                                        <th><center>Accion</center></th>-->
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($contactos as $contacto)
                                    <tr>
                                        <td>{{ $contacto->nombre }}</td>
                                        <td>{{ $contacto->empresa }} </td>
                                        <td>{{ $contacto->correo }} </td>
                                        <td>{{ $contacto->telefono }} </td>
                                        <td>{{ $contacto->mensaje }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modales-tiendas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-body">
        
    </div>
  </div>
</div>


@endsection
