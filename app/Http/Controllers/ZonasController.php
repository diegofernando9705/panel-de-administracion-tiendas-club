<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Gate;
use DateTime;

class ZonasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        Gate::authorize('haveaccess', 'zonas.show');

        $usuario = DB::table('role_user')
                    ->select('users.*', 'roles.*', 'role_user.*')
                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
                    ->join('users', 'role_user.user_id', '=', 'users.id')
                    ->where('users.id', Auth::user()->id)->get();

         /* detectar codigo de la tienda segun usuario */
            
            $tiendas = DB::table('detalle_usuario_tiendas')
                        ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                        ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                        ->join('users', 'personas.user_register', '=', 'users.id')
                        ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                        ->where('users.id', Auth::user()->id)->get();

                        foreach ($tiendas as $tienda) {
                            $codigo_tienda = $tienda->id_tienda;
                        }

        if($usuario[0]->slug == 'admin'){
             
            $zonas = DB::table('detalle_zonas_tiendas')
                        ->select('zonas_tiendas.*', 'detalle_zonas_tiendas.*', 'tiendas.*')
                        ->join('zonas_tiendas', 'detalle_zonas_tiendas.id_zona_detalle', '=', 'zonas_tiendas.id_zona')
                        ->join('tiendas', 'detalle_zonas_tiendas.codigo_tienda_detalle', '=', 'tiendas.id_tienda')
                        ->paginate(10);

            return view('zonas.index', compact('zonas'));

        }else{

            $zonas = DB::table('detalle_zonas_tiendas')
                        ->select('zonas_tiendas.*', 'detalle_zonas_tiendas.*', 'tiendas.*')
                        ->join('zonas_tiendas', 'detalle_zonas_tiendas.id_zona_detalle', '=', 'zonas_tiendas.id_zona')
                        ->join('tiendas', 'detalle_zonas_tiendas.codigo_tienda_detalle', '=', 'tiendas.id_tienda')
                        ->where('detalle_zonas_tiendas.codigo_tienda_detalle', $codigo_tienda)
                        ->paginate(10);

            /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

            $detalles_del_plan = DB::table('detalle_planes_tiendas')
              ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
              ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
              ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
              ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
              ->get();

            $dia_actual = new DateTime(date('Y-m-d'));
            $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

            $funcion = $dia_actual->diff($fecha_proximo_pago);

            if($fecha_proximo_pago < $dia_actual){
              $dias_faltantes = 0;
            }else{
              $dias_faltantes = $funcion->days;
            }

            /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

            if($dias_faltantes <= 0){
              $planes = DB::table('tabla_planes')->get();
              return view('planes.tienda.pago-plan', compact('planes'));
            }else{
              return view('zonas.index', compact('zonas'));
            }
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   

        Gate::authorize('haveaccess', 'zonas.create');

        $usuario = DB::table('role_user')
                    ->select('users.*', 'roles.*', 'role_user.*')
                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
                    ->join('users', 'role_user.user_id', '=', 'users.id')
                    ->where('users.id', Auth::user()->id)->get();

        $tiendas = DB::table('tiendas')->get();

        /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

        $detalles_del_plan = DB::table('detalle_planes_tiendas')
              ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
              ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
              ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
              ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
              ->get();

            $dia_actual = new DateTime(date('Y-m-d'));
            $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

            $funcion = $dia_actual->diff($fecha_proximo_pago);

            if($fecha_proximo_pago < $dia_actual){
              $dias_faltantes = 0;
            }else{
              $dias_faltantes = $funcion->days;
            }

          /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

          if($dias_faltantes <= 0){
            $planes = DB::table('tabla_planes')->get();
            return view('planes.tienda.pago-plan', compact('planes'));
          }else{
            return view('zonas.crear', compact('tiendas', 'usuario'));
          }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        Gate::authorize('haveaccess', 'zonas.create');

        $request->validate([
            'nombre' => 'required',
            'celular' => 'required|numeric',
            'estado' => 'required'
        ]);

        $usuario = DB::table('role_user')
                    ->select('users.*', 'roles.*', 'role_user.*')
                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
                    ->join('users', 'role_user.user_id', '=', 'users.id')
                    ->where('users.id', Auth::user()->id)->get();

        
        DB::table('zonas_tiendas')->insert([
                'nombre_zona' => $request->nombre, 
                'descripcion_zona' => $request->descripcion, 
                'celular_zona' => $request->celular, 
                'estado_zona' => $request->estado ]);

        if($usuario[0]->slug == 'admin'){
            
            $zona = DB::table('zonas_tiendas')->orderBy('id_zona', 'desc')->take(1)->get();

            DB::table('detalle_zonas_tiendas')->insert([
                'id_zona_detalle' => $zona[0]->id_zona,
                'codigo_tienda_detalle' => $request->tienda 
            ]);


        }else{
            
            /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

            $detalles_del_plan = DB::table('detalle_planes_tiendas')
              ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
              ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
              ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
              ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
              ->get();

            $dia_actual = new DateTime(date('Y-m-d'));
            $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

            $funcion = $dia_actual->diff($fecha_proximo_pago);

            if($fecha_proximo_pago < $dia_actual){
              $dias_faltantes = 0;
            }else{
              $dias_faltantes = $funcion->days;
            }

          /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

          if($dias_faltantes <= 0){
            $planes = DB::table('tabla_planes')->get();
            return view('planes.tienda.pago-plan', compact('planes'));
          }else{
            
             /* detectar codigo de la tienda segun usuario */
            
            $tiendas = DB::table('detalle_usuario_tiendas')
                        ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                        ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                        ->join('users', 'personas.user_register', '=', 'users.id')
                        ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                        ->where('users.id', Auth::user()->id)->get();

                        foreach ($tiendas as $tienda) {
                            $codigo_tienda = $tienda->id_tienda;
                        }

            $zona = DB::table('zonas_tiendas')->orderBy('id_zona', 'desc')->take(1)->get();

            DB::table('detalle_zonas_tiendas')->insert([
                'id_zona_detalle' => $zona[0]->id_zona,
                'codigo_tienda_detalle' => $codigo_tienda
            ]);

            return redirect()->route('zonas.index')
            ->with('status_success','Creación exitosa!'); 
          }


          

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Gate::authorize('haveaccess', 'zonas.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('haveaccess', 'zonas.edit');

        $usuario = DB::table('role_user')
                    ->select('users.*', 'roles.*', 'role_user.*')
                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
                    ->join('users', 'role_user.user_id', '=', 'users.id')
                    ->where('users.id', Auth::user()->id)->get();

        $tiendas = DB::table('tiendas')->get();

        if($usuario[0]->slug == 'admin'){

            $zonas = DB::table('detalle_zonas_tiendas')
                        ->select('zonas_tiendas.*', 'detalle_zonas_tiendas.*', 'tiendas.*')
                        ->join('zonas_tiendas', 'detalle_zonas_tiendas.id_zona_detalle', '=', 'zonas_tiendas.id_zona')
                        ->join('tiendas', 'detalle_zonas_tiendas.codigo_tienda_detalle', '=', 'tiendas.id_tienda')
                        ->where('id_zona', $id)->get();
            return view('zonas.edit', compact('zonas', 'usuario', 'tiendas'));
        }else{

            /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

            $detalles_del_plan = DB::table('detalle_planes_tiendas')
              ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
              ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
              ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
              ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
              ->get();

            $dia_actual = new DateTime(date('Y-m-d'));
            $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

            $funcion = $dia_actual->diff($fecha_proximo_pago);

            if($fecha_proximo_pago < $dia_actual){
              $dias_faltantes = 0;
            }else{
              $dias_faltantes = $funcion->days;
            }

          /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

          if($dias_faltantes <= 0){
            $planes = DB::table('tabla_planes')->get();
            return view('planes.tienda.pago-plan', compact('planes'));
          }else{
            
            $zonas = DB::table('detalle_zonas_tiendas')
                        ->select('zonas_tiendas.*', 'detalle_zonas_tiendas.*', 'tiendas.*')
                        ->join('zonas_tiendas', 'detalle_zonas_tiendas.id_zona_detalle', '=', 'zonas_tiendas.id_zona')
                        ->join('tiendas', 'detalle_zonas_tiendas.codigo_tienda_detalle', '=', 'tiendas.id_tienda')
                        ->where('id_zona', $id)->get();
            return view('zonas.edit', compact('zonas', 'usuario', 'tiendas'));
          }


          

        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        Gate::authorize('haveaccess', 'zonas.edit');

        $request->validate([
            'nombre' => 'required',
            'celular' => 'required|numeric',
            'estado' => 'required'
        ]);

        $usuario = DB::table('role_user')
                    ->select('users.*', 'roles.*', 'role_user.*')
                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
                    ->join('users', 'role_user.user_id', '=', 'users.id')
                    ->where('users.id', Auth::user()->id)->get();

        
        DB::table('zonas_tiendas')->where('id_zona', $id)->update([
                'nombre_zona' => $request->nombre, 
                'descripcion_zona' => $request->descripcion, 
                'celular_zona' => $request->celular, 
                'estado_zona' => $request->estado ]);


        if($usuario[0]->slug == 'admin'){
            
            $tiendas = DB::table('detalle_usuario_tiendas')
                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                ->join('users', 'personas.user_register', '=', 'users.id')
                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                ->where('users.id', Auth::user()->id)
                ->get();

                foreach ($tiendas as $tienda) {
                  $codigo_tienda = $tienda->id_tienda;
                }

                $info = DB::table('detalle_zonas_tiendas')
                    ->where([['codigo_tienda_detalle', '=', $request->tienda],['id_zona_detalle', '=', $id]])
                    ->get();

                DB::table('detalle_zonas_tiendas')->where("id_detalle_zona", $info[0]->id_detalle_zona)->delete();

                DB::table('detalle_zonas_tiendas')->insert([ 
                  'id_zona_detalle' => $id,
                  'codigo_tienda_detalle' => $request->tienda 
                ]);

                return redirect()->route('zonas.index')
                ->with('status_success','Actualización exitosa!');

        }else{


          /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

              $detalles_del_plan = DB::table('detalle_planes_tiendas')
                ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
                ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
                ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
                ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
                ->get();

              $dia_actual = new DateTime(date('Y-m-d'));
              $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

              $funcion = $dia_actual->diff($fecha_proximo_pago);

              if($fecha_proximo_pago < $dia_actual){
                $dias_faltantes = 0;
              }else{
                $dias_faltantes = $funcion->days;
              }
          /* FIN VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

          if($dias_faltantes <= 0){
            $planes = DB::table('tabla_planes')->get();
            return view('planes.tienda.pago-plan', compact('planes'));
          }else{
             /* detectar codigo de la tienda segun usuario */
            
            $tiendas = DB::table('detalle_usuario_tiendas')
                        ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                        ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                        ->join('users', 'personas.user_register', '=', 'users.id')
                        ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                        ->where('users.id', Auth::user()->id)->get();

                        foreach ($tiendas as $tienda) {
                            $codigo_tienda = $tienda->id_tienda;
                        }

             DB::table('detalle_zonas_tiendas')
                    ->where([['codigo_tienda_detalle', '=', $codigo_tienda],['id_zona_detalle', '=', $id]])->delete();

            DB::table('detalle_zonas_tiendas')->insert([ 
                'id_zona_detalle' => $id,
                'codigo_tienda_detalle' => $codigo_tienda 
            ]);
            
            return redirect()->route('zonas.index')
            ->with('status_success','Actualización exitosa!');
          }


          

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('haveaccess', 'zonas.destroy');
    }
}
