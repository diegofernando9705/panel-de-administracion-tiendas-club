<div class="container" style="padding-top: 30px; padding-bottom: 30px;">
	@foreach($personas as $persona)
		{!! Form::open(['route' => array('personas.update', $persona->id), 'files' => true]) !!}
			@csrf
			@method('PUT')

			<div class="form-group">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>Foto persona:</b></label>
						<img src="{{ asset('storage/'.$persona->foto_persona) }}">
						<input type="hidden" name="imagen_bd" value="{{ $persona->foto_persona }}">
					</div>
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Foto persona nueva:</b></label>
						<input type="file" class="form-control" placeholder="" name="foto_persona">
					</div>
				</div>
			</div>
			
			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Primer nombre:</b></label>
						<input type="text" class="form-control" placeholder="" name="primer_nombre" value="{{ $persona->primer_nombre }}">
					</div>
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b> Segundo nombre:</b></label>
						<input type="text" class="form-control" placeholder="" name="segundo_nombre" value="{{ $persona->segundo_nombre }}">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Primer apellido:</b></label>
						<input type="text" class="form-control" placeholder="" name="primer_apellido" value="{{ $persona->primer_apellido }}">
					</div>
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b> Segundo apellido:</b></label>
						<input type="text" class="form-control" placeholder="" name="segundo_apellido" value="{{ $persona->segundo_apellido }}">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Celular:</b></label>
						<input type="text" class="form-control" placeholder="" name="celular" value="{{ $persona->telefono }}">
					</div>
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Correo electronico:</b></label>
						<input type="email" class="form-control" placeholder="" name="email" value="{{ $persona->correo_electronico }}">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Departamento:</b></label>
						<select class="form-control mi-selector" id="departamento" name="departamento">
							<option value="0" selected="">Seleccionar departamento</option>
							@foreach($departamentos as $departamento)
							<option value="{{ $departamento->id }}">{{ $departamento->nombre_departamento }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Ciudad:</b></label>
						<select class="form-control mi-selector" id="ciudad" name="ciudad" required="">
							<option value="" selected="">Seleccione una ciudad</option>
						</select>
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Direccion:</b></label>
						<input type="text" class="form-control" placeholder="" name="direccion" value="{{ $persona->direccion }}">
					</div>
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Barrio:</b></label>
						<input type="text" class="form-control" placeholder="" name="barrio" value="{{ $persona->barrio }}">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Asignar al usuario:</b></label>
					<select class="form-control mi-selector" id="usuario" name="usuario" required="">
						<option value="" selected="">Seleccionar usuario</option>
						@foreach($usuarios as $usuario)
							@if($persona->user_register == $usuario->id)
								<option value="{{ $usuario->id }}" selected="">{{ $usuario->name }}</option>
							@else
								<option value="{{ $usuario->id }}">{{ $usuario->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Estado:</b></label>
					<select class="form-control mi-selector" name="estado" required="">
						<option value="" selected="">Seleccionar estado</option>
						@if($persona->estado_persona == '1')
							<option value="1" selected="">Activo</option>
							<option value="0">Inactivo</option>
						@else
						    <option value="1" selected="">Activo</option>
							<option value="0">Inactivo</option>
						@endif
						</select>
					</div>
				</div>
			</div>

			<center>
				<br>
				<button type="submit" class="btn btn-success">Actualizar</button>
			</center>
		{!! Form::close() !!}
	@endforeach
</div>
<script type="text/javascript">

	$("#departamento").change(function () {
        var valor = $(this).val();
        //alert(valor);
        $.ajax({
            type: 'get',
            url: '/busqueda/ciudad/search/' + valor,
            success: function (data) {
                document.getElementById("ciudad").innerHTML = data;
            }
        });
    });

</script>