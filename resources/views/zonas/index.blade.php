@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                              	Lista de zonas
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="messaging">
                                <b>Búsqueda de zonas:</b>

                                <input type="text" name="buscadorPersonas" id="buscadorPersonas" class="form-control" placeholder="Busca tu zona..." />                            
                            </div>
                            <br>
                            <table class="table table-bordered table-hover" id="tabla-personas">
                                <thead>
                                    <tr>
                                        <th>Nombre Zona</th>
                                        <th>Descripci&oacute;n Zona</th>
                                        <th>Celular</th>
                                        <th>Tienda</th>
                                        <th>Estado</th>
                                        <th colspan="3"><center>Acciones</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($zonas as $zona)
                                    <tr>
                                        <td>{{ $zona->nombre_zona }}</td>
                                        <td>{{ $zona->descripcion_zona }}</td>
                                        <td>{{ $zona->celular_zona }}</td>
                                        <td>{{ $zona->nombre_tienda }}</td>
                                        @if($zona->estado_zona == '1')
                                            <td class="alert alert-success">Activo</td>
                                        @else
                                            <td class="alert alert-danger">Inactivo</td>                                
                                        @endif
                                        <td>
                                            <a data-href="{{ route('zonas.edit', $zona->id_zona)}}" class="identificador-zona">
                                                <button class="btn btn-info">
                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                    </svg>
                                                </button>
                                            </a>
                                            <button class="btn btn-danger">
                                                <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-archive-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
                                                    </svg>
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $zonas->links() }}
                        </div>
                    </div>

                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modales-tiendas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-body"></div>
  </div>
</div>
@endsection
