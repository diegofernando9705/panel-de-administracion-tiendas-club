<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Permisos\Models\Role;
use App\Permisos\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;


class PermisosInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //truncate tables
        DB::statement("SET foreign_key_checks=0");
            DB::table('role_user')->truncate();
            DB::table('permission_role')->truncate();
            Permission::truncate();
            Role::truncate();
        DB::statement("SET foreign_key_checks=1");



        //user admin
        $useradmin= User::where('email','admin@admin.com')->first();
        if ($useradmin) {
            $useradmin->delete();
        }
        $useradmin= User::create([
            'name'      => 'Administrador',
            'email'     => 'admin@admin.com',
            'password'  => Hash::make('admin')    
        ]);

        //rol admin
        $roladmin=Role::create([
            'name' => 'Administrador',
            'slug' => 'admin',
            'description' => 'Super usuario del sistema',
            'full-access' => 'yes'
    
        ]);


        //clientes plan blue
        $roluser=Role::create([
            'id' => '777',
            'name' => 'Cliente plan Blue',
            'slug' => 'clienteblue',
            'description' => 'Rol para el Plan Blue',
            'full-access' => 'no'
        ]);

        
        //clientes plan basico
        $roluser=Role::create([
            'id' => '755',
            'name' => 'Cliente plan basico',
            'slug' => 'clientebasico',
            'description' => 'Rol para el Plan Basico',
            'full-access' => 'no'
        ]);


        //clientes plan free
        $roluser=Role::create([
            'id' => '600',
            'name' => 'Cliente plan free',
            'slug' => 'clientefree',
            'description' => 'Rol para el Plan Free',
            'full-access' => 'no'
        ]);

        
        //table role_user
        $useradmin->roles()->sync([ $roladmin->id ]);
      
        
        //permission
        $permission_all = [];

        
        //permission role
        $permission = Permission::create([
            'name' => 'Listar role',
            'slug' => 'role.index',
            'description' => 'El usuario puede listar todos los roles del sistema',
        ]);

        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Ver rol',
            'slug' => 'role.show',
            'description' => 'El usuario puede ver todos los roles del sistema',
        ]);

        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Crear rol',
            'slug' => 'role.create',
            'description' => 'El usuario puede crear un rol',
        ]);

        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Editar rol',
            'slug' => 'role.edit',
            'description' => 'El usuario puede editar un rol',
        ]);

        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Eliminar rol',
            'slug' => 'role.destroy',
            'description' => 'El usuario puede desactivar un rol',
        ]);

        $permission_all[] = $permission->id;
    
        


        //permission user
        $permission = Permission::create([
            'name' => 'Listar Usuario',
            'slug' => 'user.index',
            'description' => 'El usuario puede listar un usuario del sistema',
        ]);
        
        $permission_all[] = $permission->id;
        
        
        $permission = Permission::create([
            'name' => 'Ver usuario',
            'slug' => 'user.show',
            'description' => 'El usuario puede listar un usuario del sistema',
        ]);        
        
        $permission_all[] = $permission->id;
        
        
        $permission = Permission::create([
            'name' => 'Editar usuario',
            'slug' => 'user.edit',
            'description' => 'El usuario puede listar un usuario del sistema',
        ]);
        
        $permission_all[] = $permission->id;
        
        
        $permission = Permission::create([
            'name' => 'Eliminar usuario',
            'slug' => 'user.destroy',
            'description' => 'El usuario puede destroy user',
        ]);
        
        $permission_all[] = $permission->id;


        //new
        $permission = Permission::create([
            'name' => 'Ver own usuario',
            'slug' => 'userown.show',
            'description' => 'El usuario puede see own user',
        ]);
        $permission_all[] = $permission->id;
        
        
        $permission = Permission::create([
            'name' => 'Editar own usuario',
            'slug' => 'userown.edit',
            'description' => 'El usuario puede edit own user',
        ]);
        $permission_all[] = $permission->id;
        
        
        $permission = Permission::create([
            'name' => 'Create user',
            'slug' => 'user.create',
            'description' => 'El usuario puede create user',
        ]);
        $permission_all[] = $permission->id;
        
        
        //table permission_role
        //$roladmin->permissions()->sync( $permission_all);

        

        /* Modulo Tiendas */
            $permission = Permission::create([
                'name' => 'Crear tiendas',
                'slug' => 'tiendas.create',
                'description' => 'El usuario puede crear una tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Ver todas las Tiendas',
                'slug' => 'tiendas.show',
                'description' => 'El usuario puede ver todas las tiendas.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Edicion tiendas',
                'slug' => 'tiendas.edit',
                'description' => 'El usuario puede editar las tiendas.',
            ]);
            $permission_all[] = $permission->id;
            
            $permission = Permission::create([
                'name' => 'Eliminar tienda',
                'slug' => 'tiendas.destroy',
                'description' => 'El usuario puede Eliminar una tienda.',
            ]);
            $permission_all[] = $permission->id;
        /* Fin Modulo Tiendas */
        
        
        /* categorias de las Tiendas */
            $permission = Permission::create([
                'name' => 'Crear categoria tiendas',
                'slug' => 'categoryTienda.create',
                'description' => 'El usuario puede crear una categoria tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Ver todas las categorias Tiendas',
                'slug' => 'categoryTienda.show',
                'description' => 'El usuario puede ver todas las categoria tiendas.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Edicion categoria tiendas',
                'slug' => 'categoryTienda.edit',
                'description' => 'El usuario puede editar las categoria tiendas.',
            ]);
            $permission_all[] = $permission->id;
            
            $permission = Permission::create([
                'name' => 'Eliminar categoria tienda',
                'slug' => 'categoryTienda.destroy',
                'description' => 'El usuario puede Eliminar una categoria tienda.',
            ]);
            $permission_all[] = $permission->id;
        /* fin categorias de las Tiendas */
                

        /* Productos de las Tiendas */

            $permission = Permission::create([
                'name' => 'Crear productos tiendas',
                'slug' => 'productos.create',
                'description' => 'El usuario puede crear un producto de la tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Ver todos los productos de la Tienda',
                'slug' => 'productos.show',
                'description' => 'El usuario puede ver todos los productos tiendas.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Edicion productos tiendas',
                'slug' => 'productos.edit',
                'description' => 'El usuario puede editar los productos de la tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            $permission = Permission::create([
                'name' => 'Eliminar productos tienda',
                'slug' => 'productos.destroy',
                'description' => 'El usuario puede Eliminar un producto de la tienda.',
            ]);
            $permission_all[] = $permission->id;
        /* Fin productos de las Tiendas */
        

        /* Categorias de los Productos */
            $permission = Permission::create([
                'name' => 'Crear  categorias de los productos tiendas',
                'slug' => 'categoryProductos.create',
                'description' => 'El usuario puede crear un  categorias de los producto de la tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Ver todos los  categorias de los productos de la Tienda',
                'slug' => 'categoryProductos.show',
                'description' => 'El usuario puede ver todos los  categorias de los productos tiendas.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Edicion  categorias de los productos tiendas',
                'slug' => 'categoryProductos.edit',
                'description' => 'El usuario puede editar los  categorias de los productos de la tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            $permission = Permission::create([
                'name' => 'Eliminar  categorias de los productos tienda',
                'slug' => 'categoryProductos.destroy',
                'description' => 'El usuario puede Eliminar una  categoria del producto de la tienda.',
            ]);
            $permission_all[] = $permission->id;
        /* Fin categorias de los productos*/
        
        
        /* Adicionales de los Productos */
            $permission = Permission::create([
                'name' => 'Crear  adicionales de los productos tiendas',
                'slug' => 'adicionales.create',
                'description' => 'El usuario puede crear adicionales de los producto de la tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Ver todos las adicionales de los productos de la Tienda',
                'slug' => 'adicionales.show',
                'description' => 'El usuario puede ver todos los adicionales de los productos tiendas.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Edicion adicionales de los productos tiendas',
                'slug' => 'adicionales.edit',
                'description' => 'El usuario puede editar los adicionales de los productos de la tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            $permission = Permission::create([
                'name' => 'Eliminar adicionales de los productos tienda',
                'slug' => 'adicionales.destroy',
                'description' => 'El usuario puede Eliminar un adicional del producto de la tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            $permission = Permission::create([
                'name' => 'Asignar adicional a los productos tienda',
                'slug' => 'adicionales.asignar',
                'description' => 'El usuario puede asignar un adicional al producto de la tienda.',
            ]);
            $permission_all[] = $permission->id;
        /* Fin adicionales sw los productos*/


        /* Buzon de los Formularios */

            $permission = Permission::create([
                'name' => 'Ver formulario de sugerencias',
                'slug' => 'sugerencias.index',
                'description' => 'El usuario puede ver todas las sugerencias de su tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Eliminar sugerencia de la tienda',
                'slug' => 'sugerencias.destroy',
                'description' => 'El usuario puede eliminar las sugerencias de las tiendas.',
            ]);
            $permission_all[] = $permission->id;
        /* Fin buzon de formularios */


        /* Zonas de las tiendas */
        
            $permission = Permission::create([
                'name' => 'Crear  zona de las tiendas',
                'slug' => 'zonas.create',
                'description' => 'El usuario puede crear zonas de las tienda.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Ver zonas de la tienda',
                'slug' => 'zonas.show',
                'description' => 'El usuario puede ver todas las zonas de las tiendas.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Edicion zonas de las tiendas',
                'slug' => 'zonas.edit',
                'description' => 'El usuario puede editar las zonas de las tiendas.',
            ]);
            $permission_all[] = $permission->id;
            

            $permission = Permission::create([
                'name' => 'Eliminar zonas de las tiendas',
                'slug' => 'zonas.destroy',
                'description' => 'El usuario puede Eliminar una zona de la tienda.',
            ]);
            $permission_all[] = $permission->id;
        /* Fin zonas de las tiendas */


        /* Planes de las tiendas */

            $permission = Permission::create([
                'name' => 'Ver  planes',
                'slug' => 'planes.index',
                'description' => 'El usuario puede ver todos los planes.',
            ]);
            $permission_all[] = $permission->id;


            $permission = Permission::create([
                'name' => 'Crear  plan',
                'slug' => 'planes.create',
                'description' => 'El usuario puede crear un plan.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Ver planes',
                'slug' => 'planes.show',
                'description' => 'El usuario puede ver un plan.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Edicion planes de las tiendas',
                'slug' => 'planes.edit',
                'description' => 'El usuario puede editar un plan.',
            ]);
            $permission_all[] = $permission->id;
            

            $permission = Permission::create([
                'name' => 'Eliminar plan',
                'slug' => 'plan.destroy',
                'description' => 'El usuario puede Eliminar un plan.',
            ]);
            $permission_all[] = $permission->id;
        /* Fin zonas de las tiendas */


        /* Cupones de las tiendas */
            
            $permission = Permission::create([
                'name' => 'Cupones index',
                'slug' => 'cupones.index',
                'description' => 'Ver cupones.',
            ]);
            $permission_all[] = $permission->id;


            $permission = Permission::create([
                'name' => 'Crear  cupon',
                'slug' => 'cupones.create',
                'description' => 'El usuario puede crear un cupon.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Ver cupones',
                'slug' => 'cupones.show',
                'description' => 'El usuario puede ver un cupon.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Edicion cupones de las tiendas',
                'slug' => 'cupones.edit',
                'description' => 'El usuario puede editar un cupon.',
            ]);
            $permission_all[] = $permission->id;
            

            $permission = Permission::create([
                'name' => 'Eliminar cupon',
                'slug' => 'cupones.destroy',
                'description' => 'El usuario puede Eliminar un cupon.',
            ]);
            $permission_all[] = $permission->id; 
        /* Fin zonas de las tiendas */


        /* Historial de pedidos */
            
            $permission = Permission::create([
                'name' => 'Historial pedidos',
                'slug' => 'historial_pedidos.index',
                'description' => 'Historial de perdidos.',
            ]);
            $permission_all[] = $permission->id;


            $permission = Permission::create([
                'name' => 'Historial de pedidos Localmente',
                'slug' => 'historial_pedidos.local',
                'description' => 'Historial de pedidos localmente.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Historial de pedidos Domicilios',
                'slug' => 'historial_pedidos.domicilio',
                'description' => 'Historial de pedidos con domicilio localmente.',
            ]);
            $permission_all[] = $permission->id;
        /* Fin Historial de pedidos */



         /* Modulo de epayco */
            
            $permission = Permission::create([
                'name' => 'Epayco ',
                'slug' => 'epayco.index',
                'description' => 'Epayco principal.',
            ]);
            $permission_all[] = $permission->id;


            $permission = Permission::create([
                'name' => 'Epayco ver claves y cliente ID',
                'slug' => 'epayco.show',
                'description' => 'Ver configuracion de Epayco.',
            ]);
            $permission_all[] = $permission->id;
            
            
            $permission = Permission::create([
                'name' => 'Epayco crear',
                'slug' => 'epayco.create',
                'description' => 'Crear configuracion de Epayco.',
            ]);
            $permission_all[] = $permission->id;


            $permission = Permission::create([
                'name' => 'Epayco eliminar',
                'slug' => 'epayco.destroy',
                'description' => 'Eliminar configuracion de epayco.',
            ]);
            $permission_all[] = $permission->id;
            
        /* Fin Historial de pedidos */
        
    }
}
