@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                              Edicion del rol
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
<form action="{{ route('role.update', $role->id)}}" method="POST">
                     @csrf
                     @method('PUT')

                    
                        <h3>Información requerida</h3>

                         <div class="form-group">                            
                            <input type="text" class="form-control" 
                            id="name" 
                            placeholder="Name"
                            name="name"
                            value="{{ old('name', $role->name)}}"
                            readonly>
                          </div>
                          <div class="form-group">                            
                            <input type="text" 
                            class="form-control" 
                            id="slug" 
                            placeholder="Slug"
                            name="slug"
                            value="{{ old('slug' , $role->slug)}}"
                            readonly>
                          </div>

                          <div class="form-group">
                            
                            <textarea  readonly class="form-control" placeholder="Description" name="description" id="description" rows="3">{{old('description', $role->description)}}</textarea>
                          </div>

                          <hr>

                          <h3>Acceso de administrador</h3>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input disabled type="radio" id="fullaccessyes" name="full-access" class="custom-control-input" value="yes"
                            @if ( $role['full-access']=="yes") 
                              checked 
                            @elseif (old('full-access')=="yes") 
                              checked 
                            @endif
                            
                            
                            >
                            <label class="custom-control-label" for="fullaccessyes">Si</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input disabled type="radio" id="fullaccessno" name="full-access" class="custom-control-input" value="no" 
                            
                            @if ( $role['full-access']=="no") 
                              checked 
                            @elseif (old('full-access')=="no") 
                              checked 
                            @endif
                            
                            
                            >
                            <label class="custom-control-label" for="fullaccessno">Acceso personalizado</label>
                          </div>

                          <hr>


                          <h3>Permisos del sistema</h3>


                          @foreach($permissions as $permission)

                          
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" 
                            disabled
                            class="custom-control-input" 
                            id="permission_{{$permission->id}}"
                            value="{{$permission->id}}"
                            name="permission[]"

                            @if( is_array(old('permission')) && in_array("$permission->id", old('permission'))    )
                            checked

                            @elseif( is_array($permission_role) && in_array("$permission->id", $permission_role)    )
                            checked

                            @endif
                            >
                            <label class="custom-control-label" 
                                for="permission_{{$permission->id}}">
                                {{ $permission->id }}
                                - 
                                {{ $permission->name }} 
                                <em>( {{ $permission->description }} )</em>
                            
                            </label>
                          </div>


                          @endforeach
                          <hr>
                          
                          <a class="btn btn-success" href="{{route('role.edit',$role->id)}}">Editar</a>
                          <a class="btn btn-danger" href="{{route('role.index')}}">Regresar</a>

                    </form>
                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modales-tiendas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-body">
        
    </div>
  </div>
</div>


@endsection
