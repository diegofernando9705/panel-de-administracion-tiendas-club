<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Epayco extends Model
{
   
    protected $fillable = [
        'client_id', 
        'clave_id', 
        'public_key',
		'private_key',
        'epay_tienda', 
        'estado_epayco',
    ];
    
}
