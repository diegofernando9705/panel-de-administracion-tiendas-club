<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Gate;
use DateTime;

class AdicionalesController extends Controller
{	
	public function index(){

      Gate::authorize('haveaccess','adicionales.show');

      $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

      foreach ($usuario as $user) {
        if ($user->slug == "admin") {

                $adicionales = DB::table('adicionales_productos')->get();
                
                return view('adicionales_productos.administrator.index', compact('adicionales'));
        } else {

          $productos_tienda = [];
          $detalle_adicionales_tienda = [];

          $tiendas = DB::table('detalle_usuario_tiendas')
                      ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                      ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                      ->join('users', 'personas.user_register', '=', 'users.id')
                      ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                      ->where('users.id', Auth::user()->id)->get();

          foreach ($tiendas as $tienda) {
            $array_codigo = $tienda->id_tienda;
          }


          $adicionales_tienda = DB::table('detalle_adicionales_tienda')->where('codigo_tienda', $array_codigo)->get();

          foreach ($adicionales_tienda as $adicion_tienda) {
            $detalle_adicionales_tienda[] = $adicion_tienda->codigo_adicional;
          }


          $adicionales =  DB::table('adicionales_productos')->get();

          /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

            $detalles_del_plan = DB::table('detalle_planes_tiendas')
              ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
              ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
              ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
              ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
              ->get();

            $dia_actual = new DateTime(date('Y-m-d'));
            $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

            $funcion = $dia_actual->diff($fecha_proximo_pago);

            if($fecha_proximo_pago < $dia_actual){
              $dias_faltantes = 0;
            }else{
              $dias_faltantes = $funcion->days;
            }

          /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

          if($dias_faltantes <= 0){
            $planes = DB::table('tabla_planes')->get();
            return view('planes.tienda.pago-plan', compact('planes'));
          }else{
            return view('adicionales_productos.index', compact('adicionales', 'detalle_adicionales_tienda'));
          }


        }

      }
   
	}

	public function crear() {
      
      Gate::authorize('haveaccess','adicionales.create');

      /* CAPTURAR LOS PRODUCTOS DE LA TIENDA DEL USUARIO*/

      $array_codigo = [];
      $productos_tienda = [];

      $tiendas = DB::table('detalle_usuario_tiendas')
                  ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                  ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                  ->join('users', 'personas.user_register', '=', 'users.id')
                  ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                  ->where('users.id', Auth::user()->id)->get();

      foreach ($tiendas as $tienda) {
        $array_codigo[] = $tienda->id_tienda;
      }


      $productos = DB::table('detalle_tienda_productos')
                ->select('productos.name', 'productos.description', 'productos.id_categoria', 'productos.valor', 'productos.estado_producto', 'detalle_tienda_productos.*', 'productos.id as codigo_producto', 'detalle_tienda_productos.*', 'tiendas.id_tienda')
                    ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                    ->join('tiendas', 'detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                    ->get();


 $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {

                return view('adicionales_productos.create', compact('productos', 'array_codigo'));
              
            }else{
              /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

                $detalles_del_plan = DB::table('detalle_planes_tiendas')
                  ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
                  ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
                  ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
                  ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
                  ->get();

                $dia_actual = new DateTime(date('Y-m-d'));
                $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

                $funcion = $dia_actual->diff($fecha_proximo_pago);

                if($fecha_proximo_pago < $dia_actual){
                  $dias_faltantes = 0;
                }else{
                  $dias_faltantes = $funcion->days;
                }

              /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

              if($dias_faltantes <= 0){
                $planes = DB::table('tabla_planes')->get();
                return view('planes.tienda.pago-plan', compact('planes'));
              }else{

                /* VALIDAMOS CUANDO SEA PLAN FREE PARA QUE NO AGREGUE MAS DE 10 PRODUCTOS */

                $total_productos = DB::table('detalle_tienda_productos')
                        ->select('productos.*', 'detalle_tienda_productos.*')
                        ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->where('id_tienda', $tiendas[0]->id_tienda)
                        ->count();

                $captura_rol = DB::table('role_user')->where('user_id', Auth::user()->id)->get();

                if($captura_rol[0]->role_id == '755'){
                  if($total_productos <= 10){
                    return view('adicionales_productos.create', compact('productos', 'array_codigo'));  
                  }else{
                     return view('productos.mensaje_actualizar_plan');
                  }
                }else{
                  return view('adicionales_productos.create', compact('productos', 'array_codigo'));
                }

                

                /* FIN VALIDACIONES CUANDO SEA PLAN FREE PARA QUE NO AGREGUE MAS DE 10 PRODUCTOS */
                
              }
              

            }
        }

          

      
  }

  public function store(Request $request) {

    Gate::authorize('haveaccess','adicionales.create');

    $request->validate([
      'nombre_adicional' => 'required',
      'valor_adicional' => 'required|numeric'
    ]);


    $id_adicional = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);

    DB::table('adicionales_productos')->insert([
      'id_unico' => $id_adicional,
      'nombre_adicional' => $request->nombre_adicional,
      'descripcion_adicional' => $request->descripcion_adicional,
      'valor_adicional' => $request->valor_adicional
    ]);

    $code_temporal = rand(0, 150);

    if(empty($request->productos)){
      
      DB::table('detalle_adicionales_productos')->insert([
        'id_adicional_detalle' => $id_adicional,
        'id_producto_detalle' => $code_temporal
      ]);

    }else{
      
      foreach ($request->productos as $producto) {
        DB::table('detalle_adicionales_productos')->insert([
          'id_adicional_detalle' => $id_adicional,
          'id_producto_detalle' => $producto
        ]);
      }

    }


    



    $tiendas = DB::table('detalle_usuario_tiendas')
                  ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                  ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                  ->join('users', 'personas.user_register', '=', 'users.id')
                  ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                  ->where('users.id', Auth::user()->id)->get();

    foreach ($tiendas as $tienda) {
      $array_codigo = $tienda->id_tienda;
    }

    DB::table('detalle_adicionales_tienda')->insert([
      'codigo_adicional' => $id_adicional,
      'codigo_tienda' => $array_codigo
    ]);


    return redirect()->route('adicionales.index')
            ->with('status_success','Creación exitosa!'); 
  }




  public function edicion($id){
    Gate::authorize('haveaccess','adicionales.show');

    $codigo_productos_adicionales = [];
    $codigo_productos_tienda = [];



    $adicionales = DB::table('adicionales_productos')->where('id_unico', $id)->get();
    
    $adicionales_productos = DB::table('detalle_adicionales_productos')->where('id_adicional_detalle', $id)->get();

    //print_r($adicionales_productos);

    foreach ($adicionales_productos as $value) {
      $codigo_productos_adicionales[] = $value->id_producto_detalle;
    }

     /* Busqueda de productos segun la tienda */

    $tiendas = DB::table('detalle_usuario_tiendas')
                  ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                  ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                  ->join('users', 'personas.user_register', '=', 'users.id')
                  ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                  ->where('users.id', Auth::user()->id)->get();

      foreach ($tiendas as $tienda) {
        $array_codigo[] = $tienda->id_tienda;
      }


      $productos = DB::table('detalle_tienda_productos')
                ->select('productos.name', 'productos.description', 'productos.id_categoria', 'productos.valor', 'productos.estado_producto', 'detalle_tienda_productos.*', 'productos.id as codigo_producto', 'detalle_tienda_productos.*', 'tiendas.id_tienda')
                    ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                    ->join('tiendas', 'detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                    ->get();

      foreach($productos as $producto){

        if (is_array($array_codigo) && in_array($producto->id_tienda, $array_codigo)) {
            $codigo_productos_tienda[] = $producto->id;
        }

      }

      /* fin busqueda de productos segun la tienda */
      
    return view('adicionales_productos.edicion', compact('adicionales', 'productos', 'codigo_productos_adicionales', 'codigo_productos_tienda'));
  }


  public function update(Request $request, $id){
    
    Gate::authorize('haveaccess','adicionales.edit');

    $request->validate([
      'nombre_adicional' => 'required',
      'valor_adicional' => 'required',
      'productos' => 'required',
    ]);

    $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

      foreach ($usuario as $user) {
        
        if ($user->slug == "admin") {
                $adicionales = DB::table('adicionales_productos')->get();
                
                return view('adicionales_productos.administrator.index', compact('adicionales'));
        } else {

          $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();

          foreach ($tiendas as $tienda) {
            $codigo_tienda = $tienda->id_tienda;
          }



           DB::table('detalle_adicionales_productos')->where('id_adicional_detalle', $id)->delete();
            DB::table('detalle_adicionales_tienda')->where('codigo_adicional', $id)->delete();


            DB::table('adicionales_productos')->where('id_unico', $id)->update([
                'nombre_adicional' => $request->nombre_adicional,
                'descripcion_adicional' => $request->descripcion_adicional,
                'valor_adicional' => $request->valor_adicional,
            ]);

            DB::table('detalle_adicionales_tienda')->insert([
              'codigo_adicional' => $id,
              'codigo_tienda' => $codigo_tienda
            ]);


            foreach ($request->productos as $producto) {
              DB::table('detalle_adicionales_productos')->insert([
                'id_adicional_detalle' => $id,
                'id_producto_detalle' => $producto
              ]);
            }

             return redirect()->route('adicionales.index')
                    ->with('status_success','Actualización exitosa!'); 

        }
      }

  }



  public function show($id){

    Gate::authorize('haveaccess','adicionales.show');

    $codigo_productos = [];

    $adicionales = DB::table('adicionales_productos')->where('id_unico', $id)->get();
    
    $adicionales_productos = DB::table('detalle_adicionales_productos')->where('id_adicional_detalle', $id)->get();

    //print_r($adicionales_productos);

    foreach ($adicionales_productos as $value) {
      $codigo_productos[] = $value->id_producto_detalle;
    }

    $productos = DB::table('productos')->get();

    return view('adicionales_productos.ver', compact('adicionales', 'productos', 'codigo_productos'));
  }

  public function delete($id){

    Gate::authorize('haveaccess','adicionales.destroy');

    DB::table('adicionales_productos')->where('id_unico', $id)->delete();
    DB::table('detalle_adicionales_productos')->where('id_adicional_detalle', $id)->delete();
    DB::table('detalle_adicionales_tienda')->where('codigo_adicional', $id)->delete();


    return redirect()->route('adicionales.index')
                    ->with('status_success','Eliminación exitosa'); 
  }
}
