<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permisos\Models\Role;
use App\User;
use Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {      

        Gate::authorize('haveaccess', 'user.index');

        $this->authorize('haveaccess','user.index');
        $users =  User::with('roles')->orderBy('id','Desc')->paginate(20);
        //return $users; 

        return view('user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        Gate::authorize('haveaccess', 'user.create');

        //$this->authorize('create', User::class);
        //return 'Create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('haveaccess', 'user.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {      

        Gate::authorize('haveaccess', 'user.create');

        $this->authorize('view', [$user, ['user.show','userown.show'] ]);
        $roles= Role::orderBy('name')->get();

        //return $roles;
        return view('user.view', compact('roles', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {      

        Gate::authorize('haveaccess', 'user.edit');

        $this->authorize('update', [$user, ['user.edit','userown.edit'] ]);
        $roles= Role::orderBy('name')->get();
        
        //return $roles;
        
        return view('user.edit', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {      

        Gate::authorize('haveaccess', 'user.edit');

        $request->validate([
            'name'          => 'required|max:50',
            'email'         => 'required|max:50|unique:users,email,'.$user->id            
        ]);

        //dd($request->all());

        $user->update($request->all());

        $user->roles()->sync($request->get('roles'));
        
        return redirect()->route('user.index')
            ->with('status_success','Actualización exitosa!'); 



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        Gate::authorize('haveaccess', 'user.destroy');

        $this->authorize('haveaccess','user.destroy');
        $user->delete();

        return redirect()->route('user.index')
            ->with('status_success','Usuario eliminado'); 
    }
}
