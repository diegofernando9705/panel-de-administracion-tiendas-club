$(document).ready(function () {
    
    $(document).ready(function () {
        $('.mi-selector').select2();
    });


    $("#nombreTiendaUnico").keyup(function(){              
        var ta      =   $("#nombreTiendaUnico");
        letras      =   ta.val().replace(/ /g, "-");
        ta.val(letras)
    });

    /* Busqueda ciudad segun departamento */
    $("#departamento").change(function () {
        var valor = $(this).val();
        //alert(valor);
        $.ajax({
            type: 'get',
            url: '/busqueda/ciudad/search/' + valor,
            success: function (data) {
                document.getElementById("ciudad").innerHTML = data;
            }
        });
    });


    /* ACCIONES DEL MODULO DE LA TIENDA*/

    $(document).on("click", ".identificador-tienda", function(){
        
        $('#modales-tiendas').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });

    });


    $(document).on("click", ".identificador-zona", function(){
        
        $('#modales-tiendas').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });

    });



    $(document).on("click", ".identificador-tienda-ver", function(){
        
        $('#modales-tiendas').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });

    });


    $(document).on("keyup", "#buscadorTienda", function(){
        
        var variable = $(this).val();
        
        $.ajax({
            url: "/tiendas/keyup/" + variable,
            method: 'get',
            success: function(data){
                $("#table_tienda").html(data);
            }
        });
    });

    /* FIN ACCIONES DEL MODULO DE LA TIENDA*/


    /* ACCIONES DEL MODULO DE PRODUCTOS*/

    $(document).on("click", ".identificador-producto-update", function(){
        
        $('#modales-tiendas').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });

    });

    $(document).on("click", ".identificador-producto-ver", function(){
        
        $('#modales-tiendas').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });

    });

    $(document).on("keyup", "#buscadorProductos", function(){
        
        var variable = $(this).val();
        
        $.ajax({
            url: "/productos/keyup/" + variable,
            method: 'get',
            success: function(data){
                $("#tabla-productos").html(data);
            }
        });
    });
    /* FIN ACCIONES DEL MODULO PRODUCTOS*/



    /* ACCIONES DEL MODULO DE CATEGORIAS DEL PRODUCTOS*/

    $(document).on("click", ".identificador-categoria-producto-update", function(){
        
        $('#modales-tiendas').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });

    });

    $(document).on("click", ".identificador-categoria-producto-ver", function(){
        
        $('#modales-tiendas').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });

    });

    $(document).on("keyup", "#buscadorCategoriasProductos", function(){
       
        var variable = $(this).val();
        
        $.ajax({
            url: "/categoriasproductos/keyup/" + variable,
            method: 'get',
            success: function(data){
                $("#table_categorias_productos").html(data);
            }
        });
    });
    /* FIN ACCIONES DEL MODULO PRODUCTOS*/





    /* ACCIONES DEL MODULO ADICIONES DE PRODUCTOS*/

   

    $(document).on("click", ".identificador-adicion-ver", function(){
        
        $('#modales-tiendas').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                
                $("#modal-body").html(data);

            }
        });

    });


    $(document).on("click", ".identificador-adicion-edicion", function(){
        
        $('#modales-tiendas').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                
                $("#modal-body").html(data);

            }
        });

    });

    $(document).on("keyup", "#buscadorProductos", function(){
        
        var variable = $(this).val();
        
        $.ajax({
            url: "/productos/keyup/" + variable,
            method: 'get',
            success: function(data){
                $("#tabla-productos").html(data);
            }
        });
    });
    /* FIN ACCIONES DEL MODULO ADICIONAES DEL PRODUCTOS*/


    /* HISTORIAL DE PEDIDOS */


    $(document).on("click", ".identificador-pedido", function(){
        
        $('#modales-tiendas').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });

    });

    /* FIN HISTORIAL DE PEDIDOS */

    /* HORARIOS TIENDAS */
    $(document).on("click", "#todos", function(){

        if ($(this).prop('checked') == true) {
            
            document.getElementById("1").disabled = true;
            document.getElementById("hora_inicio_1").disabled = true;
            document.getElementById("hora_fin_1").disabled = true;
            
            document.getElementById("2").disabled = true;
            document.getElementById("hora_inicio_2").disabled = true;
            document.getElementById("hora_fin_2").disabled = true;

            document.getElementById("3").disabled = true;
            document.getElementById("hora_inicio_3").disabled = true;
            document.getElementById("hora_fin_3").disabled = true;

            document.getElementById("4").disabled = true;
            document.getElementById("hora_inicio_4").disabled = true;
            document.getElementById("hora_fin_4").disabled = true;
            
            document.getElementById("5").disabled = true;
            document.getElementById("hora_inicio_5").disabled = true;
            document.getElementById("hora_fin_5").disabled = true;
            
            document.getElementById("6").disabled = true;
            document.getElementById("hora_inicio_6").disabled = true;
            document.getElementById("hora_fin_6").disabled = true;

            document.getElementById("7").disabled = true;
            document.getElementById("hora_inicio_7").disabled = true;
            document.getElementById("hora_fin_7").disabled = true;

            document.getElementById("hora_inicio_8").disabled = false;
            document.getElementById("hora_fin_8").disabled = false;

       }else{
            document.getElementById("hora_inicio_8").disabled = true;
             document.getElementById("hora_fin_8").disabled = true;


            document.getElementById("1").disabled = false;
            document.getElementById("hora_inicio_1").disabled = false;
            document.getElementById("hora_fin_1").disabled = false;
            
            document.getElementById("2").disabled = false;
            document.getElementById("hora_inicio_2").disabled = false;
            document.getElementById("hora_fin_2").disabled = false;

            document.getElementById("3").disabled = false;
            document.getElementById("hora_inicio_3").disabled = false;
            document.getElementById("hora_fin_3").disabled = false;

            document.getElementById("4").disabled = false;
            document.getElementById("hora_inicio_4").disabled = false;
            document.getElementById("hora_fin_4").disabled = false;
            
            document.getElementById("5").disabled = false;
            document.getElementById("hora_inicio_5").disabled = false;
            document.getElementById("hora_fin_5").disabled = false;
            
            document.getElementById("6").disabled = false;
            document.getElementById("hora_inicio_6").disabled = false;
            document.getElementById("hora_fin_6").disabled = false;

            document.getElementById("7").disabled = false;
            document.getElementById("hora_inicio_7").disabled = false;
            document.getElementById("hora_fin_7").disabled = false;

            document.getElementById("hora_inicio_8").disabled = true;
            document.getElementById("hora_fin_8").disabled = true;
        }
    });


    /* MODAL PARA EDICION */

    $(document).on("click", ".abrir-edicion", function(){
            
         $('#modal-edicion-general').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body").html(data);
            }
        });

    });



    /* MODAL PARA VER */

    $(document).on("click", ".abrir-detalle", function(){
        
         $('#modal-informacion-general').modal({
            show: true
        });

        href = $(this).attr("data-href");
        
        $.ajax({
            url: href,
            method: 'get',
            success: function(data){
                $("#modal-body-show").html(data);
            }
        });
    });


    /* CUPONES */


    $(document).on("change", "#tipo_cupon", function(){

        $valor = $(this).val();
        
        if($valor === 'cantidad'){
            $("#cantidad").attr("style", "display:block;");

            $("#fecha").attr("style", "display:none;");
            $("#personalizado").attr("style", "display:none;");

        }else if($valor === 'fecha'){
            $("#fecha").attr("style", "display:block;");

            $("#cantidad").attr("style", "display:none;");
            $("#personalizado").attr("style", "display:none;");
        }else{
            $("#personalizado").attr("style", "display:block;");

            $("#cantidad").attr("style", "display:none;");
            $("#fecha").attr("style", "display:none;");
        }
    });

    /* OPCIONES ACTUALIZAR PLAN */
    
    $(document).on("click", "#continuar_metodo", function(){
        var formData = $("#update_plan").serialize();
       

                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: "POST",
                    url: "/metodoUpdate",// ó {{url(/admin/empresa)}} depende a tu peticion se dirigira a el index(get) o tu store(post) de tu controlador 
                    data: formData,
                    success: function (data) {

                        $("#update_plan").html(data);
                        $(this).attr("id","proceder_pago");
                   },
                    error: function (err) {
                        if (err.status == 422) { // when status code is 422, it's a validation issue
                            console.log(err.responseJSON);
                            $('#success_message').fadeIn().html(err.responseJSON.message);
                            
                            // you can loop through the errors object and show it to the user
                            console.warn(err.responseJSON.errors);
                            // display errors on each form field
                            $.each(err.responseJSON.errors, function (i, error) {
                                var el = $(document).find('[name="'+i+'"]');
                                el.after($('<span style="color: red;">'+error[0]+'</span>'));
                            });
                        }
                    }
                });
    });

    $(document).on("click", ".refrescar", function(){
        location.reload();
    });

    /*proceder pago con tarjeta de credito*/
    
    $(document).on("click", "#proceder_pago", function(){
        
        var formulario = $("#update_plan").serialize();
        
        var metodo = $(this).attr("data-metodo");


        $("#caja_superpuesta").attr("style","position: absolute; width: 96%; color:white; text-align:center;   align-items: center; display: flex; justify-content: center; background-color: rgba(0,0,0,0.5); height: 100%; z-index: 9;")
        $("#caja_superpuesta").html("<div class='spinner'></div>");

        if(metodo == 'nequi'){

            if($("#cardNumberCelular").val().trim().length > 0){

                var celular = $("#cardNumberCelular").val();

                var expresion = /^3[\d]{9}$/;

                // valida si esta vacio ò si no es celular
                if (isNaN(celular) || !expresion.test(celular)){

                    $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div> Número de celular incorrecto.");
                
                    setTimeout(
                        function(){
                            $("#caja_superpuesta").attr("style",""); 
                            $("#caja_superpuesta").html(" "); 
                        }, 2000);

                    return false;
                }else{
                    
                    $.ajax({
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        type: "POST",
                        url: "/pagoActualizacionPlan",
                        data: formulario,
                        success: function (data) {
                            $("#caja_superpuesta").attr("style","position: absolute; width: 96%; color:white; text-align:center;   align-items: center; display: flex; justify-content: center; background-color: rgba(0,0,0,0.5); height: 100%; z-index: 9;")
                            $("#caja_superpuesta").html("<div class='spinner'></div>");
                            console.log('ENVIA A RUTA pagoActualizacionPlan');
                            ejecutandoValidacion(data);
                        },
                        error: function (err) {
                            if (err.status == 422) { 
                                console.log(err.responseJSON);
                                $('#success_message').fadeIn().html(err.responseJSON.message);
                                    console.warn(err.responseJSON.errors);
                                $.each(err.responseJSON.errors, function (i, error) {
                                    var el = $(document).find('[name="'+i+'"]');
                                    el.after($('<span style="color: red;">'+error[0]+'</span>'));
                                });

                                $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div> Ningun campo puede quedar vacio");
                    
                                setTimeout(
                                    function(){
                                        $("#caja_superpuesta").attr("style",""); 
                                        $("#caja_superpuesta").html(" "); 
                                    }, 1000);
                            }
                        }
                    });
                }

            }else{


                $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div> Número de celular incorrecto.");
                
                setTimeout(
                    function(){
                        $("#caja_superpuesta").attr("style",""); 
                        $("#caja_superpuesta").html(" "); 
                    }, 3000);

                return false;
            }
        }else if(metodo == 'tarjetas-nequi'){
            
            $("#caja_superpuesta").attr("style","position: absolute; width: 96%; color:white; text-align:center;   align-items: center; display: flex; justify-content: center; background-color: rgba(0,0,0,0.5); height: 100%; z-index: 9;")
            $("#caja_superpuesta").html("<div class='spinner'></div>");
            
            var isCardValid = $.payform.validateCardNumber(cardNumber.val());
            var isCvvValid = $.payform.validateCardCVC(CVV.val());

            setTimeout(function(){ 

                if(owner.val().length < 5){
                    $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div> Nombre de propietario incorrecto");
                    
                    setTimeout(
                        function(){
                            $("#caja_superpuesta").attr("style",""); 
                            $("#caja_superpuesta").html(" "); 
                        }, 1000);
                    return false;
                } else if (!isCardValid) {
                    $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div> Número de tarjeta incorrecto");
                    
                    setTimeout(
                        function(){
                            $("#caja_superpuesta").attr("style",""); 
                            $("#caja_superpuesta").html(" "); 
                        }, 1000);
                    return false;
                }else if (!isCvvValid) {
                    
                    $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div> CVV de tarjeta incorrecto");
                    
                    setTimeout(
                        function(){
                            $("#caja_superpuesta").attr("style",""); 
                            $("#caja_superpuesta").html(" "); 
                        }, 1000);
                    return false;
                }else{

                    $.ajax({
                        
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        type: "POST",
                        url: "/pagoActualizacionPlan",
                        data: formulario,
                        success: function (data) {
                            console.log('data');
                            if(data == 0){
                                
                                $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div> Número de tarjeta incorrecto");
                    
                                setTimeout(
                                    function(){
                                        $("#caja_superpuesta").attr("style",""); 
                                        $("#caja_superpuesta").html(" "); 
                                },  1000);

                            }else{
                                verificationTransaction(data);
                            }
                        },
                        error: function (err) {
                            if (err.status == 422) { 
                                console.log(err.responseJSON);
                                $('#success_message').fadeIn().html(err.responseJSON.message);
                                    console.warn(err.responseJSON.errors);
                                $.each(err.responseJSON.errors, function (i, error) {
                                    var el = $(document).find('[name="'+i+'"]');
                                    el.after($('<span style="color: red;">'+error[0]+'</span>'));
                                });

                                $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div> Ningun campo puede quedar vacio");
                    
                                setTimeout(
                                    function(){
                                        $("#caja_superpuesta").attr("style",""); 
                                        $("#caja_superpuesta").html(" "); 
                                    }, 1000);
                            }
                        }
                    });
                }

            }, 1500);
        }else{
        }
    });

    /* funciones pago para NEQUI - ACTUALIZACION DE PLAN */

        function ejecutandoValidacion(id_token){
            var id = setInterval(function(){
                $.ajax({
                    type: "GET",
                    url: "/wompi/verification/nequi/"+id_token,
                    success: function (data) {
                        if(data['data']['status'] == "PENDING"){
                            $("#caja_superpuesta").attr("style","position: absolute; width: 96%; color:white; text-align:center;   align-items: center; display: flex; justify-content: center; background-color: rgba(0,0,0,0.5); height: 100%; z-index: 9;")
                            $("#caja_superpuesta").html("<div clss='row'><div class='spinner'></div> <div class='col-12 col-sm-12 col-md-12 col-lg-12'>Revisa tu celular...</div></div>");
                        }else{

                            clearInterval(id);
                            
                            console.log('ENVIA A RUTA /wompi/verification/nequi/id_token');

                            if(data['data']['status'] == "DECLINED"){
                                $("#caja_superpuesta").html("<div clss='row'><div class='col-12 col-sm-12 col-md-12 col-lg-12'>TRANSACCIÓN RECHAZADA</div></div>");
                                setTimeout(
                                    function(){
                                        $("#caja_superpuesta").attr("style",""); 
                                        $("#caja_superpuesta").html(" "); 
                                    }, 
                                1500);
                            }else if(data['data']['status'] == "APPROVED"){
                                
                                var formulario = $("#update_plan").serialize();


                                $.ajax({
                                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                    type: "POST",
                                    url: "/wompi/verification/pagar/"+data['data']['id'],
                                    data: formulario,
                                    success: function (data) {
                                        clearInterval(id);
                                        console.log('IR A RUTA /wompi/verification/pagar/id_transaccion');
                                        verificationTransaction(data);
                                    },
                                    error: function (err) {

                                        if (err.status == 422) { 
                                            console.log(err.responseJSON);
                                            $('#success_message').fadeIn().html(err.responseJSON.message);
                                            console.warn(err.responseJSON.errors);
                                            $.each(err.responseJSON.errors, function (i, error) {
                                                var el = $(document).find('[name="'+i+'"]');
                                                el.after($('<span style="color: red;">'+error[0]+'</span>'));
                                            });

                                        $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div> Ningun campo puede quedar vacio");
                            
                                        setTimeout(
                                            function(){
                                                $("#caja_superpuesta").attr("style",""); 
                                                $("#caja_superpuesta").html(" "); 
                                            }, 1000);
                                    }
                                }
                            });
                            }else{

                            }
                        }
                    }
                });
            }, 1000); 
        }

        function verificationTransaction(data){
            clearInterval();

            var formulario = $("#update_plan").serialize();

            var verificacion_transaccion = setInterval(function(){
                $.ajax({
                    type: "GET",
                    url: "/wompi/verification/transaction/"+data['data']['id'],
                    success: function (data) {
                        if(data['data']['status'] == 'APPROVED'){
                            clearInterval(verificacion_transaccion);
                            registroTransaccionNequi(data);
                        }else if(data['data']['status'] == 'DECLINED'){
                            console.log(data);
                            clearInterval(verificacion_transaccion);
                            $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div>"+data['data']['status_message']);
                            
                             setTimeout(
                                function(){
                                    $("#caja_superpuesta").attr("style",""); 
                                    $("#caja_superpuesta").html(" "); 
                                }, 1000);

                        }
                        else{
                            console.log(data);
                        }
                    }
                });            
            }, 1000); 
        }

        function registroTransaccionNequi(data){
                    
            var formulario = $("#update_plan").serialize();

            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: "POST",
                url: "/wompi/verification/registro_bd/"+data['data']['id'],
                data: formulario,
                success: function (data) {
                    alert("Actualizacion del plan exitosa");
                    setTimeout(function(){
                        location.reload();
                    }, 1000)
                },
                error: function (err) {
                    if (err.status == 422) {
                        console.log(err.responseJSON);
                        $('#success_message').fadeIn().html(err.responseJSON.message);
                        console.warn(err.responseJSON.errors);
                        $.each(err.responseJSON.errors, function (i, error) {
                            var el = $(document).find('[name="'+i+'"]');
                            el.after($('<span style="color: red;">'+error[0]+'</span>'));
                        });

                        $("#caja_superpuesta").html("<img src='../../img/advertencia.png' width='30px' style='margin-right:20px;'> <div style='clear: both'></div> Ningun campo puede quedar vacio");

                        setTimeout(
                            function(){
                                $("#caja_superpuesta").attr("style",""); 
                                $("#caja_superpuesta").html(" "); 
                            }, 1000);
                    }
                }
            });
        }
    /* funciones pago para NEQUI - ACTUALIZACION DE PLAN */

    /* FIN OPCIONES ACTUALIZAR PLAN */

});
