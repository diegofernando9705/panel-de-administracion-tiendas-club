@extends('layouts.app')

@section('content')
<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                                Editar mi perfil
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                           <form action="{{ route('perfil.update')}}" method="POST" enctype="multipart/form-data">
                              @csrf
                              @method('POST')

                              @foreach($descripcion_perfil as $perfil)
                              <div class="row">
                                <div class="form-group col-6 col-md-6 col-lg-6 col-xl-6">
                                  <img src="{{ asset('/storage/'.$perfil->foto_persona) }}" width="150px"/>
                                </div>
                                <div class="form-group col-6 col-md-6 col-lg-6 col-xl-6">
                                  <label><strong><code>(*)</code> Foto de perfil:  </strong></label>
                                  <input type="file" class="form-control" id="name" placeholder="Nombre" name="foto_persona" value="{{ $perfil->foto_persona }}" />
                                </div>
                              </div>

                              <div class="form-group">
                                <label><strong><code>(*)</code> Nombre:  </strong></label>
                                <input type="text" class="form-control" id="name" placeholder="Nombre" name="nombre" value="{{ $perfil->primer_nombre }}" />
                              </div>

                              <div class="form-group">
                                <label><strong>Segundo nombre: </strong> </label>
                                <input type="text" class="form-control" id="name" placeholder="Nombre" name="segundo_nombre" value="{{ $perfil->segundo_nombre }}" />
                              </div>

                              <div class="form-group">
                                <label><strong><code>(*)</code> Apellido:  </strong></label>
                                <input type="text" class="form-control" id="name" placeholder="Nombre" name="apellido" value="{{ $perfil->primer_apellido}}" />
                              </div>

                              <div class="form-group">
                                <label><strong>Segundo apellido: </strong>: </label>
                                      <input type="text" class="form-control"  id="name"  placeholder="Nombre" name="segundo_apellido" value="{{ $perfil->segundo_apellido }}" />
                              </div>

                              <div class="form-group">
                                <label><strong><code>(*)</code> Telefono celular: </strong> </label>
                                <input type="text" class="form-control"  id="name"  placeholder="Nombre" name="celular" value="{{ $perfil->telefono }}">
                              </div>

                              <div class="form-group">
                                <label><strong><code>(*)</code> Correo electrónico: </strong></label>
                                <input type="text" class="form-control" id="correo"  value="{{ $perfil->correo_electronico }}" disabled="">
                                <input type="hidden" name="correo" value="{{ $perfil->correo_electronico }}" />
                              </div>

                              <div class="form-group">
                                <label><strong><code>(*)</code> Ciudad:</strong></label>
                                  <select class="form-control mi-selector col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" name="ciudad">
                                    @foreach($ciudades as $ciudad)
                                      @if($ciudad->codigo_ciudad == $perfil->id_ciudad)
                                        <option value="{{ $ciudad->codigo_ciudad }}" selected="">{{ $ciudad->nombre_ciudad }}</option>
                                      @else
                                        <option value="{{ $ciudad->codigo_ciudad }}">{{ $ciudad->nombre_ciudad }}</option>
                                      @endif
                                    @endforeach
                                  </select>
                              </div>

                              <div class="form-group">
                                <label><strong><code>(*)</code> Direccion: </strong></label>
                                <input type="text" class="form-control" id="name" placeholder="Nombre" name="direccion" value="{{ $perfil->direccion }}" />
                              </div>

                              <div class="form-group">
                                <label><strong><code>(*)</code> Barrio: </strong></label>
                                <input type="text" class="form-control" id="name" placeholder="Nombre" name="barrio" value="{{ $perfil->barrio }}"
                                >
                              </div>

                            @endforeach
                            <center>
                              <br><br>
                              <input class="btn btn-primary" type="submit" value="Actualizar"c />
                            </center>
                        </form>
                      </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
</div>


@endsection
