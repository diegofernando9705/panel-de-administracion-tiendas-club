<!-- Listado de las tiendas, no influye si es Administrador o Cliente -->

@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                                Configuraciones de Epayco en la Plataforma
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <!--<div class="messaging">
                                <b>Búsqueda de productos:</b>

                                <input type="text" name="buscadorProductos" id="buscadorProductos" class="form-control" placeholder="Busca tu producto..." />                            
                            </div>-->
                            <br>
                            <table class="table table-bordered table-hover" id="tabla-productos">
                                <thead>
                                    <tr>
                                        <th>ID Cliente</th>
                                        <th>ID Clave</th>
                                        <th>Tienda asignada</th>
                                        <th>Estado</th>
                                        <th colspan="3"><center>Acciones</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($epaycos as $epayco)
                                    <tr>
                                        <td>{{ Crypt::decrypt($epayco->client_id) }}</td>
                                        <td>{{ Crypt::decrypt($epayco->clave_id) }}</td>
                                        <td>{{ $epayco->epay_tienda }}</td>
                                        @if($epayco->estado_epayco == '1')
                                        <td class="alert alert-success">Activo</td>
                                        @else
                                        <td class="alert alert-danger">Inactivo</td>                                
                                        @endif

                                        
                                        @if(Gate::authorize('haveaccess','epayco.destroy'))
                                        <td>
                                            <form method="POST" action="{{ route('epayco.destroy', $epayco->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm">
                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-archive-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
                                                    </svg>
                                                </button>
                                            </form>
                                        </td>
                                        @endif
                                    </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>

@include('custom.modales')

@endsection
