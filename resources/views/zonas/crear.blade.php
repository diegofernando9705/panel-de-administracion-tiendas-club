@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                              	Registrar una nueva zona
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            {!! Form::open(['route' => 'zonas.store', 'files' => true]) !!}
                                @csrf
                                @method('POST')

                                <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                            <label style="padding-bottom: 5px;"><b>(*) Nombre zona:</b></label>
                                            <input type="text" class="form-control" placeholder="" name="nombre" value="{{ old('nombre') }}">
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                            <label style="padding-bottom: 5px;"><b> Descripci&oacute;n de la zona:</b></label>
                                            <input type="text" class="form-control" placeholder="" name="descripcion" value="{{ old('descripcion') }}">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                            <label style="padding-bottom: 5px;"><b>(*) Celular:</b></label>
                                            <input type="number" class="form-control" placeholder="" name="celular" value="{{ old('celular') }}">
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                            <label style="padding-bottom: 5px;"><b>(*) Estado:</b></label>
                                          <select class="form-control mi-selector" name="estado" required="">
                                                <option value="" selected="">Seleccionar estado</option>
                                                <option value="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                @if($usuario[0]->slug == 'admin')

                                    <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                                <label style="padding-bottom: 5px;"><b>(*) Tiendas:</b></label>
                                                <select class="form-control mi-selector" name="tienda" required="">
                                                    <option value="" selected="">Seleccionar tiendas</option>
                                                    @foreach($tiendas as $tienda)
                                                        <option value="{{ $tienda->id_tienda }}">
                                                            {{ $tienda->nombre_tienda }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                @endif

                                <center>
                                    <a href="{{ url('tienda') }}" style="color:white; ">
                                        <button type="button" class="btn btn-danger">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="25px" height="25px" class=""><g><g>
                                            <g>
                                            <path d="M256,0C114.508,0,0,114.497,0,256c0,141.493,114.497,256,256,256c141.492,0,256-114.497,256-256    C512,114.507,397.503,0,256,0z M256,472c-119.384,0-216-96.607-216-216c0-119.385,96.607-216,216-216    c119.384,0,216,96.607,216,216C472,375.385,375.393,472,256,472z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                            </g><g>
                                            <g>
                                            <path d="M343.586,315.302L284.284,256l59.302-59.302c7.81-7.81,7.811-20.473,0.001-28.284c-7.812-7.811-20.475-7.81-28.284,0    L256,227.716l-59.303-59.302c-7.809-7.811-20.474-7.811-28.284,0c-7.81,7.811-7.81,20.474,0.001,28.284L227.716,256    l-59.302,59.302c-7.811,7.811-7.812,20.474-0.001,28.284c7.813,7.812,20.476,7.809,28.284,0L256,284.284l59.303,59.302    c7.808,7.81,20.473,7.811,28.284,0C351.398,335.775,351.397,323.112,343.586,315.302z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                            </g></g> </svg>
                                            Cancelar registro
                                        </button>
                                    </a>

                                    <button type="submit" class="btn btn-info" id="registroTienda">Registrar tienda</button>
                                </center>
                                {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>      
        </div>
    </div>
</div>


@endsection
