<div class="modal fade bd-example-modal-lg" id="modal-edicion-general" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-content modal-dialog modal-lg">
      <div class="modal-header">
        <h5 class="modal-title">Edici&oacute;n de informaci&oacute;n</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <!--
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>-->
    </div>
</div>


<div class="modal fade bd-example-modal-lg" id="modal-informacion-general" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-content modal-dialog modal-lg">
      <div class="modal-header">
        <h5 class="modal-title">Informaci&oacute;n detallada</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body-show">
        <p>Modal body text goes here.</p>
      </div>
      <!--
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>-->
    </div>
</div>