<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaPlanesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_planes', function (Blueprint $table) {
            $table->increments('id_plan');
            $table->char('nombre_plan', 150);
            $table->char('descripcion_plan', 200)->nullable();
            $table->integer('valor_plan');
            $table->integer('estado_plan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_planes');
    }
}
