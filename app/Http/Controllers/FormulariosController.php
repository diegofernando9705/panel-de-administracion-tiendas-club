<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use Auth;
use DB;


class FormulariosController extends Controller
{
    public function indexSugerencias(){
    	
    	Gate::authorize('haveaccess','sugerencias.index');
        
        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

		if($usuario[0]->slug == "admin") {

			$sugerencias = DB::table('detalle_buzon_tienda')
							->select('detalle_buzon_tienda.*','tiendas.*','formulario_buzon_sugerencia.*')
							->join('tiendas', 'detalle_buzon_tienda.codigo_tienda_buzon_fk', '=', 'tiendas.id_tienda')
							->join('formulario_buzon_sugerencia', 'detalle_buzon_tienda.codigo_formulario_buzon_fk', '=', 'formulario_buzon_sugerencia.id')
							->get();

                return view('formularios.sugerencias.index', compact('sugerencias'));
        } else {


        	$tiendas = DB::table('detalle_usuario_tiendas')
        				->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
        				->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
        				->join('users', 'personas.user_register', '=', 'users.id')
        				->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
        				->where('users.id', Auth::user()->id)->get();

        	$sugerencias = DB::table('detalle_buzon_tienda')
        					->select('detalle_buzon_tienda.*','tiendas.*','formulario_buzon_sugerencia.*')
        					->join('tiendas', 'detalle_buzon_tienda.codigo_tienda_buzon_fk', '=', 'tiendas.id_tienda')
        					->join('formulario_buzon_sugerencia', 'detalle_buzon_tienda.codigo_formulario_buzon_fk', '=', 'formulario_buzon_sugerencia.id')
        					->where('detalle_buzon_tienda.codigo_tienda_buzon_fk', $tiendas[0]->id_tienda)
        					->paginate(10);

               return view('formularios.sugerencias.index', compact('sugerencias'));
       	}
    }

    public function deletesugerencias($id){

        Gate::authorize('haveaccess','sugerencias.destroy');

    	DB::table('detalle_buzon_tienda')->where('codigo_formulario_buzon_fk', $id)->delete();
    	DB::table('formulario_buzon_sugerencia')->where('id', $id)->delete();


        return redirect()->route('sugerencias.index')
            ->with('status_success','Eliminado correctamente!'); 
    }

    public function indexContacto(){

        Gate::authorize('haveaccess','contacto.index');
        
            $contactos = DB::table('formulario_contacto')->get();

        return view('formularios.contacto.index', compact('contactos'));
        
    }
}
