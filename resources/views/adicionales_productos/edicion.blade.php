<div class="tituloModal">
    <div class="container">
        <h3>
            <strong>Edición de la adición</strong>
        </h3>
    </div>
</div> 
<div class="container" class="contenerdorFormulario">
	@foreach($adicionales as $adicion)
	    {!! Form::open(['route' => array('adicionales.update', $adicion->id_unico), 'files' => true]) !!}
			
			@csrf
			@method('POST')

			<div class="form-group">
				<label><strong><code>(*) </code>Nombre Adicional: </strong></label>
				<input type="text" class="form-control" name="nombre_adicional" value="{{ $adicion->nombre_adicional }}" />
			</div>

			<div class="form-group">
				<label><strong><code>(*) </code>Valor Adicional: </strong></label>
				<input type="text" class="form-control" name="valor_adicional" value="{{ $adicion->valor_adicional }}" />
			</div>

			<div class="form-group">
				<label><strong></code>Descripcion Adicional: </strong></label>
				<input type="text" class="form-control" name="descripcion_adicional" value="{{ $adicion->descripcion_adicional }}" />
			</div>

			<div class="form-group">
				<label><strong><code>(*) </code>Producto: </strong></label><br />
				<select class="form-control mi-selector" multiple="" name="productos[]">
					@foreach($productos as $producto)
						<?php if (is_array($codigo_productos_tienda) && in_array($producto->codigo_producto, $codigo_productos_tienda)) { ?>
							<?php if (is_array($codigo_productos_adicionales) && in_array($producto->codigo_producto, $codigo_productos_adicionales)) { ?>
								<option value="{{ $producto->codigo_producto }}" selected="">{{ $producto->name }}</option>
							<?php }else{ ?> 
								<option value="{{ $producto->codigo_producto }}">{{ $producto->name }}</option>
							<?php } ?>
						<?php } ?>
					@endforeach
				</select>
			</div>

			<center>
				<button type="submit" class="btn btn-info" id="registroTienda">Actualizar adición</button>
			</center>
			
			<br />
		{!! Form::close() !!}
	@endforeach
</div>

<script type="text/javascript">
	$(document).ready(function () {
        $('.mi-selector').select2();
    });
</script>