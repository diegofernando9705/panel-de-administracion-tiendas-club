<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleZonasTiendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('detalle_zonas_tiendas', function (Blueprint $table) {
            $table->increments('id_detalle_zona');
            $table->integer('id_zona_detalle');
            $table->integer('codigo_tienda_detalle');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_zonas_tiendas');
    }
}
