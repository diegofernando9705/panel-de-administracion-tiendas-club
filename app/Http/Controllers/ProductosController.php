<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Gate;
use DateTime;

class ProductosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        Gate::authorize('haveaccess','productos.show');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {

                $productos = DB::table('detalle_tienda_productos')
                ->select('productos.name', 'productos.description', 'productos.id_categoria', 'productos.valor', 'productos.estado_producto', 'detalle_tienda_productos.*', 'productos.id as codigo_producto', 'detalle_tienda_productos.*', 'tiendas.id_tienda', 'tiendas.nombre_tienda')
                    ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                    ->join('tiendas', 'detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                    ->paginate(10);

                return view('productos.administrator.index', compact('productos'));
                
            } else {
                  
                $array_codigo = [];

                $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();


                foreach ($tiendas as $tienda) {
                    $array_codigo[] = $tienda->id_tienda;
                }

                $productos = DB::table('detalle_tienda_productos')
                ->select('productos.name', 'productos.description', 'productos.id_categoria', 'productos.valor', 'productos.estado_producto', 'detalle_tienda_productos.*', 'productos.id as codigo_producto', 'detalle_tienda_productos.*', 'tiendas.id_tienda', 'tiendas.nombre_tienda')
                    ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                    ->join('tiendas', 'detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                    ->get();



              /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

                $detalles_del_plan = DB::table('detalle_planes_tiendas')
                  ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
                  ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
                  ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
                  ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
                  ->get();

                $dia_actual = new DateTime(date('Y-m-d'));
                $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

                $funcion = $dia_actual->diff($fecha_proximo_pago);

                if($fecha_proximo_pago < $dia_actual){
                  $dias_faltantes = 0;
                }else{
                  $dias_faltantes = $funcion->days;
                }
              /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

              if($dias_faltantes <= 0){
                $planes = DB::table('tabla_planes')->get();
                return view('planes.tienda.pago-plan', compact('planes'));
              }else{
                return view('productos.index', compact('productos', 'array_codigo'));
              }


          
            }

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        Gate::authorize('haveaccess','productos.create');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {
                
                $tiendas = DB::table('tiendas')->get();
                $categorias = DB::table('categorias_productos')->get();

                return view('productos.administrator.crear', compact('categorias', 'tiendas'));

            }else{


              $productos_tienda = [];
                  $detalle_adicionales_tienda = [];

                  $tiendas = DB::table('detalle_usuario_tiendas')
                              ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                              ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                              ->join('users', 'personas.user_register', '=', 'users.id')
                              ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                              ->where('users.id', Auth::user()->id)->get();
                              
              /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

            $detalles_del_plan = DB::table('detalle_planes_tiendas')
              ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
              ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
              ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
              ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
              ->get();

            $dia_actual = new DateTime(date('Y-m-d'));
            $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

            $funcion = $dia_actual->diff($fecha_proximo_pago);

            if($fecha_proximo_pago < $dia_actual){
              $dias_faltantes = 0;
            }else{
              $dias_faltantes = $funcion->days;
            }

          /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

          if($dias_faltantes <= 0){
            $planes = DB::table('tabla_planes')->get();
            return view('planes.tienda.pago-plan', compact('planes'));
          }else{
            /* VER ADICIONALES DE LAS TIENDAS AL CREAR PRODUCTO */

                

                  foreach ($tiendas as $tienda) {
                    $array_codigo = $tienda->id_tienda;
                  }


                  $adicionales_tienda = DB::table('detalle_adicionales_tienda')->where('codigo_tienda', $array_codigo)->get();

                  foreach ($adicionales_tienda as $adicion_tienda) {
                    $detalle_adicionales_tienda[] = $adicion_tienda->codigo_adicional;
                  }


                  $adicionales =  DB::table('adicionales_productos')->get();

                /* FIN VER ADICIONALES DE LAS TIENDAS AL CREAR PRODUCTOS */





                $array_categoria = [];
                $codigo_temporal = [];

                $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();

                foreach ($tiendas as $tienda) {
                    $codigo_tienda = $tienda->id_tienda;
                }


                $productos_tienda = DB::table('detalle_tienda_productos')
                        ->select('productos.*', 'detalle_tienda_productos.*')
                        ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->where('id_tienda', $codigo_tienda)
                        ->get();

                $categorias = DB::table('categorias_productos')->get();

                        
                foreach ($productos_tienda as $producto) {
                    $array_categoria[] = $producto->id_categoria;
                }

                $categorias_temporales = DB::table('categorias_temporales')->where('id_tienda', $codigo_tienda)->get();
                        
                foreach ($categorias_temporales as $categoria_temporal) {
                    $codigo_temporal[] = $categoria_temporal->id_categoria;
                }

                $categorias_productos = DB::table('categorias_productos')->get();

                /* VALIDAMOS CUANDO SEA PLAN FREE PARA QUE NO AGREGUE MAS DE 10 PRODUCTOS */

                $total_productos = DB::table('detalle_tienda_productos')
                        ->select('productos.*', 'detalle_tienda_productos.*')
                        ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->where('id_tienda', $codigo_tienda)
                        ->count();

                $captura_rol = DB::table('role_user')->where('user_id', Auth::user()->id)->get();

                if($captura_rol[0]->role_id == '755'){
                  if($total_productos <= 10){
                    return view('productos.creacion', compact('array_categoria', 'categorias', 'codigo_temporal', 'adicionales', 'detalle_adicionales_tienda'));  
                  }else{
                     return view('productos.mensaje_actualizar_plan');
                  }
                }else{
                  return view('productos.creacion', compact('array_categoria', 'categorias', 'codigo_temporal', 'adicionales', 'detalle_adicionales_tienda'));
                }

                

                /* FIN VALIDACIONES CUANDO SEA PLAN FREE PARA QUE NO AGREGUE MAS DE 10 PRODUCTOS */

                
          }


                
            }
        }

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        Gate::authorize('haveaccess','productos.create');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {

                $request->validate([
                    'imagen_producto' => 'required|mimes:jpg,jpeg,bmp,png',
                    'nombre_producto' => 'required',
                    'codigo_tienda' => 'required',
                    'valor_producto' => 'required',
                    'categoria_producto' => 'required',
                    'estado_producto' => 'required',
                ]);

            }else{

                $request->validate([
                    'imagen_producto' => 'required|mimes:jpg,jpeg,bmp,png',
                    'nombre_producto' => 'required',
                    'valor_producto' => 'required',
                    'categoria_producto' => 'required',
                    'estado_producto' => 'required',
                ]);

            }
        }

        


        //obtenemos el campo file definido en el formulario
        $file = $request->file('imagen_producto');
        //obtenemos el nombre del archivo
        $imagen_producto = $file->getClientOriginalName();
        //obtenemos el nombre del archivo
        $imagen_producto = $file->getClientOriginalName('imagen_producto');
        \Storage::disk('public')->put($imagen_producto, \File::get($file));

        DB::table('productos')->insert([
            'imagen_producto' => $imagen_producto,
            'name' => $request->nombre_producto,
            'description' => $request->descripcion_producto,
            'id_categoria' => $request->categoria_producto,
            'valor' => $request->valor_producto,
            'estado_producto' => $request->estado_producto
        ]);

        

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {
                $codigo_tienda = $request->codigo_tienda;
            }else{

                /* detectar codigo de la tienda segun usuario */
                 $tiendas = DB::table('detalle_usuario_tiendas')
                                        ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                        ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                        ->join('users', 'personas.user_register', '=', 'users.id')
                                        ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                        ->where('users.id', Auth::user()->id)->get();

                foreach ($tiendas as $tienda) {
                    $codigo_tienda = $tienda->id_tienda;
                }

            }
        }





        /* detectar ultimo producto registrado */
        $producto = DB::table('productos')->orderby('id','DESC')->take(1)->get();
        foreach($producto as $produ){
            $id_producto = $produ->id;
        }

        DB::table('detalle_tienda_productos')->insert([
            'id_producto' => $id_producto,
            'id_tienda' => $codigo_tienda
        ]);

        DB::table('categorias_productos')->where('id', $request->categoria_producto)->update(['estado_categoria' => '1']);
        DB::table('categorias_temporales')->where('id_categoria', $request->categoria_producto)->delete();


         /* Agregamos adicionales de los productos */
        if(empty($request->adicionales_productos[0])){
            
        }else{
            
            foreach ($request->adicionales_productos as $adicional) {
                 DB::table('detalle_adicionales_productos')->insert([
                    'id_adicional_detalle' => $adicional,
                    'id_producto_detalle' => $id_producto
                  ]);   
            }

        }

        /* fin agregado adicionales de los productos */

        return redirect()->route('productos.index')
            ->with('status_success','Creación exitosa!'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        Gate::authorize('haveaccess','productos.edit');

         $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {
                
                $productos = DB::table('productos')->where('id', $id)->get();
                $categorias = DB::table('categorias_productos')->get();

                return view('productos.administrator.ver', compact('categorias', 'productos'));
            }else{

                $productos = DB::table('productos')->where('id', $id)->get();

                $array_categoria = [];
                $codigo_temporal = [];

                $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();

                foreach ($tiendas as $tienda) {
                    $codigo_tienda = $tienda->id_tienda;
                }


                $productos_tienda = DB::table('detalle_tienda_productos')
                        ->select('productos.*', 'detalle_tienda_productos.*')
                        ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->where('id_tienda', $codigo_tienda)
                        ->get();

                $categorias = DB::table('categorias_productos')->get();
                        
                foreach ($productos_tienda as $producto) {
                    $array_categoria[] = $producto->id_categoria;
                }

                $categorias_temporales = DB::table('categorias_temporales')->where('id_tienda', $codigo_tienda)->get();
                        
                foreach ($categorias_temporales as $categoria_temporal) {
                    $codigo_temporal[] = $categoria_temporal->id_categoria;
                }

                $categorias_productos = DB::table('categorias_productos')->get();

                /* SABER ADICIONALES DE LAS TIENDAS*/
                $productos_tienda = [];
                  $detalle_adicionales_tienda = [];

                  $tiendas = DB::table('detalle_usuario_tiendas')
                              ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                              ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                              ->join('users', 'personas.user_register', '=', 'users.id')
                              ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                              ->where('users.id', Auth::user()->id)->get();

                  foreach ($tiendas as $tienda) {
                    $array_codigo = $tienda->id_tienda;
                  }


                  $adicionales_tienda = DB::table('detalle_adicionales_tienda')->where('codigo_tienda', $array_codigo)->get();

                  foreach ($adicionales_tienda as $adicion_tienda) {
                    $detalle_adicionales_tienda[] = $adicion_tienda->codigo_adicional;
                  }

                  /* TODAS LAS ADICIONALES DE LA BD */
                  $adicionales =  DB::table('adicionales_productos')->get();


                /*SABER ADICIONALES ASIGNADAS AL PRODUCTO*/
                $adicionales_asignadas = DB::table('detalle_adicionales_productos')->where('id_producto_detalle', $id);



                return view('productos.ver', compact('array_categoria', 'categorias', 'codigo_temporal', 'productos', 'adicionales', 'adicionales_tienda'));

            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
        Gate::authorize('haveaccess','productos.edit');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {
                
                $productos = DB::table('productos')->where('id', $id)->get();
                $categorias = DB::table('categorias_productos')->get();

                return view('productos.administrator.edicion', compact('categorias', 'productos'));
            }else{

                $productos = DB::table('productos')->where('id', $id)->get();

                $array_categoria = [];
                $codigo_temporal = [];

                $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();

                foreach ($tiendas as $tienda) {
                    $codigo_tienda = $tienda->id_tienda;
                }


                $productos_tienda = DB::table('detalle_tienda_productos')
                        ->select('productos.*', 'detalle_tienda_productos.*')
                        ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->where('id_tienda', $codigo_tienda)
                        ->get();

                $categorias = DB::table('categorias_productos')->get();
                        
                foreach ($productos_tienda as $producto) {
                    $array_categoria[] = $producto->id_categoria;
                }

                $categorias_temporales = DB::table('categorias_temporales')->where('id_tienda', $codigo_tienda)->get();
                        
                foreach ($categorias_temporales as $categoria_temporal) {
                    $codigo_temporal[] = $categoria_temporal->id_categoria;
                }

                $categorias_productos = DB::table('categorias_productos')->get();

                /* SABER ADICIONALES DE LAS TIENDAS*/
                $productos_tienda = [];
                  $detalle_adicionales_tienda = [];

                  $tiendas = DB::table('detalle_usuario_tiendas')
                              ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                              ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                              ->join('users', 'personas.user_register', '=', 'users.id')
                              ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                              ->where('users.id', Auth::user()->id)->get();

                  foreach ($tiendas as $tienda) {
                    $array_codigo = $tienda->id_tienda;
                  }


                  $adicionales_tienda = DB::table('detalle_adicionales_tienda')->where('codigo_tienda', $array_codigo)->get();

                  foreach ($adicionales_tienda as $adicion_tienda) {
                    $detalle_adicionales_tienda[] = $adicion_tienda->codigo_adicional;
                  }

                  /* TODAS LAS ADICIONALES DE LA BD */
                  $adicionales =  DB::table('adicionales_productos')->get();

                $codigo_adicionales_asignadas = [];
                /*SABER ADICIONALES ASIGNADAS AL PRODUCTO*/
                $adicionales_asignadas = DB::table('detalle_adicionales_productos')->where('id_producto_detalle', $id)->get();

                foreach ($adicionales_asignadas as $codigo) {
                    $codigo_adicionales_asignadas[] = $codigo->id_adicional_detalle;
                }


                return view('productos.edicion', compact('array_categoria', 'categorias', 'codigo_temporal', 'productos', 'adicionales', 'detalle_adicionales_tienda', 'codigo_adicionales_asignadas'));

            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
        Gate::authorize('haveaccess','productos.edit');

        $request->validate([
            'foto_producto' => 'mimes:jpg,jpeg,bmp,png',
            'nombre_producto' => 'required',
            'valor_producto' => 'required',
            'categoria_producto' => 'required',
            'estado_producto' => 'required',
        ]);

        if(empty($request->foto_producto)){
            $imagen_producto = $request->imagen_bd;
        }else{
            //obtenemos el nombre del archivo
             //obtenemos el campo file definido en el formulario
            $file = $request->file('foto_producto');
            $imagen_producto = $file->getClientOriginalName('foto_producto');
            \Storage::disk('public')->put($imagen_producto, \File::get($file));
        }

        DB::table('productos')->where('id', $id)->update([
            'imagen_producto' => $imagen_producto,
            'name' => $request->nombre_producto,
            'description' => $request->descripcion_producto,
            'id_categoria' => $request->categoria_producto,
            'valor' => $request->valor_producto,
            'estado_producto' => $request->estado_producto,
        ]);


        /* se eliminan los adicionales asignados al producto, para actualizar y asignar unos nuevos*/
        DB::table('detalle_adicionales_productos')->where('id_producto_detalle', $id)->delete();


         /* Agregamos adicionales de los productos */
        if(empty($request->adicionales_productos[0])){
            
        }else{
            
            foreach ($request->adicionales_productos as $adicional) {
                 DB::table('detalle_adicionales_productos')->insert([
                    'id_adicional_detalle' => $adicional,
                    'id_producto_detalle' => $id
                  ]);   
            }

        }

        /* fin agregado adicionales de los productos */
        return redirect()->route('productos.index')
            ->with('status_success','Actualizacion exitosa!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Gate::authorize('haveaccess','productos.destroy');

        DB::table('productos')->where('id', $id)->update([
            'estado_producto' => '0',
        ]);

        return redirect()->route('productos.index')
            ->with('status_success','Actualizacion exitosa!'); 
    }

    public function keyupproducto($producto){

        Gate::authorize('haveaccess','productos.show');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {

                $productos = DB::table('productos')->where("name", 'like', $producto . "%")->get();
                $categorias = DB::table('categorias_productos')->get();

                return view('productos.administrator.keyup_producto', compact('categorias', 'productos'));
            }else{

                $array_codigo = [];

                $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();


                foreach ($tiendas as $tienda) {
                    $array_codigo[] = $tienda->id_tienda;
                }   

                $productos = DB::table('detalle_tienda_productos')
                ->select('productos.name', 'productos.description', 'productos.id_categoria', 'productos.valor', 'productos.estado_producto', 'detalle_tienda_productos.*', 'productos.id as codigo_producto', 'detalle_tienda_productos.*', 'tiendas.id_tienda', 'tiendas.nombre_tienda')
                    ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                    ->join('tiendas', 'detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                    ->where("name", 'like', $producto . "%")
                    ->where("tiendas.id_tienda", $array_codigo)
                    ->get();
                $categorias = DB::table('categorias_productos')->get();

                return view('productos.administrator.keyup_producto', compact('categorias', 'productos'));
            }
        }

    }

}
