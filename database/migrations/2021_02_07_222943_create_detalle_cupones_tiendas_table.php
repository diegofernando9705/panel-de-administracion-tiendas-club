<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleCuponesTiendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_cupones_tiendas', function (Blueprint $table) {
            $table->increments('id_detalle_cupon');
            $table->char('cod_detalle_cupon', 255);
            $table->integer('cod_detalle_tienda_cupon');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_cupones_tiendas');
    }
}
