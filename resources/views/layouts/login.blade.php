<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Panel de administración - Tiendas Club</title>
        <!--<link rel="stylesheet" href="{{ asset('bootstrap4/css/bootstrap.css') }}"> --
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        -->
        <!-- Template CSS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
        <link href="{{  asset('style-template/css/style.css') }}" rel="stylesheet" />
        <link href="{{  asset('style-template/css/style-personalizados.css') }}" rel="stylesheet" />
        <link href="{{ asset('style-template/css/font-awesome.css') }}" rel="stylesheet">
        
        <!-- google fonts -->
        <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet" />
    </head>

    <body>
        
    <section>
        
        <main class="">
            @yield('content')
        </main>

    </section>
    
    @yield('scripts')

    <button class="bg-primary" id="movetop" onclick="topFunction()" title="Go to top">
        <span class="fa fa-angle-up"></span>
    </button>

    <script>

          window.onscroll = function () {
            scrollFunction()
          };

          function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
              document.getElementById("movetop").style.display = "block";
            } else {
              document.getElementById("movetop").style.display = "none";
            }
          }


          function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
          }
    </script>

    
    <script src="{{ asset('style-template/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('style-template/js/jquery-1.10.2.min.js') }}"></script>
    <script src="{{ asset('style-template/js/Chart.min.js') }}"></script>
    <script src="{{ asset('style-template/js/utils.js') }}"></script>
    <script src="{{ asset('style-template/js/bar.js') }}"></script>
    <script src="{{ asset('style-template/js/linechart.js') }}"></script>
    <script src="{{ asset('style-template/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset('style-template/js/scripts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ asset('style-template/js/actions.js') }}"></script>
    <script src="{{ asset('style-template/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('bootstrap4/js/acciones.js') }}"></script>


    <script>
        var closebtns = document.getElementsByClassName("close-grid");
        var i;

        for (i = 0; i < closebtns.length; i++) {
            closebtns[i].addEventListener("click", function () {
              this.parentElement.style.display = 'none';
            });
          }
    </script>

    <script>
        $(function () {
            $('.sidebar-menu-collapsed').click(function () {
                $('body').toggleClass('noscroll');
            })
        });
    </script>

    <script src="{{ asset('style-template/js/modernizr.js') }}"></script>

    <script>
        $(window).load(function () {
            $(".se-pre-con").fadeOut("slow");;
        });
    </script>

    <script type="text/javascript">
        
        $(document).on("click", "#btn-register-new", function(){
                
            var formData = $("#formulario-registro").serialize();

            $.ajax({
                
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: "POST",
                url: "/registro_usuario",
                data: formData,
                success: function (data) {
                    
                    $("span.errores").attr("style","display:none !important;");

                    if(data === '3'){
                        $("#informacion-respuesta").html("<div class='alert alert-danger' role='alert'>El correo electrónico ya se encuentra registrados.</div>");
                    }else if(data === '4'){
                        $("#informacion-respuesta").html("<div class='alert alert-danger' role='alert'>Las contraseñas no coinciden.</div>");
                    }else{
                        $("#contenido-registro").html(data);
                    }
                },
                error: function (err) {
                    if (err.status == 422) { // when status code is 422, it's a validation issue
                        console.log(err.responseJSON);
                        $('#success_message').fadeIn().html(err.responseJSON.message);
                        
                        // you can loop through the errors object and show it to the user
                        console.warn(err.responseJSON.errors);
                        // display errors on each form field
                        $.each(err.responseJSON.errors, function (i, error) {
                            var el = $(document).find('[name="'+i+'"]');
                            el.after($('<span class="errores" style="color: red;">'+error[0]+'</span>'));
                        });
                    }
                }
            });

        });
        
    $(document).ready(function () {
        $('.mi-selector').select2();
    });

    /* Busqueda ciudad segun departamento */
    $(document).on("change", "#departamento", function(){
        var valor = $(this).val();
        
        $.ajax({
            type: 'get',
            url: '/busqueda/ciudad/search/' + valor,
            success: function (data) {
                document.getElementById("ciudad").innerHTML = data;
            }
        });
    });


        $(document).on("click", "#btn-registro-persona", function(){
            
            var formulario = $("#formulario-personas").serialize();

            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: "POST",
                url: "/registro_persona",
                data: formulario,
                success: function (data) {
                    if(data === 2){
                        alert('Numero de celular invalido');
                    }else{
                        $("#contenido-registro").html(data);
                    }
                },
                error: function (err) {
                    if (err.status == 422) { // when status code is 422, it's a validation issue
                        console.log(err.responseJSON);
                        $('#success_message').fadeIn().html(err.responseJSON.message);
                        
                        // you can loop through the errors object and show it to the user
                        console.warn(err.responseJSON.errors);
                        // display errors on each form field
                        $.each(err.responseJSON.errors, function (i, error) {
                            var el = $(document).find('[name="'+i+'"]');
                            el.after($('<span style="color: red;">'+error[0]+'</span>'));
                        });
                    }
                }
            });

        });
            

        $(document).on("click", "#btn-registro-tienda", function(){
            
            var formulario = $("#formulario-registro-tienda").serialize();

            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: "POST",
                url: "/registro_tienda",
                data: formulario,
                success: function (data) {
                    console.log(data);
                    if(data == 4){
                        $("#informacion-respuesta").html("<div class='alert alert-danger' role='alert'>El nombre de la tienda ya existe.</div>");
                    }else if(data == 2){

                    }else{
                        alert('Registro exitoso');
                        setTimeout(function(){
                                location.href = "/";  
                        },1000);
                    }
                },
                error: function (err) {
                    if (err.status == 422) { // when status code is 422, it's a validation issue
                        console.log(err.responseJSON);
                        $('#success_message').fadeIn().html(err.responseJSON.message);
                        
                        // you can loop through the errors object and show it to the user
                        console.warn(err.responseJSON.errors);
                        // display errors on each form field
                        $.each(err.responseJSON.errors, function (i, error) {
                            var el = $(document).find('[name="'+i+'"]');
                            el.after($('<span style="color: red;">'+error[0]+'</span>'));
                        });
                    }
                }
            });

        });


    </script>
    </body>
</html>
