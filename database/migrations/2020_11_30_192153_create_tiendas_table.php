<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiendas', function (Blueprint $table) {
             $table->integer('id_tienda');
            $table->longText('image_tienda')->required();
            $table->longText('nombre_tienda')->required();
            $table->longText('url_tienda')->nullable();
            $table->longText('descripcion_tienda')->required();
            $table->string('direccion_tienda', 100)->required();
            $table->longText('horario_tienda')->nullable();
            $table->integer('domicilio')->nullable();
            $table->integer('id_cate_tienda')->required();
            $table->integer('id_ciudad')->required();
            $table->longText('telefono_movil')->required();
            $table->integer('telefono_fijo')->nullable();
            $table->longText('instagram_link')->nullable();
            $table->longText('facebook_link')->nullable();
            $table->longText('twitter_link')->nullable();
            $table->integer('estado_tienda')->nullable();
            $table->primary('id_tienda');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiendas');
    }
}
