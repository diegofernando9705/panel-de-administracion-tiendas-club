<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use QrCode;
use Auth;
use Gate;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class TiendasController extends Controller {

    public function busquedaCiudad($param) {

        if ($param != "") {
            $ciudades = DB::table('ciudades')->where('id_departamento', $param)->get();
        } else {
            $ciudades = DB::table('ciudades')->get();
        }

        return view("tiendas.ciudad", compact("ciudades"));
    }

    public function create(Request $request) {

        Gate::authorize('haveaccess', 'tiendas.create');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {

            }else{

                DB::table('personas')->where('user_register', Auth::user()->id)->update([
                    'estado_persona' => '1'
                ]);
                
                $validacion = DB::table('detalle_usuario_tiendas')
                    ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                    ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                    ->where('personas.correo_electronico', $user->email)
                    ->get();

                return redirect()->route('tiendas.listado')->with('status_success','Actualizacion exitosa!'); 

            }
        }

        $departamentos = DB::table('departamentos')->get();
        $categorias_tienda = DB::table('categorias_tiendas')->get();
        $personas = DB::table('personas')->where('estado_persona', 1)->get();

        
        return view('tiendas.crear', compact('departamentos', 'categorias_tienda', 'personas'));
    }

    public function store(Request $request) {

        Gate::authorize('haveaccess','tiendas.create');

        $request->validate([
            'nombreTiendaUnico' => 'required',
            'imgTienda' => 'required|mimes:jpg,jpeg,bmp,png',
            'background_tienda' => 'required|mimes:jpg,jpeg,bmp,png',
            'nombreTienda' => 'required',
            'descripcionTienda' => 'required',
            'direccionTienda' => 'required',
            'horarioTienda' => 'required',
            'domicilio' => 'required',
            'departamentoTienda' => 'required',
            'personas' => 'required',
            'ciudadTienda' => 'required',
            'nombre_zona' => 'required',
            'celular_zona' => 'required'
        ]);
        

        /*
            $horario = [];

            if(isset($request->dia_lunes)){
                $request->validate([
                    'hora_de_inicio_lunes' => 'required',
                    'hora_de_fin_lunes' => 'required',
                ]);
                $horario[1] = "Lunes-".$request->hora_de_inicio_lunes."_".$request->hora_de_fin_lunes;

            }

            if(isset($request->dia_martes)){
                $request->validate([
                    'hora_de_inicio_martes' => 'required',
                    'hora_de_fin_martes' => 'required',
                ]);
                $horario[2] = "Martes-".$request->hora_de_inicio_martes."_".$request->hora_de_fin_martes;
            }

            if(isset($request->dia_miercoles)){
                $request->validate([
                    'hora_de_inicio_miercoles' => 'required',
                    'hora_de_fin_miercoles' => 'required',
                ]);
                $horario[3] = "Miercoles-".$request->hora_de_inicio_miercoles."_".$request->hora_de_fin_miercoles;
            }

            if(isset($request->dia_jueves)){
                $request->validate([
                    'hora_de_inicio_jueves' => 'required',
                    'hora_de_fin_jueves' => 'required',
                ]);
                $horario[4] = "Jueves-".$request->hora_de_inicio_jueves."_".$request->hora_de_fin_jueves;
            }

            if(isset($request->dia_viernes)){
                $request->validate([
                    'hora_de_inicio_viernes' => 'required',
                    'hora_de_fin_viernes' => 'required',
                ]);
                $horario[5] = "Viernes-".$request->hora_de_inicio_viernes."_".$request->hora_de_fin_viernes;
            }

            if(isset($request->dia_sabado)){
                $request->validate([
                    'hora_de_inicio_sabado' => 'required',
                    'hora_de_fin_sabado' => 'required',
                ]);
                $horario[6] = "Sabado-".$request->hora_de_inicio_sabado."_".$request->hora_de_fin_sabado;
            }

            if(isset($request->dia_domingo)){
                $request->validate([
                    'hora_de_inicio_domingo' => 'required',
                    'hora_de_fin_domingo' => 'required',
                ]);
                $horario[7] = "Domingo-".$request->hora_de_inicio_domingo."_".$request->hora_de_fin_domingo;
            }
        */

        $codigo_tienda = rand(1, 50000);

        //obtenemos el campo file definido en el formulario (LOGO TIENDA)
        $file = $request->file('imgTienda');
        $nombre = $file->getClientOriginalName('imgTienda');



        //obtenemos el campo file definido en el formulario (LOGO TIENDA)
        $file = $request->file('background_tienda');
        $background_tienda = $file->getClientOriginalName('background_tienda');

        DB::table('tiendas')->insert([
            'id_tienda' => $codigo_tienda,
            'image_tienda' => $nombre,
            'background_tienda' => $background_tienda,
            'nombre_tienda' => $request->nombreTienda,
            'url_tienda' => $request->nombreTiendaUnico,
            'descripcion_tienda' => $request->descripcionTienda,
            'direccion_tienda' => $request->direccionTienda,
            'horario_tienda' => $request->horario,
            'domicilio' => $request->domicilio,
            'id_cate_tienda' => $request->categoria_tienda,
            'id_ciudad' => $request->ciudadTienda,
            'telefono_movil' => $request->celular_zona,
            'telefono_fijo' => $request->telFijo,
            'instagram_link' => $request->linkIns,
            'facebook_link' => $request->linkFb,
            'twitter_link' => $request->linkTwitter,
            'estado_tienda' => '1',
        ]);

        /* INSERTAR ZONA PREDETERMINADA */

        DB::table('zonas_tiendas')->insert([
                'nombre_zona' => $request->nombre_zona, 
                'descripcion_zona' => $request->descripcion_zona, 
                'celular_zona' => $request->celular_zona, 
                'estado_zona' => '1' ]);

        $zona = DB::table('zonas_tiendas')->orderBy('id_zona', 'desc')->take(1)->get();

        DB::table('detalle_zonas_tiendas')->insert([
                'id_zona_detalle' => $zona[0]->id_zona,
                'codigo_tienda_detalle' => $codigo_tienda
            ]);


        /* FIN INSERTAR ZONA PREDETERMINADA */
        

        //obtenemos el nombre del archivo LOGO TIENDA
        $nombre = $file->getClientOriginalName('imgTienda');
        \Storage::disk('public')->put($nombre, \File::get($file));


        //obtenemos el nombre del archivo BACKGROUND
        $background_tienda = $file->getClientOriginalName('background_tienda');
        \Storage::disk('public')->put($background_tienda, \File::get($file));


        DB::table('detalle_usuario_tiendas')->insert([
            'id_persona' => $request->personas,
            'id_tienda' => $codigo_tienda
        ]);

        return redirect()->route('tiendas.listado')
            ->with('status_success','Creación exitosa!'); 
    }

    public function tiendas() {
        
        Gate::authorize('haveaccess','tiendas.show');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {
                $tiendas = DB::table('tiendas')->get();
            } else {
                $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();
            }
        }

        return view('tiendas.listado', compact('tiendas'));
    }

    public function edit($id) {

        Gate::authorize('haveaccess','tiendas.edit');


     $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

      foreach ($usuario as $user) {
          $rol = $user->slug;
      }


        $tiendas = DB::table('tiendas')->where('url_tienda', $id)->get();

        /*
        $dias = [];
        $dia = ['s'];


        /*
        $horario_tienda_general = explode(",", $horario);

        foreach ($horario_tienda_general as $key => $valor) {
            $dias[] = explode("-", $valor);
        }

        foreach ($dias as $dias_semana) {
            foreach ($dias_semana as $dia_semana) {
                $dia[] = $dia_semana;
            }
        }

    
        /* LUNES *
        $lunes = array_search('Lunes',$dia);
        if ($lunes) {
            $lunes_active = true;
            $hora = $lunes+1;
            $horas = explode("_", $dia[$hora]);
            $hora_apertura_lunes = $horas[0];
            $hora_cierre_lunes = $horas[1];
        }else {
            $lunes_active = false;
            $hora_apertura_lunes = "";
            $hora_cierre_lunes = "";
        }


        /* MARTES *
        $martes = array_search('Martes',$dia);
        if ($martes) {
            $martes_active = true;
            $hora = $martes+1;
            $horas = explode("_", $dia[$hora]);
            $hora_apertura_martes = $horas[0];
            $hora_cierre_martes = $horas[1];
        }else{
            $martes_active = false;
            $hora_apertura_martes = "";
            $hora_cierre_martes = "";
        }


        /* MIERCOLES *
        $miercoles = array_search('Miercoles',$dia);
        if ($miercoles) {
            $miercoles_active = true;
            $hora = $miercoles+1;
            $horas = explode("_", $dia[$hora]);
            $hora_apertura_miercoles = $horas[0];
            $hora_cierre_miercoles = $horas[1];
        }else{
            $miercoles_active = false;
            $hora_apertura_miercoles = "";
            $hora_cierre_miercoles = "";
        }


        /* JUEVES *
        $jueves = array_search('Jueves',$dia);
        if ($jueves) {
            $jueves_active = true;
            $hora = $jueves+1;
            $horas = explode("_", $dia[$hora]);
            $hora_apertura_jueves = $horas[0];
            $hora_cierre_jueves = $horas[1];
        }else{
            $jueves_active = false;
            $hora_apertura_jueves = "";
            $hora_cierre_jueves = "";
        }


        /* VIERNES *
        $viernes = array_search('Viernes',$dia);
        if ($viernes) {
            $viernes_active = true;
            $hora = $viernes+1;
            $horas = explode("_", $dia[$hora]);
            $hora_apertura_viernes = $horas[0];
            $hora_cierre_viernes = $horas[1];
        }else{
            $viernes_active = false;
            $hora_apertura_viernes = "";
            $hora_cierre_viernes = "";
        }


        /* SABADO *
        $sabado = array_search('Sabado',$dia);
        if ($sabado) {
            $sabado_active = true;
            $hora = $sabado+1;
            $horas = explode("_", $dia[$hora]);
            $hora_apertura_sabado = $horas[0];
            $hora_cierre_sabado = $horas[1];
        }else{
            $sabado_active = false;
            $hora_apertura_sabado = "";
            $hora_cierre_sabado = "";
        }


        /* DOMINGO *
        $domingo = array_search('Domingo',$dia);
        if ($domingo) {
            $domingo_active = true;
            $hora = $domingo+1;
            $horas = explode("_", $dia[$hora]);
            $hora_apertura_domingo = $horas[0];
            $hora_cierre_domingo = $horas[1];
        }else{
            $domingo_active = false;
            $hora_apertura_domingo = "";
            $hora_cierre_domingo = "";
        }
        */

        $categorias_tienda = DB::table('categorias_tiendas')->get();
        $departamentos = DB::table('departamentos')->get();
        $ciudades = DB::table('ciudades')->get();
        $zonas = DB::table('detalle_zonas_tiendas')
                ->select('zonas_tiendas.*', 'tiendas.*', 'detalle_zonas_tiendas.*')
                ->join('zonas_tiendas', 'detalle_zonas_tiendas.id_zona_detalle', '=', 'zonas_tiendas.id_zona')
                ->join('tiendas', 'detalle_zonas_tiendas.codigo_tienda_detalle', '=', 'tiendas.id_tienda')
                ->where('tiendas.id_tienda', '=', $tiendas[0]->id_tienda)->get();

        $zona_count =  DB::table('detalle_zonas_tiendas')
                ->select('zonas_tiendas.*', 'tiendas.*', 'detalle_zonas_tiendas.*')
                ->join('zonas_tiendas', 'detalle_zonas_tiendas.id_zona_detalle', '=', 'zonas_tiendas.id_zona')
                ->join('tiendas', 'detalle_zonas_tiendas.codigo_tienda_detalle', '=', 'tiendas.id_tienda')
                ->where('tiendas.id_tienda', '=', $tiendas[0]->id_tienda)->count();


        return view('tiendas.edicion', compact('tiendas', 'departamentos', 'categorias_tienda', 'ciudades', 'rol', 'zonas', 'zona_count'));
    }

    public function update(Request $request, $id){

        Gate::authorize('haveaccess','tiendas.edit');

        $request->validate([
            'foto_tienda' => 'mimes:jpg,jpeg,bmp,png',
            'nombreTienda' => 'required',
            'descripcionTienda' => 'required',
            'direccionTienda' => 'required',
            'horarioTienda' => 'required',
            'domicilio' => 'required',
            'categoria_tienda' => 'required',
            'departamentoTienda' => 'required',
            'ciudadTienda' => 'required',
            'telMovil' => 'required',
        ]);

        /*
            $horario = [];

            if(isset($request->dia_lunes)){
                    $request->validate([
                        'hora_de_inicio_lunes' => 'required',
                        'hora_de_fin_lunes' => 'required',
                    ]);
                    $horario[1] = "Lunes-".$request->hora_de_inicio_lunes."_".$request->hora_de_fin_lunes."";

                }

                if(isset($request->dia_martes)){
                    $request->validate([
                        'hora_de_inicio_martes' => 'required',
                        'hora_de_fin_martes' => 'required',
                    ]);
                    $horario[2] = "Martes-".$request->hora_de_inicio_martes."_".$request->hora_de_fin_martes."";
                }

                if(isset($request->dia_miercoles)){
                    $request->validate([
                        'hora_de_inicio_miercoles' => 'required',
                        'hora_de_fin_miercoles' => 'required',
                    ]);
                    $horario[3] = "Miercoles-".$request->hora_de_inicio_miercoles."_".$request->hora_de_fin_miercoles."";
                }

                if(isset($request->dia_jueves)){
                    $request->validate([
                        'hora_de_inicio_jueves' => 'required',
                        'hora_de_fin_jueves' => 'required',
                    ]);
                    $horario[4] = "Jueves-".$request->hora_de_inicio_jueves."_".$request->hora_de_fin_jueves."";
                }

                if(isset($request->dia_viernes)){
                    $request->validate([
                        'hora_de_inicio_viernes' => 'required',
                        'hora_de_fin_viernes' => 'required',
                    ]);
                    $horario[5] = "Viernes-".$request->hora_de_inicio_viernes."_".$request->hora_de_fin_viernes."";
                }

                if(isset($request->dia_sabado)){
                    $request->validate([
                        'hora_de_inicio_sabado' => 'required',
                        'hora_de_fin_sabado' => 'required',
                    ]);
                    $horario[6] = "Sabado-".$request->hora_de_inicio_sabado."_".$request->hora_de_fin_sabado."";
                }

                if(isset($request->dia_domingo)){
                    $request->validate([
                        'hora_de_inicio_domingo' => 'required',
                        'hora_de_fin_domingo' => 'required',
                    ]);
                    $horario[7] = "Domingo-".$request->hora_de_inicio_domingo."_".$request->hora_de_fin_domingo."";
                }
        
            //return $request->all();
        */

        

        /* VALIDACION LOGO TIENDA */

        if(empty($request->foto_tienda)){
            $imagen_tienda = $request->imagen_bd;
        }else{
            //obtenemos el nombre del archivo
             //obtenemos el campo file definido en el formulario
            $file = $request->file('foto_tienda');
            $imagen_tienda = $file->getClientOriginalName('foto_tienda');
            \Storage::disk('public')->put($imagen_tienda, \File::get($file));
        }



         /* VALIDACION BACKGROUND TIENDA */

        if(empty($request->background_tienda)){
            $background_tienda = $request->background_tienda;
        }else{
            //obtenemos el nombre del archivo
             //obtenemos el campo file definido en el formulario
            $file = $request->file('background_tienda');
            $background_tienda = $file->getClientOriginalName('background_tienda');
            \Storage::disk('public')->put($background_tienda, \File::get($file));
        }




        if(empty($request->llave_publica)){
            $public_key = "0";
        }else{
            $public_key = $request->llave_publica;
        }

        DB::table('tiendas')->where('url_tienda', $id)->update([
            'image_tienda' => $imagen_tienda,
            'background_tienda' => $background_tienda,
            'nombre_tienda' => $request->nombreTienda,
            'url_tienda' => $request->nameUnicoTienda,
            'llave_publica' => $public_key,
            'descripcion_tienda' => $request->descripcionTienda,
            'direccion_tienda' => $request->direccionTienda,
            'horario_tienda' => $request->horarioTienda,
            'domicilio' => $request->domicilio,
            'id_cate_tienda' => $request->categoria_tienda,
            'id_ciudad' => $request->ciudadTienda,
            'telefono_movil' => $request->telMovil,
            'telefono_fijo' => $request->telFijo,
            'instagram_link' => $request->linkIns,
            'facebook_link' => $request->linkFb,
            'twitter_link' => $request->linkTwitter,
            'estado_tienda' => $request->estado_tienda,
        ]);

        if(!empty($request->id_persona)){
            DB::table('personas')->where('id', $request->id_persona)->update([
                'estado_persona' => '1'
            ]);
        }

        

        return redirect()->route('tiendas.listado')
            ->with('status_success','Actualizacion exitosa!'); 
    }

    public function show($id){
        
        Gate::authorize('haveaccess','tiendas.show');

        $tiendas = DB::table('tiendas')->where('url_tienda', $id)->get();
        $categorias_tienda = DB::table('categorias_tiendas')->get();
        $departamentos = DB::table('departamentos')->get();
        $ciudades = DB::table('ciudades')->get();



        return view('tiendas.ver', compact('tiendas', 'departamentos', 'categorias_tienda', 'ciudades'));
    }


    public function keyuptienda($variable){
        
        Gate::authorize('haveaccess','tiendas.show');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {
                
                $tiendas = DB::table('tiendas')->where("nombre_tienda", 'like', $variable . "%")->get();

                return view('tiendas.keyup_tienda', compact('tiendas'));

            } else {
                return "<div class='alert alert-danger'>Esta funcion no es permitida.</div>";
            }
        }

    }

    public function destroy($variable){
        
        Gate::authorize('haveaccess','tiendas.destroy');

        DB::table('tiendas')->where('url_tienda', $variable)->update([
            'estado_tienda' => '0',
        ]);

        return redirect()->route('tiendas.listado')
            ->with('status_success','Tienda desactivada.'); 
    }

    public function historial_pedidos(){

        Gate::authorize('haveaccess', 'historial_pedidos.index');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
            if ($user->slug == "admin") {
                $pedidos_locales = DB::table('historial_pedido')
                                ->select('pedido_local.*', 'historial_pedido.codigo_tienda', 'historial_pedido.codigo_pedido as cod_pedido_historial', 'historial_pedido.estado_pedido')
                                ->join('pedido_local', 'historial_pedido.codigo_pedido','=','pedido_local.codigo_pedido')
                                ->where('historial_pedido.estado_pedido', '<>', 'Cerrado')
                                ->paginate(10);
                
                $pedidos_domicilios = DB::table('historial_pedido')
                                ->select('pedido_domicilio.*', 'historial_pedido.codigo_tienda', 'historial_pedido.codigo_pedido as cod_pedido_historial', 'historial_pedido.estado_pedido')
                                ->join('pedido_domicilio', 'historial_pedido.codigo_pedido','=','pedido_domicilio.codigo_pedido')
                                ->where('historial_pedido.estado_pedido', '<>', 'Cerrado')
                                ->paginate(10);

                return view('tiendas.historial_pedidos', compact('pedidos_domicilios', 'pedidos_locales'));

            } else {

                $array_codigo = [];

                $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();


                foreach ($tiendas as $tienda) {
                    $array_codigo[] = $tienda->id_tienda;
                }

                $pedidos_locales = DB::table('historial_pedido')
                                ->select('pedido_local.*', 'historial_pedido.codigo_tienda', 'historial_pedido.codigo_pedido as cod_pedido_historial', 'historial_pedido.estado_pedido')
                                ->join('pedido_local', 'historial_pedido.codigo_pedido','=','pedido_local.codigo_pedido')
                                ->where('historial_pedido.codigo_tienda', $array_codigo)
                                ->where('historial_pedido.estado_pedido', '<>', 'Cerrado')
                                ->paginate(10);
                
                $pedidos_domicilios = DB::table('historial_pedido')
                                ->select('pedido_domicilio.*', 'historial_pedido.codigo_tienda', 'historial_pedido.codigo_pedido as cod_pedido_historial', 'historial_pedido.estado_pedido')
                                ->join('pedido_domicilio', 'historial_pedido.codigo_pedido','=','pedido_domicilio.codigo_pedido')
                                ->where('historial_pedido.codigo_tienda', $array_codigo)
                                ->where('historial_pedido.estado_pedido', '<>', 'Cerrado')
                                ->paginate(10);

                return view('tiendas.historial_pedidos', compact('pedidos_domicilios', 'pedidos_locales'));
            }
        }
    }

    public function pedido_cambio_estado(Request $request){
        //return $request->all();
        DB::table('historial_pedido')
                ->where('codigo_pedido', $request->codigo_pedido)
                ->update([
                    'estado_pedido' => $request->estado
                ]);

        return redirect()->route('tiendas.historial')
            ->with('status_success','Pedido actualizado.');
    }

    public function ver_pedido_local($pedido){

        Gate::authorize('haveaccess', 'historial_pedidos.local');

        $pedidos_locales = DB::table('historial_pedido')
                                ->select('pedido_local.*', 'historial_pedido.codigo_tienda', 'historial_pedido.codigo_pedido as cod_pedido_historial', 'historial_pedido.estado_pedido')
                                ->join('pedido_local', 'historial_pedido.codigo_pedido','=','pedido_local.codigo_pedido')
                                ->where('historial_pedido.codigo_pedido', $pedido)
                                ->get();

        return view('tiendas.descripcion_pedido_local', compact('pedidos_locales'));
    }

    public function ver_pedido_domi($pedido){

        Gate::authorize('haveaccess', 'historial_pedidos.domicilio');

        $pedidos_domicilios = DB::table('historial_pedido')
                                ->select('pedido_domicilio.*', 'historial_pedido.codigo_tienda', 'historial_pedido.codigo_pedido as cod_pedido_historial', 'historial_pedido.estado_pedido')
                                ->join('pedido_domicilio', 'historial_pedido.codigo_pedido','=','pedido_domicilio.codigo_pedido')
                                ->where('historial_pedido.codigo_pedido', $pedido)
                                ->get();

        return view('tiendas.descripcion_pedido_domicilio', compact('pedidos_domicilios'));
    }
}
