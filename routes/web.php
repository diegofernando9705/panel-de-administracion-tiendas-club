<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Permisos\Models\Role;
use App\Permisos\Models\Permission;
use Illuminate\Support\Facades\Gate;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


Route::get('/registro', 'AuthPersonalizedController@register');
Route::post('/registro_usuario', 'AuthPersonalizedController@register_user')->name('register_user');
Route::post('/registro_persona', 'AuthPersonalizedController@registro_persona');
Route::post('/registro_tienda', 'AuthPersonalizedController@registro_tienda');

Route::get('busqueda/ciudad/search/{ciudad}', 'TiendasController@busquedaCiudad');

Auth::routes(['register' => false]);


Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home_index');

Route::get('/perfil', 'PersonasController@perfil')->name('perfil');
Route::get('/cambio_plan', 'PlanesController@cambio_plan')->name('cambio_plan');


Route::post('/perfil/update', 'PersonasController@perfilUpdate')->name('perfil.update');




Route::resource('/role', 'RoleController')->names('role');
Route::resource('/reportes', 'ReportesController')->names('reportes');

/* Modulo de la tienda*/
Route::get('/tiendas/listado', 'TiendasController@tiendas')->name('tiendas.listado');
Route::get('/tiendas/keyup/{variable}', 'TiendasController@keyuptienda');
Route::get('/tiendas/pedidos', 'TiendasController@historial_pedidos')->name('tiendas.historial');

Route::post('/tiendas/pedidos/estado', 'TiendasController@pedido_cambio_estado')->name('tiendas.estado');
Route::get('/tiendas/verpedido/local/{codigo}', 'TiendasController@ver_pedido_local')->name('tiendas.verPedidoLocal');
Route::get('/tiendas/verpedido/domicilio/{codigo}', 'TiendasController@ver_pedido_domi')->name('tiendas.verPedidoDomicilio');
Route::get('/tiendas/cerrar/pedido', 'TiendasController@ver_pedido_domi')->name('tiendas.pedidoDestroy');




Route::resource('/tiendas', 'TiendasController')->names('tiendas');
Route::resource('/zonas', 'ZonasController')->names('zonas');



/* Modulo de productos*/
Route::resource('/productos', 'ProductosController')->names('productos');
Route::get('/productos/keyup/{variable}', 'ProductosController@keyupproducto');


/* Modulo categorias de los productos*/
Route::get('/categoriasproductos/keyup/{variable}', 'CategoriasproductosController@keyup');
Route::resource('/categoriasproductos', 'CategoriasproductosController')->names('categoriasproductos');


/* Adicionales de los productos */
Route::get('adicionales/listar', 'AdicionalesController@index')->name('adicionales.index');
Route::get('adicionales/crear', 'AdicionalesController@crear')->name('adicionales.crear');
Route::PUT('adicionales/store', 'AdicionalesController@store')->name('adicionales.store');
Route::get('adicionales/show/{id}', 'AdicionalesController@show')->name('adicionales.show');
Route::get('adicionales/edicion/{id}', 'AdicionalesController@edicion')->name('adicionales.edicion');
Route::POST('adicionales/update/{id}', 'AdicionalesController@update')->name('adicionales.update');
Route::DELETE('adicionales/delete/{id}', 'AdicionalesController@delete')->name('adicionales.destroy');


/* Modulo de personas */
Route::resource('/personas', 'PersonasController')->names('personas');

Route::get('/perfil/{codigo}', 'PersonasController@perfil')->name('perfil.show');


/* Modulo de seugerencias */

Route::get('/sugerencias', 'FormulariosController@indexsugerencias')->name('sugerencias.index');
Route::get('/sugerencias/edit/{codigo]', 'FormulariosController@showSugerencia')->name('sugerencias.showsugerencia');
Route::DELETE('/sugerencias/delete/{codigo}', 'FormulariosController@deletesugerencias')->name('sugerencias.destroy');

Route::get('/contacto', 'FormulariosController@indexContacto')->name('contacto.index');


/* Modulo de notificaciones */

Route::get('/notification/get', 'HomeController@notification');
Route::get('/notification/show/{id?}', 'HomeController@ver_notification');


/* Modulo de planes */
Route::resource('/planes', 'PlanesController')->names('planes');
Route::get('/planes_asignar', 'PlanesController@asignar')->name('planes.asignar');
Route::post('/asignar_store', 'PlanesController@asignar_store')->name('planes.asignar_store');
Route::get('/asignaciones', 'PlanesController@asignaciones')->name('planes.asignaciones');

 	// ---  actualizar plan
	Route::post('/metodoUpdate', 'PlanesController@metodoPagoActualizacion')->name('planes.metodoUpdate');
	Route::post('/metodoUpdatePay/{valor}', 'PlanesController@metodoUpdatePay')->name('planes.metodoUpdatePay');
  Route::post('/metodoUpdatePayNequi/{valor}', 'PlanesController@metodoUpdatePayNequi')->name('planes.metodoUpdatePayNequi');

  //Ir al pago para actualizar plan
  Route::post('/pagoActualizacionPlan/{pago?}', 'PlanesController@pagoActualizacionPlan')->name('planes.pagoActualizacionPlan');

  /* Verificacion Pago con Nequi */
  Route::get('/wompi/verification/nequi/{id}', 'PlanesController@verification_nequi');
  Route::post('/wompi/verification/pagar/{id}', 'PlanesController@pagar_nequi');
  Route::get('/wompi/verification/transaction/{id}', 'PlanesController@verification_transaction');
  Route::post('/wompi/verification/registro_bd/{id}', 'PlanesController@registro_bd');
  Route::get('/cambio_plan', 'PlanesController@cambio_plan')->name('planes.cambio_plan');

  /*verificacion Pago con Nequi pero tarjetas de credito*/
  Route::get('/wompi/verification/nequi/tarjetas/{id}', 'PlanesController@verification_nequi');



/* Modulo de cupones */
Route::resource('/cupones', 'CuponesController')->names('cupones');


/* Modulo de Epayco */
Route::resource('/epayco', 'EpaycoController')->names('epayco');	
/* Modulo de Epayco */


/* Cerrar sesion*/
Route::get('/logout', 'HomeController@logout');

Route::resource('/user', 'UserController', ['except' => [
        'create', 'store']])->names('user');
