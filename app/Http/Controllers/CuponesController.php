<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;
use Illuminate\Support\Facades\Crypt;
use Gate;
use DateTime;

class CuponesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        Gate::authorize('haveaccess','cupones.index');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        if($usuario[0]->slug == 'admin'){

             $cupones = DB::table('detalle_cupones_tiendas')
                            ->select('detalle_cupones_tiendas.*', 'tiendas.*', 'tabla_cupones.*')
                            ->join('tiendas', 'detalle_cupones_tiendas.cod_detalle_tienda_cupon', '=', 'tiendas.id_tienda')
                            ->join('tabla_cupones', 'detalle_cupones_tiendas.cod_detalle_cupon', '=', 'tabla_cupones.cod_cupon')
                            ->get();
            return view('cupones.index', compact('cupones'));
        }else{
            
            $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();

        
            $cupones = DB::table('detalle_cupones_tiendas')
                            ->select('detalle_cupones_tiendas.*', 'tiendas.*', 'tabla_cupones.*')
                            ->join('tiendas', 'detalle_cupones_tiendas.cod_detalle_tienda_cupon', '=', 'tiendas.id_tienda')
                            ->join('tabla_cupones', 'detalle_cupones_tiendas.cod_detalle_cupon', '=', 'tabla_cupones.cod_cupon')
                            ->where('detalle_cupones_tiendas.cod_detalle_tienda_cupon', '=', $tiendas[0]->id_tienda)
                            ->get();

            $detalles_del_plan = DB::table('detalle_planes_tiendas')
              ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
              ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
              ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
              ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
              ->get();

            $dia_actual = new DateTime(date('Y-m-d'));
            $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

            $funcion = $dia_actual->diff($fecha_proximo_pago);

            if($fecha_proximo_pago < $dia_actual){
              $dias_faltantes = 0;
            }else{
              $dias_faltantes = $funcion->days;
            }

          /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

          if($dias_faltantes <= 0){
            $planes = DB::table('tabla_planes')->get();
            return view('planes.tienda.pago-plan', compact('planes'));
          }else{
            return view('cupones.index', compact('cupones'));
          }
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        Gate::authorize('haveaccess','cupones.create');


        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        foreach ($usuario as $user) {
          if ($user->slug == "admin") {

             $tiendas = DB::table('detalle_usuario_tiendas')
            ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
            ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
            ->join('users', 'personas.user_register', '=', 'users.id')
            ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
            ->where('users.id', Auth::user()->id)
            ->get();

            $usuario = DB::table('role_user')
              ->select('users.*', 'roles.*', 'role_user.*')
              ->join('roles', 'role_user.role_id', '=', 'roles.id')
              ->join('users', 'role_user.user_id', '=', 'users.id')
              ->where('users.id', Auth::user()->id)
              ->get();

            $tiendas = DB::table('tiendas')->get();
            return view('cupones.crear', compact('tiendas', 'usuario'));

          }else{
            $tiendas = DB::table('detalle_usuario_tiendas')
            ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
            ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
            ->join('users', 'personas.user_register', '=', 'users.id')
            ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
            ->where('users.id', Auth::user()->id)
            ->get();

            $detalles_del_plan = DB::table('detalle_planes_tiendas')
              ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
              ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
              ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
              ->where([['id_detalle_plan_tienda', $tiendas[0]->id_tienda], ['estado_detalle_plan', '1']])
              ->get();

              $dia_actual = new DateTime(date('Y-m-d'));
              $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

              $funcion = $dia_actual->diff($fecha_proximo_pago);

              if($fecha_proximo_pago < $dia_actual){
                $dias_faltantes = 0;
              }else{
                $dias_faltantes = $funcion->days;
              }


              /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

            if($dias_faltantes <= 0){
              $planes = DB::table('tabla_planes')->get();
              return view('planes.tienda.pago-plan', compact('planes'));
            }else{

              $usuario = DB::table('role_user')
                ->select('users.*', 'roles.*', 'role_user.*')
                ->join('roles', 'role_user.role_id', '=', 'roles.id')
                ->join('users', 'role_user.user_id', '=', 'users.id')
                ->where('users.id', Auth::user()->id)
                ->get();

          $tiendas = DB::table('tiendas')->get();
          return view('cupones.crear', compact('tiendas', 'usuario'));
            }



          }
        }

          
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        Gate::authorize('haveaccess','cupones.create');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        $request->validate([
            'cod_cupon' => 'required',
            'nombre_cupon' => 'required',
            'tipo_cupon' => 'required',
            'estado_cupon' => 'required'
        ]);

        if($request->tipo_cupon == 'dinero'){
            $request->validate([
                'valor_descuento' => 'required'
            ]);
        }else if($request->tipo_cupon == 'fecha'){
            
            $request->validate([
                'fecha_inicio' => 'required',
                'fecha_fin' => 'required'
            ]);

        }else{

        }


        if($usuario[0]->slug == 'admin'){

            $validate = DB::table('tabla_cupones')->where('cod_cupon', 'club_'.$request->cod_cupon)->count();


            if($validate > 0){
                return Redirect::back()->withErrors(['Código del cupón ya ha sido generado.']);
            }else{

                DB::table('tabla_cupones')->insert([
                    'cod_cupon' => 'club_'.$request->cod_cupon,
                    'nombre_cupon' => $request->nombre_cupon,
                    'descripcion_cupon' => $request->descripcion_cupon,
                    'tipo_cupon' => $request->tipo_cupon,
                    'valor_descuento' => $request->valor_descuento,
                    'fecha_inicio' => $request->fecha_inicio,
                    'fecha_fin' => $request->fecha_fin,
                    'estado_cupon' => $request->estado_cupon
                ]);

                DB::table('detalle_cupones_tiendas')->insert([
                    'cod_detalle_cupon' => 'club_'.$request->cod_cupon,
                    'cod_detalle_tienda_cupon' => $request->tienda
                ]);


                return redirect()->route('cupones.index')
                ->with('status_success','Cupón generado correctamente!'); 
            }


        }else{

            $tiendas = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                ->join('users', 'personas.user_register', '=', 'users.id')
                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                ->where('users.id', Auth::user()->id)->get();

            $validate = DB::table('tabla_cupones')->where('cod_cupon', 'club_'.$request->cod_cupon)->count();

            

            if($validate > 0){
                return Redirect::back()->withErrors(['Código del cupón ya ha sido generado.']);
            }else{

                DB::table('tabla_cupones')->insert([
                    'cod_cupon' => 'club_'.$request->cod_cupon,
                    'nombre_cupon' => $request->nombre_cupon,
                    'descripcion_cupon' => $request->descripcion_cupon,
                    'tipo_cupon' => $request->tipo_cupon,
                    'valor_descuento' => $request->valor_descuento,
                    'fecha_inicio' => $request->fecha_inicio,
                    'fecha_fin' => $request->fecha_fin,
                    'estado_cupon' => $request->estado_cupon
                ]);

                DB::table('detalle_cupones_tiendas')->insert([
                    'cod_detalle_cupon' => 'club_'.$request->cod_cupon,
                    'cod_detalle_tienda_cupon' => $tiendas[0]->id_tienda
                ]);


                return redirect()->route('cupones.index')
                ->with('status_success','Cupón generado correctamente!'); 
            }

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {      

        Gate::authorize('haveaccess','cupones.show');

        $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

            $cupones = DB::table('detalle_cupones_tiendas')
                            ->select('detalle_cupones_tiendas.*', 'tiendas.*', 'tabla_cupones.*')
                            ->join('tiendas', 'detalle_cupones_tiendas.cod_detalle_tienda_cupon', '=', 'tiendas.id_tienda')
                            ->join('tabla_cupones', 'detalle_cupones_tiendas.cod_detalle_cupon', '=', 'tabla_cupones.cod_cupon')
                            ->where('detalle_cupones_tiendas.cod_detalle_cupon', '=', $id)
                            ->get();

       

        return view('cupones.ver', compact('cupones', 'usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('haveaccess','cupones.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('haveaccess','cupones.edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Gate::authorize('haveaccess','cupones.destroy');

        DB::table('tabla_cupones')->where('cod_cupon', $id)->delete();

        return redirect()->route('cupones.index')
                ->with('status_success','Cupón eliminado correctamente!'); 

    }
}
