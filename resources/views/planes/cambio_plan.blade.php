@extends('layouts.app')

@section('content')


<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4" style="text-align: center;">
                            <div class="title-update col-11 col-sm-11 col-md-11 col-lg-11 col-xl-11">
                            	Actualizaci&oacute;n del plan actual <strong>{{ $detalles_del_plan[0]->nombre_plan }}</strong>
                            </div>

                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4" >
                            <form id="update_plan">
                            @csrf
                            @method('POST')

                            <div class="alert alert-danger">
                                <b>Atenci&oacute;n: </b> Tu plan a&uacute;n le quedan {{ $dias }} dia(s) de activaci&oacute;n
                            </div>

                            <div class="form-group">
                                <label><strong><code>(*) </code>Nombre plan: </strong></label>
                                <select class="form-control mi-selector" name="plan">
                                    <option value="" selected="">Seleccione un plan</option>
                                    @foreach($planes as $plan)
                                        @if($plan->id_plan == $detalles_del_plan[0]->id_plan)
                                            <option value="{{ $plan->id_plan }}" selected="">{{ $plan->nombre_plan }} - <em> $ {{ $plan->valor_plan }} </em></option>
                                        @else
                                            <option value="{{ $plan->id_plan }}" >{{ $plan->nombre_plan }} - <em> $ {{ $plan->valor_plan }} </em></option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <label><strong><code>(*) </code>Pagar con: </strong></label><br>
                                <br>
                                <div class="row">
                                    <!--
                                    <div class="col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                                        <center>
                                            <input type="checkbox" name="metodo" class="only-one" value="epayco">
                                        </center>
                                        <img src="{{ asset('img/logo_epayco.png') }}" width="100%">
                                    </div>-->
                                    <div class="col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                                         <center>
                                            <input type="checkbox" name="metodo" class="only-one" value="wompi">
                                        </center>
                                        <img src="{{ asset('img/logo_wompi.png') }}" width="100%">
                                    </div>
                                </div>
                            </div>
                   

                    <center>
                        <a href="{{ route('planes.index') }}" style="color:white; ">
                            <button type="button" class="btn btn-danger">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="25px" height="25px" class=""><g><g>
                                            <g>
                                                <path d="M256,0C114.508,0,0,114.497,0,256c0,141.493,114.497,256,256,256c141.492,0,256-114.497,256-256    C512,114.507,397.503,0,256,0z M256,472c-119.384,0-216-96.607-216-216c0-119.385,96.607-216,216-216    c119.384,0,216,96.607,216,216C472,375.385,375.393,472,256,472z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                        </g><g>
                                            <g>
                                                <path d="M343.586,315.302L284.284,256l59.302-59.302c7.81-7.81,7.811-20.473,0.001-28.284c-7.812-7.811-20.475-7.81-28.284,0    L256,227.716l-59.303-59.302c-7.809-7.811-20.474-7.811-28.284,0c-7.81,7.811-7.81,20.474,0.001,28.284L227.716,256    l-59.302,59.302c-7.811,7.811-7.812,20.474-0.001,28.284c7.813,7.812,20.476,7.809,28.284,0L256,284.284l59.303,59.302    c7.808,7.81,20.473,7.811,28.284,0C351.398,335.775,351.397,323.112,343.586,315.302z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                        </g></g> </svg>
                                Cancelar registro
                            </button>
                        </a>

                        <input type="button" class="btn btn-info" value="Continuar con el pago" id="continuar_metodo">
                    </center>
                    </form>
                        </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>

<script type="text/javascript">
    let Checked = null;
    //The class name can vary
    for (let CheckBox of document.getElementsByClassName('only-one')){
        CheckBox.onclick = function(){
        if(Checked!=null){
          Checked.checked = false;
          Checked = CheckBox;
        }
        Checked = CheckBox;
      }
    }
</script>
@endsection

