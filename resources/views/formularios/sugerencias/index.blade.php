<!-- Listado de las tiendas, no influye si es Administrador o Cliente -->

@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                                Sugerencias registradas en el sistema
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="messaging">
                                <b>Búsqueda de sugerencias:</b>

                                <input type="text" name="buscadorSugerencias" id="buscadorSugerencias" class="form-control" placeholder="Busca tu sugerencia..." />                            
                            </div>
                            <br>
                            <table class="table table-bordered table-hover" id="tabla-sugerencias">
                                <thead>
                                    <tr>
                                        <th>Nombre Persona</th>
                                        <th>Telefono Persona</th>
                                        <th>Calificacion en estrellas</th>
                                        <th>Mensaje</th>
                                        <th colspan="3"><center>Acciones</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($sugerencias as $sugerencia)
                                    <tr>
                                        <td>{{ $sugerencia->nombre_persona }}</td>
                                        <td>{{ $sugerencia->telefono_persona }}
                                        	<?php $numero = $sugerencia->calificacion_persona; ?></td>
                                        <td>
                                        	<p class="clasificacion">
                                        		<?php for($i=1;$i<=$numero;$i++){ ?> 
                                        			<label for="radio1" class="estrellas_label">&#9733;</label>
                                        		<?php } ?>
                                        	</p>
                                        </td>
                                        <td>{{ $sugerencia->mensaje_sugerencia }}</td>
                                        
                                        @if(Gate::authorize('haveaccess','sugerencias.destroy'))
                                        <td>
                                            <form method="POST" action="{{ route('sugerencias.destroy', $sugerencia->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm">
                                                    <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-archive-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
                                                    </svg>
                                                </button>
                                            </form>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modales-tiendas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-body">
        
    </div>
  </div>
</div>


@endsection
