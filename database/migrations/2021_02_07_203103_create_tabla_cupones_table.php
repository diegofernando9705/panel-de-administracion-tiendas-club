<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablaCuponesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabla_cupones', function (Blueprint $table) {
            $table->char('cod_cupon', 255);
            $table->char('nombre_cupon', 255);
            $table->char('descripcion_cupon', 255)->nullable();
            $table->char('tipo_cupon', 255);
            $table->char('valor_descuento', 255)->nullable();
            $table->char('fecha_inicio', 255)->nullable();
            $table->char('fecha_fin', 255)->nullable();
            $table->char('estado_cupon', 255);
            $table->timestamps();

            $table->primary('cod_cupon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabla_cupones');
    }
}
