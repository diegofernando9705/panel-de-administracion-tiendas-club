<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;
use Gate;
use DateTime;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        
        date_default_timezone_set('America/Bogota');

        //Se valida si el usuario ya tiene una tienda registrada
        $sql = DB::table('personas')->where('user_register', Auth::user()->id)->get();
        
        $departamentos = DB::table('departamentos')->get();
        $categorias_tienda = DB::table('categorias_tiendas')->where('estado_categoria_tienda', 1)->get();
        $mensaje = "Es importante que por favor registre su tienda para continuar con el proceso.";
        
        $dimension = sizeof($sql);

            if($dimension == 0){
                $usuario = DB::table('role_user')
                            ->select('users.*', 'roles.*', 'role_user.*')
                            ->join('roles', 'role_user.role_id', '=', 'roles.id')
                            ->join('users', 'role_user.user_id', '=', 'users.id')
                            ->where('users.id', Auth::user()->id)->get();

                if ($usuario[0]->slug == "admin") {
                    /* CODIGO VENTAS POR ULTIMOS 7 DIAS */

                        $number = intval((cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'))/2)/2); // 31



                        $fechamovimiento =  date('Y-m-d h:m');
                        $meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
                        $mes = substr($fechamovimiento, 5, -9);
                        if ($mes <= 12) {
                            $mes = $meses[$mes - 1];
                        }
                        else{
                            echo "Solo existen 12 meses hay un error en el formato de tu fecha: ".$fechamovimiento." contacta con soporte";
                        }
                        
                        $dias_mes = [];
                        $presupuesto = [];

                        $f=date("Y-m-d");
                        for( $i = 6; $i > 0; $i-- ){
                            $ultimos_dias[] = date("d", strtotime("$f   -$i day"));
                            $dias_mes[] = date("d", strtotime("$f   -$i day"));
                        }

                        foreach ($ultimos_dias as $dia) {
                            $fecha = date('Y-m-').$dia;
                            $presupuesto[] = DB::table('historial_pedido')->whereDate('created_at', $fecha)->count(); 
                        }

                        $dia_hoy = DB::table('historial_pedido')->whereDate('created_at', date('Y-m-d'))->count(); 
                        
                        /* FIN CODIGO VENTAS POR ULTIMOS 7 DIAS */





                        /* CODIGO VENTAS POR HORAS */
                        
                            $total_hora = [];
                            $array_hora = ['', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '00:00', '01:00', '02:00', '03:00'];
                            
                            unset($array_hora[0]);

                            echo $dimension_array = sizeof($array_hora);

                            for($i=1; $i<=$dimension_array; $i++){
                                echo $array_hora[$i]."-";
                                $total_hora[] = DB::table('historial_pedido')->whereTime('created_at', $array_hora[$i])->count();
                            }

                        /* FIN CODIGO VENTAS POR HORAS */



                        /* CODIGO TIENDAS CON MAS VENTAS */

                            $tiendas_nombres = [];
                            $ventas_tiendas = [];

                            $rating_tiendas = DB::table('tiendas')->get();

                            foreach ($rating_tiendas as $codigo) {
                                $tiendas_nombres[] = $codigo->nombre_tienda;
                                $ventas_tiendas[] = DB::table('historial_pedido')->where('codigo_tienda', $codigo->id_tienda)->count();
                                
                            }

                        /* FIN CODIGO TIENDAS CON MAS VENTAS */


                        /* VENTAS TOTALES */

                            $productos = DB::table('detalle_tienda_productos')
                            ->select('productos.name', 'productos.description', 'productos.id_categoria', 'productos.valor', 'productos.estado_producto', 'detalle_tienda_productos.*', 'productos.id as codigo_producto', 'detalle_tienda_productos.*', 'tiendas.id_tienda', 'tiendas.nombre_tienda')
                                ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                                ->join('tiendas', 'detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                                ->count();

                            $tiendas = DB::table('tiendas')->count();

                            $venta_local = DB::table('pedido_local')->select(DB::raw('SUM(total_pedido) as total_local'))->get();
                            $venta_domicilio = DB::table('pedido_domicilio')->select(DB::raw('SUM(total_pedido) as total_local'))->get();
                            
                            foreach ($venta_local as $local) {
                                $presupuesto_local = $local->total_local;
                            }

                            foreach ($venta_domicilio as $domicilio) {
                                $presupuesto_domicilio = $domicilio->total_local;
                            }

                            $ventas_totales = $presupuesto_domicilio+$presupuesto_local;
                        
                        /* FIN VENTAS TOTALES */


                        /* VENTAS TOTALES POR DIA */
                            date_default_timezone_set('America/Bogota');

                            $venta_local_dia = DB::table('historial_pedido')->whereDate('created_at', date('Y-m-d'))->count();

                        /* FIN VENTAS TOTALES */



                        /* VENTAS LOCALES VS DOMICILIOS */
                            date_default_timezone_set('America/Bogota');

                            $venta_local_total = DB::table('pedido_domicilio')->count();
                            $venta_domicilio_total = DB::table('pedido_local')->count();
                            
                        /* FIN VENTAS LOCALES VS DOMICILIOS */



                        /* CODIGO ESTRELLAS */

                        $array_descripcion = ['Una Estrella', 'Dos Estrellas', 'Tres Estrellas', 'Cuatro Estrellas', 'Cinco Estrellas'];
                        $array_informacion = [];

                        $array_valores = [];
                        $valores = [];


                        $rating_tienda = DB::table('detalle_buzon_tienda')
                                            ->select('detalle_buzon_tienda.*','tiendas.*','formulario_buzon_sugerencia.*')
                                            ->join('tiendas', 'detalle_buzon_tienda.codigo_tienda_buzon_fk', '=', 'tiendas.id_tienda')
                                            ->join('formulario_buzon_sugerencia', 'detalle_buzon_tienda.codigo_formulario_buzon_fk', '=', 'formulario_buzon_sugerencia.id')->get();

                        foreach ($rating_tienda as $tienda) {
                            $array_valores[] = $tienda->calificacion_persona;
                        }
                        
                        $valores[] = array_count_values($array_valores);

                        if(isset($valores[0][1])){
                            $array_informacion[] = $valores[0][1];
                        }else{
                            $array_informacion[] = 0;
                        }

                        if(isset($valores[0][2])){
                            $array_informacion[] = $valores[0][2];
                        }else{
                            $array_informacion[] = 0;
                        }

                        if(isset($valores[0][3])){
                            $array_informacion[] = $valores[0][3];
                        }else{
                            $array_informacion[] = 0;
                        }

                        if(isset($valores[0][4])){
                            $array_informacion[] = $valores[0][4];
                        }else{
                            $array_informacion[] = 0;
                        }


                        if(isset($valores[0][5])){
                            $array_informacion[] = $valores[0][5];
                        }else{
                            $array_informacion[] = 0;
                        }


                        /* FIN CODIGO ESTRELLAS */


                        return view('home', compact('productos', 'tiendas', 'venta_local', 'meses', 'mes', 'number', 'dias_mes', 'presupuesto', 'ventas_totales', 'dia_hoy', 'total_hora', 'array_hora', 'array_descripcion', 'array_informacion', 'venta_local_dia', 'venta_domicilio_total', 'venta_local_total'));
                        
                    } else {
                        


                        $array_codigo = [];

                        $tiendas = DB::table('detalle_usuario_tiendas')
                                        ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                        ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                        ->join('users', 'personas.user_register', '=', 'users.id')
                                        ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                        ->where('users.id', Auth::user()->id)->get();


                        foreach ($tiendas as $tienda) {
                            $array_codigo[] = $tienda->id_tienda;
                        }

                        $productos = DB::table('detalle_tienda_productos')
                        ->select('productos.name', 'productos.description', 'productos.id_categoria', 'productos.valor', 'productos.estado_producto', 'detalle_tienda_productos.*', 'productos.id as codigo_producto', 'detalle_tienda_productos.*', 'tiendas.id_tienda', 'tiendas.nombre_tienda')
                            ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                            ->join('tiendas', 'detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                            ->get();    
                }

            }else{

                if($sql[0]->estado_persona ==  "3"){

                    /* actualizar tienda */

                    $usuario = DB::table('role_user')
                            ->select('users.*', 'roles.*', 'role_user.*')
                            ->join('roles', 'role_user.role_id', '=', 'roles.id')
                            ->join('users', 'role_user.user_id', '=', 'users.id')
                            ->where('users.id', Auth::user()->id)->get();

                    foreach ($usuario as $user) {
                      $rol = $user->slug;
                    }

                    $informacion = DB::table('detalle_usuario_tiendas')
                        ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                        ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                        ->join('users', 'personas.user_register', '=', 'users.id')
                        ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                        ->where('users.id', Auth::user()->id)->get();

                    $id_persona = $sql[0]->id;


                    foreach ($informacion as $info) {

                        $tiendas = DB::table('tiendas')->where('id_tienda', $info->id_tienda)->get();
                        $categorias_tienda = DB::table('categorias_tiendas')->get();
                        $departamentos = DB::table('departamentos')->get();
                        $ciudades = DB::table('ciudades')->get();

                        return view('tiendas.edicion_temporal', compact('tiendas', 'departamentos', 'categorias_tienda', 'ciudades', 'rol', 'id_persona'));
                    }
                }else{

                    $usuario = DB::table('role_user')
                            ->select('users.*', 'roles.*', 'role_user.*')
                            ->join('roles', 'role_user.role_id', '=', 'roles.id')
                            ->join('users', 'role_user.user_id', '=', 'users.id')
                            ->where('users.id', Auth::user()->id)->get();


                    if ($usuario[0]->slug == "admin") {
                        return "";
                    }else{
                        

                        /* VER CODIGO DE LA TIENDA DEL USUARIO */

                            $informacion = DB::table('detalle_usuario_tiendas')
                                ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                                ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                                ->join('users', 'personas.user_register', '=', 'users.id')
                                                ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                                ->where('users.id', Auth::user()->id)->get();
                        /* FIN VER CODIGO DE LA TIENDA DEL USUARIO */

                        /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

                            $detalles_del_plan = DB::table('detalle_planes_tiendas')
                                            ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
                                            ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
                                            ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
                                            ->where([['id_detalle_plan_tienda', $informacion[0]->id_tienda], ['estado_detalle_plan', '1']])
                                            ->get();
                                            
                           // return $informacion[0]->id_tienda;
                            $dia_actual = new DateTime(date('Y-m-d'));
                            $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

                            $funcion = $dia_actual->diff($fecha_proximo_pago);
                            
                            if($fecha_proximo_pago < $dia_actual){
                                $dias_faltantes = 0;
                            }else{
                                $dias_faltantes = $funcion->days;
                            }
                        /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */


                        /* CODIGO VENTAS POR ULTIMOS 7 DIAS */

                            $number = intval((cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'))/2)/2); // 31

                            $fechamovimiento =  date('Y-m-d h:m');
                            $meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

                            $mes = substr($fechamovimiento, 5, -9);

                            if ($mes <= 12) {
                                $mes = $meses[$mes - 1];
                            }else{
                                echo "Solo existen 12 meses hay un error en el formato de tu fecha: ".$fechamovimiento." contacta con soporte";
                            }

                            $dias_mes = [];
                            $presupuesto = [];

                            $f=date("Y-m-d");
                            for( $i = 7; $i > 0; $i-- ){
                                $ultimos_dias[] = date("d", strtotime("$f   -$i day"));
                                $dias_mes[] = date("d", strtotime("$f   -$i day"));
                            }

                            foreach ($ultimos_dias as $dia) {
                                $fecha = date('Y-m-').$dia;
                                $presupuesto[] = DB::table('historial_pedido')->where('codigo_tienda', $informacion[0]->id_tienda)->whereDate('created_at', $fecha)->count(); 
                            }

                            $dia_hoy = DB::table('historial_pedido')->where('codigo_tienda', $informacion[0]->id_tienda)->whereDate('created_at', date('Y-m-d'))->count(); 
                        /* FIN CODIGO VENTAS POR ULTIMOS 7 DIAS */


                        /* CODIGO VENTAS POR HORAS */
                            $total_hora = [];
                            $array_hora = ['', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '00:00', '01:00', '02:00', '03:00'];
                            
                            unset($array_hora[0]);

                            $dimension_array = sizeof($array_hora);

                            for($i=1; $i<=$dimension_array; $i++){
                                //echo $array_hora[$i]."-";
                                $total_hora[] = DB::table('historial_pedido')->where('codigo_tienda', $informacion[0]->id_tienda)->whereTime('created_at', $array_hora[$i])->count();
                            }
                        /* FIN CODIGO VENTAS POR HORAS */


                        /* VENTAS TOTALES POR DIA */
                            date_default_timezone_set('America/Bogota');

                            $venta_local_dia = DB::table('historial_pedido')->whereDate('created_at', date('Y-m-d'))->where('codigo_tienda', $informacion[0]->id_tienda)->count();
                        /* FIN VENTAS TOTALES */


                        /* VENTAS LOCALES VS DOMICILIOS */
                            date_default_timezone_set('America/Bogota');

                            $venta_local_total = DB::table('historial_pedido')
                                                ->select('pedido_local.*')
                                                ->join('pedido_local', 'historial_pedido.codigo_pedido', '=', 'pedido_local.codigo_pedido')
                                                ->where('historial_pedido.codigo_tienda', $informacion[0]->id_tienda)->count();

                            $venta_domicilio_total = DB::table('historial_pedido')
                                                ->select('pedido_domicilio.*')
                                                ->join('pedido_domicilio', 'historial_pedido.codigo_pedido', '=', 'pedido_domicilio.codigo_pedido')
                                                ->where('historial_pedido.codigo_tienda', $informacion[0]->id_tienda)->count();
                        /* FIN VENTAS LOCALES VS DOMICILIOS */


                        /* CODIGO ESTRELLAS */

                            $array_descripcion = ['Una Estrella', 'Dos Estrellas', 'Tres Estrellas', 'Cuatro Estrellas', 'Cinco Estrellas'];
                            $array_informacion = [];

                            $array_valores = [];
                            $valores = [];


                            $rating_tienda = DB::table('detalle_buzon_tienda')
                                                ->select('detalle_buzon_tienda.*','tiendas.*','formulario_buzon_sugerencia.*')
                                                ->join('tiendas', 'detalle_buzon_tienda.codigo_tienda_buzon_fk', '=', 'tiendas.id_tienda')
                                                ->join('formulario_buzon_sugerencia', 'detalle_buzon_tienda.codigo_formulario_buzon_fk', '=', 'formulario_buzon_sugerencia.id')
                                                ->where('detalle_buzon_tienda.codigo_tienda_buzon_fk', $informacion[0]->id_tienda)->get();

                            foreach ($rating_tienda as $tienda) {
                                $array_valores[] = $tienda->calificacion_persona;
                            }
                            
                            $valores[] = array_count_values($array_valores);

                            if(isset($valores[0][1])){
                                $array_informacion[] = $valores[0][1];
                            }else{
                                $array_informacion[] = 0;
                            }

                            if(isset($valores[0][2])){
                                $array_informacion[] = $valores[0][2];
                            }else{
                                $array_informacion[] = 0;
                            }

                            if(isset($valores[0][3])){
                                $array_informacion[] = $valores[0][3];
                            }else{
                                $array_informacion[] = 0;
                            }

                            if(isset($valores[0][4])){
                                $array_informacion[] = $valores[0][4];
                            }else{
                                $array_informacion[] = 0;
                            }


                            if(isset($valores[0][5])){
                                $array_informacion[] = $valores[0][5];
                            }else{
                                $array_informacion[] = 0;
                            }
                        /* FIN CODIGO ESTRELLAS */


                        /* VER CANTIDAD DE PRODUCTOS */
                            $productos = DB::table('detalle_tienda_productos')->where('id_tienda', $informacion[0]->id_tienda)->count();
                            //
                        /* FIN VER CANTIDAD DE PRODUCTOS */

                        $tiendas = "1";


                        /* VER TOTALES DE VENTAS LOCALES Y DOMICILIOS JUNTOS */

                            $venta_local = DB::table('historial_pedido')
                                                    ->select('pedido_local.*')
                                                    ->select(DB::raw('SUM(total_pedido) as total_local'))
                                                    ->join('pedido_local', 'historial_pedido.codigo_pedido', '=', 'pedido_local.codigo_pedido')
                                                    ->where('historial_pedido.codigo_tienda', $informacion[0]->id_tienda)->get();

                            $venta_domicilio = DB::table('historial_pedido')
                                                    ->select('pedido_domicilio.*')
                                                    ->select(DB::raw('SUM(total_pedido) as total_local'))
                                                    ->join('pedido_domicilio', 'historial_pedido.codigo_pedido', '=', 'pedido_domicilio.codigo_pedido')
                                                    ->where('historial_pedido.codigo_tienda', $informacion[0]->id_tienda)->get();
                            
                            foreach ($venta_local as $local) {
                                $presupuesto_local = $local->total_local;
                            }

                            foreach ($venta_domicilio as $domicilio) {
                                $presupuesto_domicilio = $domicilio->total_local;
                            }

                            $ventas_totales = $presupuesto_domicilio+$presupuesto_local;
                        /* FIN VER TOTALES DE VENTAS LOCALES Y DOMICILIOS JUNTOS */


                        /* validar prueba de 15 dìas */
                        
                        /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */

                            $detalles_del_plan = DB::table('detalle_planes_tiendas')
                                            ->select('detalle_planes_tiendas.*', 'tiendas.*', 'tabla_planes.*')
                                            ->join('tiendas', 'detalle_planes_tiendas.id_detalle_plan_tienda', '=','tiendas.id_tienda')
                                            ->join('tabla_planes', 'detalle_planes_tiendas.id_detalle_plan', '=','tabla_planes.id_plan')
                                            ->where([['id_detalle_plan_tienda', $informacion[0]->id_tienda], ['estado_detalle_plan', '1']])
                                            ->get();
                                            
                           // return $informacion[0]->id_tienda;
                            $dia_actual = new DateTime(date('Y-m-d'));
                            $fecha_proximo_pago = new DateTime($detalles_del_plan[0]->fecha_proximo_pago);

                            $funcion = $dia_actual->diff($fecha_proximo_pago);
                            
                            if($fecha_proximo_pago < $dia_actual){
                                $dias_faltantes = 0;
                            }else{
                                $dias_faltantes = $funcion->days;
                            }
                        /* VER DIFERENCIAS ENTRE FECHAS SEGUN PLAN */


                         /* VALIDAMOS CUANDO SEA PLAN FREE */

                            $captura_rol = DB::table('role_user')->where('user_id', Auth::user()->id)->get();

                            if($captura_rol[0]->role_id == '755'){
                                return view('home-755', compact('productos', 'tiendas', 'venta_local', 'meses', 'mes', 'number', 'dias_mes', 'presupuesto', 'ventas_totales', 'dia_hoy', 'total_hora', 'array_hora', 'array_descripcion', 'array_informacion', 'venta_local_dia', 'venta_local_total', 'venta_domicilio_total', 'dias_faltantes', 'detalles_del_plan'));
                            }else{

                                if($dias_faltantes <= 0){
                                    
                                    $planes = DB::table('tabla_planes')->get();

                                    return view('planes.tienda.pago-plan', compact('planes'));
                                }else{

                                    return view('home', compact('productos', 'tiendas', 'venta_local', 'meses', 'mes', 'number', 'dias_mes', 'presupuesto', 'ventas_totales', 'dia_hoy', 'total_hora', 'array_hora', 'array_descripcion', 'array_informacion', 'venta_local_dia', 'venta_local_total', 'venta_domicilio_total', 'dias_faltantes', 'detalles_del_plan'));
                                }

                            /* validar prueba de 15 dìas */
                        }

                        /* FIN VALIDACIONES CUANDO SEA PLAN FREE */
                        


                }
            }
        }
    }


    public function logout(){
        Auth::logout();
        //Redireccionamos al inicio de la app con un mensaje
        return Redirect::to('/')->with('msg', 'Nos vemos pronto!.');
    }

    public function notification(){
        $usuario = DB::table('role_user')
                            ->select('users.*', 'roles.*', 'role_user.*')
                            ->join('roles', 'role_user.role_id', '=', 'roles.id')
                            ->join('users', 'role_user.user_id', '=', 'users.id')
                            ->where('users.id', Auth::user()->id)->get();
        
        $total_calificacion = [];

        if ($usuario[0]->slug == "admin") {
            $notification = DB::table('tabla_notificaciones')->where('estado_notificacion', 'recibida')->count();
            $descripcion_notificaciones = DB::table('tabla_notificaciones')->where('estado_notificacion', 'recibida')->orderBy('id_notificaciones', 'desc')->get();

            return view('custom.notifications', compact('notification', 'descripcion_notificaciones'));

        }else{

             $informacion = DB::table('detalle_usuario_tiendas')
                        ->select('personas.id', 'detalle_usuario_tiendas.*', 'tiendas.*', 'users.*')
                                        ->join('personas', 'detalle_usuario_tiendas.id_persona', '=', 'personas.id')
                                        ->join('users', 'personas.user_register', '=', 'users.id')
                                        ->join('tiendas', 'detalle_usuario_tiendas.id_tienda', '=', 'tiendas.id_tienda')
                                        ->where('users.id', Auth::user()->id)->get();

            $notification = DB::table('tabla_notificaciones')->where('estado_notificacion', 'recibida')->where('codigo_tienda_notificaciones', $informacion[0]->id_tienda)->count();
            
            $descripcion_notificaciones = DB::table('tabla_notificaciones')->where('estado_notificacion', 'recibida')->orderBy('id_notificaciones', 'desc')->where('codigo_tienda_notificaciones', $informacion[0]->id_tienda)->get();

            return view('custom.notifications', compact('notification', 'descripcion_notificaciones'));

        }
    }


    public function ver_notification($id){

        $descripcion_notificaciones = DB::table('tabla_notificaciones')->where('id_notificaciones', $id)->get();
        
        foreach ($descripcion_notificaciones as $value) {
            
            if($value->seccion_notificaciones == 'formulario-sugerencia'){
                
                DB::table('tabla_notificaciones')->where('id_notificaciones', $id)->update(['estado_notificacion' => 'leida']);
                return redirect('/sugerencias');

            }else if($value->seccion_notificaciones == 'formulario-contacto'){
                
                DB::table('tabla_notificaciones')->where('id_notificaciones', $id)->update(['estado_notificacion' => 'leida']);
                return redirect('/contacto');
                
            }else{

            }

        }
    }
}
