<div class="container" class="contenerdorFormulario" style="margin-bottom: 50px;">

    <div class="tituloModal">
        <div class="container">
            <h3>
                <strong>Edición de la zona</strong>
            </h3>
        </div>
    </div> 

    <hr>

{!! Form::open(['route' => array('zonas.update', $zonas[0]->id_zona), 'files' => true]) !!}
@csrf
@method('PUT')
    
    <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                <label style="padding-bottom: 5px;"><b>(*) Nombre zona:</b></label>
                <input type="text" class="form-control" placeholder="" name="nombre" value="{{ $zonas[0]->nombre_zona }}">
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                <label style="padding-bottom: 5px;"><b> Descripci&oacute;n de la zona:</b></label>
                <input type="text" class="form-control" placeholder="" name="descripcion" value="{{ $zonas[0]->descripcion_zona }}">
            </div>
        </div>
    </div>

    <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                <label style="padding-bottom: 5px;"><b>(*) Celular:</b></label>
                <input type="number" class="form-control" placeholder="" name="celular" value="{{ $zonas[0]->celular_zona }}">
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                <label style="padding-bottom: 5px;"><b>(*) Estado:</b></label>
                <select class="form-control mi-selector" name="estado" required="">
                    
                    @if($zonas[0]->estado_zona == '1')
                        <option value="1" selected="">Activo</option>
                        <option value="0">Inactivo</option>    
                    @else
                        <option value="1">Activo</option>
                        <option value="0" selected="">Inactivo</option>
                    @endif
                    
                </select>
            </div>
        </div>
    </div>
    

    @if($usuario[0]->slug == 'admin')
    

        <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <label style="padding-bottom: 5px;"><b>(*) Tiendas:</b></label>
                    <select class="form-control mi-selector" name="tienda" required="">

                        @foreach($tiendas as $tienda)
                            @if($zonas[0]->codigo_tienda_detalle == $tienda->id_tienda)
                                <option value="{{ $tienda->id_tienda }}" selected="">{{ $tienda->nombre_tienda }}</option>
                            @else
                                <option value="{{ $tienda->id_tienda }}">{{ $tienda->nombre_tienda }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>      
    @endif


    <center>
        <button type="submit" class="btn btn-info" id="registroTienda">Actualizar zona</button>
    </center>

{!! Form::close() !!}

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.mi-selector').select2();
    });
</script>