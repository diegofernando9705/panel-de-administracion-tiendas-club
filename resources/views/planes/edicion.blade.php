<div class="container" style="padding-top: 30px; padding-bottom: 30px;">

		{!! Form::open(['route' => array('planes.update', $plan[0]->id_plan)]) !!}
			@csrf
			@method('PUT')

			<div class="form-group">
				<label><strong><code>(*) </code>Nombre plan: </strong></label>
				<input type="text" class="form-control" name="nombre_plan" value="{{ $plan[0]->nombre_plan }}">
			</div>


			<div class="form-group">
				<label>Descripción del plan:</label>
				<textarea class="form-control" name="descripcion_plan" maxlength="100">{{ $plan[0]->descripcion_plan }}</textarea>
			</div>


			<div class="form-group">
				<label><strong><code>(*) </code>Valor del plan: </strong></label><br>
				<input type="number" class="form-control" name="valor_plan" value="{{ $plan[0]->valor_plan }}" />
			</div>

			<div class="form-group">
				<label><strong><code>(*) </code>Estado: </strong></label><br>
				<select class="form-control mi-selector" name="estado_plan">
					@if($plan[0]->estado_plan == '1')
                        <option value="1" selected="">Activo</option>
                        <option value="0">Inactivo</option>    
                    @else
                        <option value="1">Activo</option>
                        <option value="0" selected="">Inactivo</option>
                    @endif
				</select>
			</div>
			
			<center>
				<br>
				<button type="submit" class="btn btn-success">Actualizar</button>
			</center>
		{!! Form::close() !!}
	
</div>
