@extends('layouts.login')

@section('content')

    <!-- content -->
    <div class="">
        <!-- login form -->
        <section class="login-form py-md-5 py-3">
            <div class="card card_border p-md-4">
                <div class="card-body" id="contenido-registro">
                    <!-- form -->

                    <form id="formulario-registro">

                        <div class="login__header text-center mb-lg-5 mb-4">
                            <h3 class="login__title mb-2">
                                <img src="https://tiendas.club/web_principal/images/TIENDAS_CLUB.png" width="150px">
                            </h3>
                            <br>
                            <p>Cree su tienda <b>COMPLETAMENTE GRATIS</b>.</p>
                            <div id="informacion-respuesta"></div>
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1" value="{{ old('nombre_tienda') }}" required autocomplete="email" autofocus><strong>Nombre de usuario: </strong></label>
                            <input type="text" class="form-control login_text_field_bg input-style" name="nombre_usuario" aria-describedby="emailHelp" placeholder="" required="" autofocus="" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAUBJREFUOBGVVE2ORUAQLvIS4gwzEysHkHgnkMiEc4zEJXCMNwtWTmDh3UGcYoaFhZUFCzFVnu4wIaiE+vvq6+6qTgthGH6O4/jA7x1OiCAIPwj7CoLgSXDxSjEVzAt9k01CBKdWfsFf/2WNuEwc2YqigKZpK9glAlVVwTTNbQJZlnlCkiTAZnF/mePB2biRdhwHdF2HJEmgaRrwPA+qqoI4jle5/8XkXzrCFoHg+/5ICdpm13UTho7Q9/0WnsfwiL/ouHwHrJgQR8WEwVG+oXpMPaDAkdzvd7AsC8qyhCiKJjiRnCKwbRsMw9hcQ5zv9maSBeu6hjRNYRgGFuKaCNwjkjzPoSiK1d1gDDecQobOBwswzabD/D3Np7AHOIrvNpHmPI+Kc2RZBm3bcp8wuwSIot7QQ0PznoR6wYSK0Xb/AGVLcWwc7Ng3AAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;" value="{{ old('nombre_tienda') }}">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1" required autocomplete="email" autofocus><strong>Correo electrónico:</strong> </label>
                            <input type="email" class="form-control login_text_field_bg input-style" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required="" autofocus="" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAUBJREFUOBGVVE2ORUAQLvIS4gwzEysHkHgnkMiEc4zEJXCMNwtWTmDh3UGcYoaFhZUFCzFVnu4wIaiE+vvq6+6qTgthGH6O4/jA7x1OiCAIPwj7CoLgSXDxSjEVzAt9k01CBKdWfsFf/2WNuEwc2YqigKZpK9glAlVVwTTNbQJZlnlCkiTAZnF/mePB2biRdhwHdF2HJEmgaRrwPA+qqoI4jle5/8XkXzrCFoHg+/5ICdpm13UTho7Q9/0WnsfwiL/ouHwHrJgQR8WEwVG+oXpMPaDAkdzvd7AsC8qyhCiKJjiRnCKwbRsMw9hcQ5zv9maSBeu6hjRNYRgGFuKaCNwjkjzPoSiK1d1gDDecQobOBwswzabD/D3Np7AHOIrvNpHmPI+Kc2RZBm3bcp8wuwSIot7QQ0PznoR6wYSK0Xb/AGVLcWwc7Ng3AAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;" value="{{ old('email') }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password" ><strong>{{ __('Contraseña') }}:</strong></label>
                                <input id="password" type="password" class="form-control  login_text_field_bg input-style @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>


                        <div class="form-group">
                            <label for="password-confirm"><strong>{{ __('Confirmar contraseña') }}:</strong></label>
                                <input id="password-confirm" type="password" class="form-control  login_text_field_bg input-style" name="password_confirmation" required autocomplete="new-password">                            
                        </div>
                        
                        <div class="align-items-center">
                            <center>
                                <a href="{{ url('/') }}">
                                <button type="button" class="btn btn-danger btn-style mt-4">Regresar</button>
                                </a>
                                <input type="button" class="btn btn-primary btn-style mt-4" id="btn-register-new" value="Registrarmes">
                            </center>
                        </div>
                    </form>

                </div>
            </div>
        </section>

    </div>

    @endsection
