<div class="container" class="contenerdorFormulario">
	<div class="form-group">
		<label><strong><code>(*) </code>C&oacute;digo del cup&oacute;n: </strong></label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" value="{{ $cupones[0]->cod_cupon }}" disabled="">
		</div>
	</div>

	<div class="form-group">
		<label><strong><code>(*) </code>Nombre cup&oacute;n: </strong></label>
		<input type="text" class="form-control" value="{{ $cupones[0]->cod_cupon }}" disabled="">
	</div>

	<div class="form-group">
		<label>Descripción del cup&oacute;n:</label>
		<textarea class="form-control" maxlength="100" disabled="">{{ $cupones[0]->descripcion_cupon }}</textarea>
	</div>


	<div class="form-group">
		<label><strong><code>(*) </code>Descuento por: </strong></label><br>
		<input type="text" class="form-control" value="{{ $cupones[0]->tipo_cupon }}" disabled="">
	</div>

	<div class="form-group">
		<label><strong><code></code>Valor del descuento: </strong></label><br>
		<input type="number" class="form-control" value="{{ $cupones[0]->valor_descuento }}"  disabled="" />
		<small id="emailHelp" class="form-text text-muted">Llenar si se ha seleccionado tipo: Dinero.</small>
	</div>


	<div class="form-group">
		<label><strong>Fecha del cupón: </strong></label><br>
		<div class="row">
			<div class="fecha_inicio col-6 col-md-6 col-lg-6 col-sm-6 col-xl-6">
				<input type="date" class="form-control" value="{{ $cupones[0]->fecha_inicio }}" disabled="" >
				<small id="emailHelp" class="form-text text-muted">Fecha inicial para la activación.</small>
			</div>

			<div class="fecha_fin col-6 col-md-6 col-lg-6 col-sm-6 col-xl-6">
				<input type="date" class="form-control" value="{{ $cupones[0]->fecha_fin }}" disabled="" >
				<small id="emailHelp" class="form-text text-muted">Fecha final para la desactivación.</small>
			</div>
		</div>
	</div>
	 @if($usuario[0]->slug == 'admin')
	<div class="form-group">
		<label><strong><code>(*) </code>Tienda: </strong></label><br>
		@if($cupones[0]->id_tienda == '1')
			<input class="form-control" value="Administrador" disabled="">
		@else
			<input class="form-control" value="{{ $cupones[0]->nombre_tienda }}" disabled="">
		@endif
	</div>
	@endif

	<div class="form-group">
		<label><strong><code>(*) </code>Estado: </strong></label><br>
		<select class="form-control mi-selector" name="estado_cupon" disabled="">
			@if($cupones[0]->estado_cupon == '1')
				<option value="1">Activo</option>
				<option value="2">Próximo</option>
				<option value="0">Inactiva</option>
			@elseif($cupones[0]->estado_cupon == '2')
				<option value="1">Activo</option>
				<option value="2" selected="">Próximo</option>
				<option value="0">Inactiva</option>
			@else
				<option value="1">Activo</option>
				<option value="2">Próximo</option>
				<option value="0" selected="">Inactiva</option>
			@endif
		</select>
	</div>
</div>