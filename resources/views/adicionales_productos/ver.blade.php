<div class="tituloModal">
    <div class="container">
        <h3>
            <strong>Informaci&oacute;n del adicional</strong>
        </h3>
    </div>
</div> 
<div class="container" class="contenerdorFormulario">

	@foreach($adicionales as $adicion)
	<div class="form-group">
                        <label><strong><code>(*) </code>Nombre adicional: </strong></label>
                        <input type="text" class="form-control" value="{{ $adicion->nombre_adicional }}" disabled="">
                    </div>

                    <div class="form-group">
                        <label>Descripción del adicional:</label>
                        <input type="text" class="form-control" value="{{ $adicion->descripcion_adicional }}" disabled="">
                    </div>
                    
                    <div class="form-group">
                        <label><strong><code>(*) </code>Valor del adicional: </strong></label><br>
                            <input type="number" class="form-control" value="{{ $adicion->valor_adicional }}"  disabled=""/>
                    </div>

                    <div class="form-group">
                        <label><strong><code>(*) </code>Productos del adicional: </strong></label><br>
                        @foreach($productos as $producto)
                        	<?php if (is_array($codigo_productos) && in_array($producto->id, $codigo_productos)) { ?>
                        		<input type="text" class="form-control" value="{{ $producto->name }}"  disabled="" style="margin-bottom: 10px;" />
                        	<?php } ?>
                        @endforeach
                    </div>

                            </div>
                @endforeach