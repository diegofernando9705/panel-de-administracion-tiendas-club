<div class="caja_superpuesta" id="caja_superpuesta"></div>
<div class="form-group owner">
    <label for="owner"><code><b>(*)</b></code> Nombre en la tarjeta:</label>
    <input type="text" class="form-control" id="owner" name="nombre_tarjeta" />
</div>

<div class="form-group" id="card-number-field">
    <label for="cardNumber"><code><b>(*)</b></code> N&uacute;mero de tarjeta:</label>
    <input type="text" class="form-control" id="cardNumber" name="numero_tarjeta" />
</div>

<div class="form-group CVV">
    <label for="cvv"><code><b>(*)</b></code> C&oacute;digo de seguridad:</label>
    <input type="text" class="form-control" id="cvv" name="cvv_tarjeta" />
</div>


<div class="form-group" id="expiration-date">
    <label>Expiration Date</label>
    <select name="mes_tarjeta">
        <option value="01">Enero</option>
        <option value="02">Febrero </option>
        <option value="03">Marzo</option>
        <option value="04">Abril</option>
        <option value="05">Mayo</option>
        <option value="06">Junio</option>
        <option value="07">Julio</option>
        <option value="08">Agosto</option>
        <option value="09">Septiembre</option>
        <option value="10">Octubre</option>
        <option value="11">Noviembre</option>
        <option value="12">Diciembre</option>
    </select>
    <select name="ano_tarjeta">
        <option value="21">2021</option>
        <option value="22">2022</option>
        <option value="23">2023</option>
        <option value="24">2024</option>
        <option value="25">2025</option>
        <option value="26">2026</option>
        <option value="27">2027</option>
        <option value="28">2028</option>
        <option value="29">2029</option>
        <option value="30">2030</option>

    </select>
</div>
        <div class="row">
            <div class="valor_pagaras col-12 col-sm-12 col-lg-12 col-xl-12">
                <p>
                    
                </p>
            </div>
            <div class="form-group col-12 col-sm-12 col-lg-12 col-xl-12">
                <label>N&Uacute;MERO DE CUOTAS</label>
                <select class="form-control" name="cuotas">
                    <option value="1">1 cuota</option>
                    <option value="2">2 cuotas</option>
                    <option value="3">3 cuotas</option>
                    <option value="4">4 cuotas</option>
                    <option value="5">5 cuotas</option>
                    <option value="6">6 cuotas</option>
                    <option value="7">7 cuotas</option>
                    <option value="8">8 cuotas</option>
                    <option value="9">9 cuotas</option>
                    <option value="10">10 cuotas</option>
                </select>
            </div>
        </div>

<div class="form-group" id="credit_cards">
    <img src="https://demo.tutorialzine.com/2016/11/simple-credit-card-validation-form/assets/images/visa.jpg" id="visa" />
    <img src="https://demo.tutorialzine.com/2016/11/simple-credit-card-validation-form/assets/images/mastercard.jpg" id="mastercard" />
    <img src="https://demo.tutorialzine.com/2016/11/simple-credit-card-validation-form/assets/images/amex.jpg" id="amex" />
</div>

<script type="text/javascript">

    var owner = $('#owner'),
    cardNumber = $('#cardNumber'),
    cardNumberField = $('#card-number-field'),
    CVV = $("#cvv"),
    mastercard = $("#mastercard"),
    confirmButton = $('#confirm-purchase'),
    visa = $("#visa"),
    amex = $("#amex");

    cardNumber.payform('formatCardNumber');
    CVV.payform('formatCardCVC');

    cardNumber.keyup(function() {
        amex.removeClass('transparent');
        visa.removeClass('transparent');
        mastercard.removeClass('transparent');

        if ($.payform.validateCardNumber(cardNumber.val()) == false) {
            cardNumberField.removeClass('has-success');
            cardNumberField.addClass('has-error');
        } else {
            cardNumberField.removeClass('has-error');
            cardNumberField.addClass('has-success');
        }


        if ($.payform.parseCardType(cardNumber.val()) == 'visa') {
            mastercard.addClass('transparent');
            amex.addClass('transparent');
        } else if ($.payform.parseCardType(cardNumber.val()) == 'amex') {
            mastercard.addClass('transparent');
            visa.addClass('transparent');
        } else if ($.payform.parseCardType(cardNumber.val()) == 'mastercard') {
            amex.addClass('transparent');
            visa.addClass('transparent');
        }

    });

</script>
<br>
<center>
<button type="button" class="btn btn-success" id="proceder_pago" data-metodo="tarjetas-nequi">Continuar</button>
</center>