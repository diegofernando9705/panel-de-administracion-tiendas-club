@extends('layouts.app')

@section('content')


<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                                Modulo de Roles
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
<table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Slug</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">¿Acceso total?</th>
                                <th colspan="3"></th>
                            </tr>
                        </thead>
                        <tbody>


                            @foreach ($roles as $role)
                            <tr>
                                <th scope="row">{{ $role->id}}</th>
                                <td>{{ $role->name}}</td>
                                <td>{{ $role->slug}}</td>
                                <td>{{ $role->description}}</td>
                                <td>{{ $role['full-access']}}</td>                            
                                <td> 
                                    @can('haveaccess','role.show')
                                    <a class="btn btn-info" href="{{ route('role.show',$role->id)}}">Ver</a> 
                                    @endcan 
                                </td>  
                                <td> 
                                    @can('haveaccess','role.edit')
                                    <a class="btn btn-success" href="{{ route('role.edit',$role->id)}}">Editar</a> 
                                    @endcan
                                </td>  

                                <td> 
                                    @can('haveaccess','role.destroy')
                                    <form action="{{ route('role.destroy',$role->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger">Eliminar</button>
                                    </form>
                                    @endcan

                                </td>  
                            </tr>      
                            @endforeach





                        </tbody>
                    </table>

                    {{ $roles->links() }}

                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modales-tiendas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-body">
        
    </div>
  </div>
</div>

@endsection
