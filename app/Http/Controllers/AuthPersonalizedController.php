<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\User;

class AuthPersonalizedController extends Controller
{
    public function register(){
    	$planes = DB::table('tabla_planes')->where('estado_plan', 1)->get();
    	return view('auth.registro', compact('planes'));
    }

    public function register_user(Request $request){

    	$user = DB::table('users')->where('email', $request->email)->count();

    	if($user > 0){
    		return 3;
    	}else{

            $request->validate([
                'nombre_usuario' => 'required',
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if($request->password === $request->password_confirmation){
                
                DB::table('users')->insert([
                    'name' => $request->nombre_usuario,
                    'email' => $request->email,
                    'register_complete' => '3',
                    'password' => Hash::make($request->password),
                ]);

                $usuario = DB::table('users')->where('email', $request->email)->get();

                $departamentos = DB::table('departamentos')->get();

    		  return view('personas.auth.registro-personas', compact('departamentos', 'usuario'));
            
            }else{
                return 4;   
            }

            

    	}

    }

    public function registro_persona(Request $request){

    	$request->validate([
    		'primer_nombre' => 'required',
			'primer_apellido' => 'required',
			'celular' => 'required',
			'email' => 'required|email',
			'direccion' => 'required',
			'barrio' => 'required',
			'departamento' => 'required',
			'ciudad' => 'required'
    	]);

        $pattern = '/^([0-9]{4})(-)([0-9]{7})$/';
        $pattern_2 = '/^([0-9]{3})(-)([0-9]{4})(-)([0-9]{4})$/';

        if((preg_match($pattern, $request->celular)) || (preg_match($pattern_2, $request->celular))){

            return 2;

        } else {
            $usuario = DB::table('users')->where('email', $request->email)->get();

            $codigo_tienda = rand(1, 99999);
            
            DB::table('personas')->insert([
                'foto_persona'       => 'Tiendas club',
                'primer_nombre'      => $request->primer_nombre,
                'segundo_nombre'     => $request->segundo_nombre,
                'primer_apellido'    => $request->primer_apellido,
                'segundo_apellido'   => $request->segundo_apellido,
                'telefono'           => $request->celular,
                'correo_electronico' => $request->email,
                'id_ciudad'          => $request->ciudad,
                'direccion'          => $request->direccion,
                'barrio'             => $request->barrio,
                'user_register'      => $usuario[0]->id,
                'estado_persona'     => '3',
            ]);

            $departamentos = DB::table('departamentos')->get();
            $personas = DB::table('personas')->orderby('id','DESC')->take(1)->get();
            $categorias_tienda = DB::table('categorias_tiendas')->get();
            $id_user = Crypt::encryptString($usuario[0]->id);

            DB::table('detalle_usuario_tiendas')->insert([
                'id_persona' => $personas[0]->id,
                'id_tienda' => $codigo_tienda
            ]);

            DB::table('role_user')->insert([
                'role_id' => '600',
                'user_id' => $usuario[0]->id
            ]);
        }

    	

    	return view('tiendas.auth.registro-tienda', compact('codigo_tienda', 'departamentos', 'categorias_tienda', 'usuario', 'id_user'));
    }

    public function registro_tienda(Request $request){


        $tienda_validate = DB::table('tiendas')->where('url_tienda', $request->nombreTiendaUnico)->count();
        $codigo_persona = DB::table('personas')->where('user_register', Crypt::decryptString($request->id_user))->get();

        if($tienda_validate > 0){
            return 4;
        }else{

            $codigo_persona = DB::table('personas')->where('user_register', $request->id_user)->get();

            $request->validate([
                'codigo_tienda' => 'required',
                'nombreTiendaUnico' => 'required',
                'nombreTienda' => 'required',
                'descripcionTienda' => 'required',
                'direccionTienda' => 'required',
                'horarioTienda' => 'required',
                'domicilio' => 'required',
                'ciudadTienda' => 'required',
                'id_user' => 'required',
                'categoria_tienda' => 'required',
            ]);


            DB::table('tiendas')->insert([
                'id_tienda' => $request->codigo_tienda,
                'image_tienda' => 'vacio',
                'background_tienda' => 'vacio',
                'nombre_tienda' => $request->nombreTienda,
                'url_tienda' => $request->nombreTiendaUnico,
                'descripcion_tienda' => $request->descripcionTienda,
                'direccion_tienda' => $request->direccionTienda,
                'horario_tienda' => $request->horarioTienda,
                'domicilio' => $request->domicilio,
                'id_cate_tienda' => $request->categoria_tienda,
                'id_ciudad' => $request->ciudadTienda,
                'telefono_movil' => $request->celular_zona,
                'telefono_fijo' => $request->telFijo,
                'instagram_link' => $request->linkIns,
                'facebook_link' => $request->linkFb,
                'twitter_link' => $request->linkTwitter,
                'estado_tienda' => '1',
            ]);

            $date = date("d-m-Y");

            $fecha_15 = strtotime($date."+ 15 days");

            DB::table('detalle_planes_tiendas')->insert([
                'id_detalle_plan' => '2',
                'id_detalle_plan_tienda' => $request->codigo_tienda,
                'fecha_pago' => date('Y-m-d'),
                'fecha_proximo_pago' => date("Y-m-d", $fecha_15),
                'estado_detalle_plan' => '1'
            ]);


            /* INSERTAR ZONA PREDETERMINADA */

            DB::table('zonas_tiendas')->insert([
                    'nombre_zona' => $request->nombre_zona, 
                    'descripcion_zona' => $request->descripcion_zona, 
                    'celular_zona' => $request->celular_zona, 
                    'estado_zona' => '1' ]);

            $zona = DB::table('zonas_tiendas')->orderBy('id_zona', 'desc')->take(1)->get();

            DB::table('detalle_zonas_tiendas')->insert([
                    'id_zona_detalle' => $zona[0]->id_zona,
                    'codigo_tienda_detalle' => $request->codigo_tienda
            ]);


        /* FIN INSERTAR ZONA PREDETERMINADA */
        

            return 1;    
            
        }
        
    }
}
