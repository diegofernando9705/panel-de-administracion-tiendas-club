<!-- Listado de las tiendas, no influye si es Administrador o Cliente -->

@extends('layouts.app')

@section('content')




<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                                Adicionales de todos los productos
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="messaging">
                                <b>Búsqueda de adicional:</b>
                                <input type="text" name="buscadorAdicional" id="buscadorCategoriasProductos" class="form-control" placeholder="Busca tu categoria..." />
                            </div>
                            <br>
                            <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nombre adicion</th>
                                <th>Descripcion adicion</th>
                                <th>Valor adicion</th>
                                <th colspan="3"><center>Acciones</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($adicionales as $adicion)
                            
                                <tr>
                                <td>{{ $adicion->nombre_adicional }}</td>
                                <td>{{ $adicion->descripcion_adicional }}</td>
                                <td>{{ $adicion->valor_adicional }}</td>


                                @if(Gate::authorize('haveaccess','productos.show'))
                                <td>
                                    <a href="{{ route('productos.show', $adicion->id_unico)}}">
                                        <button type="button" class="btn btn-success btn-sm">
                                            <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                            <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                            </svg>
                                        </button>
                                    </a>
                                </td>
                                @endif
                                
                                @if(Gate::authorize('haveaccess','productos.destroy'))
                                <td>
                                    <button type="button" class="btn btn-danger btn-sm">
                                        <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-archive-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
                                        </svg>
                                    </button>
                                </td>
                                @endif
                            </tr>

                            @endforeach
                            
                    </table>
                        </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modales-tiendas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-body">
        
    </div>
  </div>
</div>



@endsection
