<div class="tituloModal">
    <div class="container">
        <h3>
            <strong>Ver detalles de la categoria</strong>
        </h3>
    </div>
</div> 
<div class="container" class="contenerdorFormulario" style="margin-bottom: 20px;">
@foreach($categorias as $categoria)
    {!! Form::open(['route' => array('categoriasproductos.update', $categoria->id), 'files' => true]) !!}
                                @csrf
                                @method('PUT')

                                <div class="row">
                                    <div class="form-group col-3 col-md-3 col-lg-3 col-xl-3">
                                        <img src="{{ asset('/storage/'.$categoria->imagen_categoria) }}" width="150px"/>
                                    </div>
                                    <input type="hidden" name="imagen_bd" value="{{ $categoria->imagen_categoria }}">
                                   
                                </div>

                                <div class="form-group">
                                    <label><strong><code>(*) </code>Nombre categoria: </strong></label>
                                    <input type="text" class="form-control" name="nombre_categoria" value="{{ $categoria->nombre_categoria }}" disabled="" >
                                </div>

                                <div class="form-group">
                                    <label>Descripción de la categoria:</label>
                                    <input type="text" class="form-control" name="descripcion_categoria" value="{{ $categoria->descripcion_categoria }}" disabled="" >
                                </div>
                                <div class="form-group">
                                    <label><strong><code>(*)</code>Estado de la categoria:</strong></label><br>
                                <select class="form-control mi-selector" id="estado_categoria" name="estado_categoria" disabled="" >
                                            @if($categoria->estado_categoria == 1)
                                                <option value="1" selected="">Activo</option>
                                                <option value="0">Inactivo</option>
                                            @else
                                                <option value="0">Activo</option>
                                                <option value="0" selected="">Inactivo</option>
                                            @endif
                                        </select> 
                                    </div>
                                
                                <!--
                                <div class="form-group">
                                    <label><strong><code>(*) </code>Estado: </strong></label><br>
                                        <select class="form-control mi-selector" name="estado_categoria">
                                            <option value="1">Activa</option>
                                            <option value="0">Inactiva</option>
                                        </select>
                                </div>-->

                              
                                
                                {!! Form::close() !!}
                                 @endforeach

                            </div>