
<div class="alert alert-danger">
	Todos los campos marcados con asteriscos (*) son obligatorios.
</div>

	<div class="row">
		<div class="datos_personales col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
			@csrf
			@method('POST')

			<div class="form-group">
				<label><strong><code>(*)</code>Nombre:</strong></label>
				<input type="text" name="nombre" id="nombre" class="form-control" value="{{ $persona[0]->primer_nombre }} {{ $persona[0]->primer_apellido }}" readonly="">
			</div>

			<div class="form-group" id="div_correo">
				<label><strong><code>(*)</code>Correo electr&oacute;nico:</strong> </label>
				<input type="text" name="email_wompi" id="email_wompi" class="form-control" value="{{ $persona[0]->correo_electronico }}" readonly="">
			</div>

			<div class="form-group">
				<label><strong><code>(*)</code>Celular:</strong></label>
				<input type="number" name="telefono" id="telefono" class="form-control" value="{{ $persona[0]->telefono }}" >
			</div>

			<div class="form-group">
				<label><strong><code>(*)</code>Valor a Pagar:</strong></label>
				<input type="number" name="valor_pago" id="valor_pago" class="form-control" value="{{ $planes[0]->valor_plan }}" readonly="">
			</div>
			<input type="hidden" name="code_plan" value="{{ $planes[0]->id_plan }}">

			<div class="form-check" style="margin-bottom: 15px;">
				<input class="form-check-input" type="checkbox" name="acceptance_token" id="terminos_condiciones_wompi" checked="" value="true" />
				<center>
					<label class="form-check-label" for="defaultCheck1">
						Acepto haber le&iacute;do los <b><a href="" target="_blank">t&eacute;minos y condiciones y la pol&iacute;tica de privacidad</b></a> para hacer esta compra.
					</label>
				</center>
			</div>
			<hr>
			<div class="form-group" id="div_metodo_pago">		
				
				<div class="row">
					@foreach($accepted_payment_methods as $method)
				    	@if($method == 'NEQUI')
						    <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
						    	<center>
									<input type="checkbox" name="metodo" class="only-one" value="nequi" >
								</center>
								<img src="{{ asset('img/nequi.jpg') }}" width="100%">
						    </div>
				    	@endif
						@if($method == 'CARD')
						    <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
								<center>
									<input type="checkbox" name="metodo" class="only-one" value="tarjetas-nequi">
								</center>
								<img src="{{ asset('img/tarjetas.png') }}" width="100%">
							</div>
				    	@endif

					@endforeach
				</div>
				<!--
				<div class="contenedor-tarjetas">
					/* @foreach($accepted_payment_methods as $method)
					@if($method == 'PSE')
						    <div id="PSE" class="PSE">
						    	<div class="select_pse">
							        <img src="{{ asset('assets/images/marca-de-verificacion.png') }}">
							    </div>
						        <div class="img_nequi">
						            <img src="{{ asset('assets/images/PSE.png') }}" width="100%">
						        </div>
						        <div class="descripcion">
						            PSE
						        </div>
						    </div>
				    	@endif
				    	@if($method == 'BANCOLOMBIA_TRANSFER')
						    <div id="BANCOLOMBIA_TRANSFER" class="BANCOLOMBIA_TRANSFER">
						    	<div class="select_transferencia">
							        <img src="{{ asset('assets/images/marca-de-verificacion.png') }}">
							    </div>
						        <div class="img_nequi">
						            <img src="{{ asset('assets/images/bancolombia.png') }}" width="100%">
						        </div>
						        <div class="descripcion">
						            Cuenta Bancolombia
						        </div>
						    </div> 
				    	@endif
				    @endforeach */
				</div>-->
			</div>
		</div>
		<div class="datos_pago col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6" id="formulario_pago">
			<div class="alert alert-success">
				<strong>Usuario: </strong> Por favor selecciona un medio de pago, para continuar con la actualizaci&oacute;n de Plan
			</div>

			<p style="text-align: center; font-size: 40px; font-weight: bold; margin-top: 40px; margin-bottom: 20px; color: black;">
					VALOR A PAGAR: ${{ $planes[0]->valor_plan }}
				</p>
		</div>
	</div>

<script type="text/javascript">

	 let Checked = null;
    //The class name can vary
    for (let CheckBox of document.getElementsByClassName('only-one')){
        CheckBox.onclick = function(){
        if(Checked!=null){
          Checked.checked = false;
          Checked = CheckBox;
        }
        Checked = CheckBox;
      }
    }

    $(document).on("click", ".only-one", function(){
    	
    	var formData = $("#update_plan").serialize();

    	$valor = $(this).val();

    	$.ajax({
    		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    		type: "POST",
    		url: "/metodoUpdatePayNequi/"+$valor,
    		data: formData,
    		success: function (data) {
    			$("#formulario_pago").html(data);
    		}
    	});
    });
</script>