<!-- Listado de las tiendas, no influye si es Administrador o Cliente -->

@extends('layouts.app')

@section('content')

<div class="data-tables">
    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <div class="data-tables">
                <div class="row">
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <div class="card-header chart-grid__header pl-0 pt-0">
                                Listado de asignaciones de Planes
                            </div>
                        </div>
                        @include('custom.message')
                    </div>
                    
                    <div class="col-lg-12 chart-grid mb-4">
                        <div class="card card_border p-4">
                            <!--<div class="messaging">
                                <b>Búsqueda de productos:</b>

                                <input type="text" name="buscadorProductos" id="buscadorProductos" class="form-control" placeholder="Busca tu producto..." />                            
                            </div>-->
                            <br>
                            <table class="table table-bordered table-hover" id="tabla-productos">
                                <thead>
                                    <tr>
                                        <th>Nombre Plan</th>
                                        <th>Tienda</th>
                                        <th>Fecha de Inicio</th>
                                        <th>Fecha de Proximo Pago</th>
                                        <th>Estado</th>
                                        <th colspan="3"><center>Acciones</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($asignaciones as $asignacion)
                                    <tr>
                                        <td>{{ $asignacion->nombre_plan }}</td>
                                        <td>{{ $asignacion->nombre_tienda }}</td>
                                        <td>{{ $asignacion->fecha_pago }}</td>
                                        <td>{{ $asignacion->fecha_proximo_pago }}</td>

                                        @if($asignacion->estado_plan == '1')
                                        <td class="alert alert-success">Activo</td>
                                        @else
                                        <td class="alert alert-danger">Inactivo</td>                                
                                        @endif
                                    </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</div>

@include('custom.modales')

@endsection
