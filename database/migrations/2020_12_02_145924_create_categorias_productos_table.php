<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasProductosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('categorias_productos', function (Blueprint $table) {
            $table->id();
            $table->longText('imagen_categoria');
            $table->string('nombre_categoria');
            $table->longText('descripcion_categoria')->nulleable();
            $table->integer('estado_categoria');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('categorias_productos');
    }

}
