<div class="container" style="padding-top: 30px; padding-bottom: 30px;">
	@foreach($personas as $persona)

			<div class="form-group">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>Foto persona:</b></label>
						<img src="{{ asset('storage/'.$persona->foto_persona) }}">
					</div>
				</div>
			</div>
			
			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Primer nombre:</b></label>
						<input type="text" class="form-control" placeholder="" value="{{ $persona->primer_nombre }}" disabled="">
					</div>
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b> Segundo nombre:</b></label>
						<input type="text" class="form-control" placeholder="" value="{{ $persona->segundo_nombre }}" disabled="">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Primer apellido:</b></label>
						<input type="text" class="form-control" placeholder="" value="{{ $persona->primer_apellido }}" disabled="">
					</div>
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b> Segundo apellido:</b></label>
						<input type="text" class="form-control" placeholder="" value="{{ $persona->segundo_apellido }}" disabled="">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Celular:</b></label>
						<input type="text" class="form-control" placeholder="" value="{{ $persona->telefono }}" disabled="">
					</div>
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Correo electronico:</b></label>
						<input type="email" class="form-control" placeholder="" value="{{ $persona->correo_electronico }}" disabled="">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
						<label style="padding-bottom: 5px;"><b>(*) Ciudad:</b></label>
						<select class="form-control mi-selector" id="usuario" disabled="">
							@foreach($departamentos as $departamento)
								@if($persona->id_ciudad == $departamento->codigo_departamento)
									<option value="{{ $departamento->nombre_departamento }}" selected="">{{ $departamento->nombre_departamento }}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Direccion:</b></label>
						<input type="text" class="form-control" placeholder="" value="{{ $persona->direccion }}" disabled="">
					</div>
					<div class="col-12 col-sm-6 col-md-6 col-lg-6">
						<label style="padding-bottom: 5px;"><b>(*) Barrio:</b></label>
						<input type="text" class="form-control" placeholder="" value="{{ $persona->barrio }}" disabled="">
					</div>
				</div>
			</div>

			<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Asignar al usuario:</b></label>
					<select class="form-control mi-selector" id="usuario" disabled="">
						<option value="" selected="">Seleccionar usuario</option>
						@foreach($usuarios as $usuario)
							@if($persona->user_register == $usuario->id)
								<option value="{{ $usuario->id }}" selected="">{{ $usuario->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6">
					<label style="padding-bottom: 5px;"><b>(*) Estado:</b></label>
					<select class="form-control mi-selector" disabled="">
						<option value="" selected="">Seleccionar estado</option>
						@if($persona->estado_persona == '1')
							<option value="1" selected="">Activo</option>
							<option value="0">Inactivo</option>
						@else
						    <option value="1" selected="">Activo</option>
							<option value="0">Inactivo</option>
						@endif
						</select>
					</div>
				</div>
			</div>
	@endforeach
</div>
<script type="text/javascript">

	$("#departamento").change(function () {
        var valor = $(this).val();
        //alert(valor);
        $.ajax({
            type: 'get',
            url: '/busqueda/ciudad/search/' + valor,
            success: function (data) {
                document.getElementById("ciudad").innerHTML = data;
            }
        });
    });

</script>