<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Epayco;
use Illuminate\Support\Facades\Crypt;
use Gate;
use DB;
use Auth;



class EpaycoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        Gate::authorize('haveaccess','epayco.index');

        $epaycos = Epayco::all();

        return view('epayco.index', compact('epaycos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        Gate::authorize('haveaccess','epayco.create');

        $tiendas = DB::table('tiendas')->get();
         $usuario = DB::table('role_user')
                        ->select('users.*', 'roles.*', 'role_user.*')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', Auth::user()->id)->get();

        return view('epayco.create', compact('tiendas', 'usuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        Gate::authorize('haveaccess','epayco.create');
        
        $request->validate([
            'id_cliente' => 'required', 
            'id_clave' => 'required',
            'public_key' => 'required',
            'private_key' => 'required',
            'tienda' => 'required', 
            'estado_epayco' => 'required',
        ]);
        
        Epayco::create([
            'client_id'     => Crypt::encrypt($request->id_cliente),
            'clave_id'      => Crypt::encrypt($request->id_clave),
            'public_key'    => Crypt::encrypt($request->public_key),
            'private_key'   => Crypt::encrypt($request->private_key),
            'epay_tienda'   => $request->tienda,
            'estado_epayco' => $request->estado_epayco
        ]);
        
        return redirect()->route('epayco.index')
            ->with('status_success','Creado correctamente!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Epayco::where('id', '=', $id)->delete();

        return redirect()->route('epayco.index')
            ->with('status_success','Elimado correctamente!'); 

    }
}
