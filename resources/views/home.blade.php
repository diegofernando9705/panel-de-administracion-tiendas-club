@extends('layouts.app')

@section('content')

    <div class="main-content">
        <div class="container-fluid content-top-gap">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb my-breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html">
                            Inicio
                        </a>
                    </li>
                    <li aria-current="page" class="breadcrumb-item active">
                            Principal
                    </li>
                </ol>
            </nav>
            <div class="welcome-msg pt-3 pb-4">
                <h1>
                    Hola
                    <span class="text-primary">
                        {{ auth()->user()->name }}
                    </span>, Bienvenido
                </h1>
                <p>
                    Ver informacion del perfil.
                </p>
            </div>


            <!-- statistics data -->
            <div class="statistics">
                <div class="row">
                     @if(isset($dias_faltantes))
                    <div class="alert-emergencia col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12">
                        <input type="button" class="boton-alerta " value="Tienes {{ $dias_faltantes }} d&iacute;a(s) de tu plan" /> 
                    </div>
                    @endif

                    <div class="col-xl-6 pr-xl-2">
                        <div class="row">
                            <div class="col-sm-6 pr-sm-2 statistics-grid">
                                <div class="card card_border border-primary-top p-4">
                                    <i class="lnr lnr-store"></i>
                                    <h3 class="text-primary number">
                                        {{ $tiendas }}
                                    </h3>
                                    <p class="stat-text">
                                        Tiendas registradas
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-6 pl-sm-2 statistics-grid">
                                <div class="card card_border border-primary-top p-4">
                                    <i class="lnr lnr-coffee-cup">
                                    </i>
                                    <h3 class="text-secondary number">
                                        {{ $productos }}
                                    </h3>
                                    <p class="stat-text">
                                        Productos registrados
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 pl-xl-2">
                        <div class="row">
                            <div class="col-sm-6 pr-sm-2 statistics-grid">
                                <div class="card card_border border-primary-top p-4">
                                    <i class="lnr lnr-cloud-download">
                                    </i>
                                    <h3 class="text-success number">
                                        {{ $venta_local_dia }}
                                    </h3>
                                    <p class="stat-text">
                                        Ventas del d&iacute;a
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-6 pl-sm-2 statistics-grid">
                                <div class="card card_border border-primary-top p-4">
                                    <i class="lnr lnr-cart">
                                    </i>
                                    <h3 class="text-danger number">
                                        $ {{ number_format($ventas_totales) }}
                                    </h3>
                                    <p class="stat-text">
                                        Ventas totales
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="chart">
                <div class="row">
                    <div class="col-lg-6 pr-lg-2 chart-grid">
                        <div class="card text-center card_border">
                            <div class="card-header chart-grid__header">
                                Pedidos en los &uacute;ltimos {{ $number }} d&iacute;as
                            </div>
                            <div class="card-body">
                                <div id="container">
                                    <canvas id="barchart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pl-lg-2 chart-grid">
                        <div class="card text-center card_border">
                            <div class="card-header chart-grid__header">
                                Pedidos por hora
                            </div>
                            <div class="card-body">
                                <div id="container">
                                    <canvas id="linechart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="chart">
                <div class="row">
                    <div class="col-lg-6 pr-lg-2 chart-grid">
                        <div class="card text-center card_border">
                            <div class="card-header chart-grid__header">
                                Ranking de calificaci&oacute;n
                            </div>
                            <div class="card-body">
                                <div id="container">
                                    <canvas id="myChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pl-lg-2 chart-grid">
                        <div class="card text-center card_border">
                            <div class="card-header chart-grid__header">
                                Ventas en el punto VS Ventas a domicilio
                            </div>
                            <div class="card-body">
                                <div id="container">
                                    <canvas id="domicilios_locales"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>




@endsection


@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){

            var ctx = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'polarArea',
                data: {
                    labels: [<?php foreach ($array_descripcion as $nombres) {
                        echo "'".$nombres."',";
                    } ?>],
                    datasets: [{
                        label: '# of Votes',
                        data: [<?php foreach ($array_informacion as $datos) {
                                echo "'".$datos."',";
                            } ?>],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });


            new Chart(document.getElementById("barchart"), {
                type: 'bar',
                data: {
                    labels: [<?php foreach($dias_mes as $dia) { echo "'".date('Y-m-').$dia."',"; } ?><?php echo "'Hoy'"; ?>],
                    datasets: [{
                        data: [<?php foreach ($presupuesto as $valor) { echo "'".$valor."',"; } echo $dia_hoy; ?>],
                        label: 'Ventas en el mes',
                        backgroundColor: "#4755AB",
                        borderWidth: 1,
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                }
            });
        });

        new Chart(document.getElementById("linechart"), {
    type: 'line',
    data: {
        labels: [<?php foreach ($array_hora as $hora) { echo "'Hora ".$hora."',"; } echo "''"; ?>],
        datasets: [{
            label: 'Pedidos: ',
            backgroundColor: window.chartColors.navy,
            borderColor: window.chartColors.navy,
            data: [<?php foreach ($total_hora as $valor_hora) { echo "'".$valor_hora."',"; } echo "''"; ?>],
            fill: false,
        }]
    },
    options: {
        responsive: true,
        // title: {
        //  display: true,
        //  text: 'Chart.js Line Chart'
        // },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Todos los meses'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Value'
                }
            }]
        }
    }
});

    /* locales vs domicilios */

    new Chart(document.getElementById("domicilios_locales"), {
                type: 'pie',
                data: {
                    labels: ['Pedidos locales', 'Pedidos domicilios'],
                    datasets: [{
                        data: [<?php echo $venta_local_total; ?>, <?php echo $venta_domicilio_total; ?>],
                        label: 'Ventas en el mes',
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                }
            });


    </script>

@endsection